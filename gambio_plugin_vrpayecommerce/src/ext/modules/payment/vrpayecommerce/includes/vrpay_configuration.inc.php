<?php
/**
 * Set multiselect option to credit cards cardtypes (VISA, MASTERCARD, JCB, DINER, AMEX)
 *
 * @param  array $select_array
 * @param  array $key_value
 * @param  array $key
 * @return string
 */
function vrpay_cfg_multiselect_option($select_array, $key_value, $key = '')
{
    $selected = explode(',', $key_value);
    $name = (($key) ? 'configuration['.$key.']' : 'configuration_value');
    $string = '<script language="JavaScript">
           function getSelectedCards()
           {
                var cardtypes = document.getElementById(\'cards_types\');
                var cardtypesVal = "";
                var i = 0;
                for (i=0;i<cardtypes.length;i++)
                {
                    if (cardtypes[i].selected)
                    {
                        cardtypesVal = cardtypesVal + "," + cardtypes[i].value;
                    }
                }
                document.getElementById(\'select_cards\').value = cardtypesVal.substring(1);
            }
        </script>';
    $string .= '<br><input id ="select_cards" type="hidden" name ="'.$name.'" value ="'.$key_value.'">';
    $string .= '<select onchange="getSelectedCards();" id ="cards_types"
    name="cards_types[]" multiple="multiple" size="5" style="width:148px; height: 85px;">';
    foreach ($select_array as $value) {
        $attribute = in_array($value, $selected) ? 'selected="selected"' : '';
        $string .= '<option value="'.$value.'" '.$attribute.'>'.$value.'</option>';
    }
    $string .= '</select>';
    return $string;
}

/**
 * Set input field required for vrpayecommerce parameter config
 *
 * @param  array $key_value
 * @param  array $key
 * @return xtc_draw_input_field
 */
function vrpay_cfg_input_required($key_value, $key = '')
{
    $name = (($key) ? 'configuration['.$key.']' : 'configuration_value');
    return xtc_draw_input_field($name, $key_value, 'required');
}
