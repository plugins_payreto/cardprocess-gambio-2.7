<?php
chdir('../../../../');
require 'includes/application_top.php';
require DIR_WS_LANGUAGES.$_SESSION['language'].'/modules/payment/vrpayecommerce_info.php';

$smarty = new Smarty;

$GLOBALS['breadcrumb']->add(NAVBAR_TITLE_1_ACCOUNT_PASSWORD, xtc_href_link(FILENAME_ACCOUNT, '', 'SSL'));
$GLOBALS['breadcrumb']->add(FRONTEND_MC_INFO, xtc_href_link('account_payment_information.php', '', 'SSL'));

$selected_payment = xtc_db_prepare_input($_REQUEST['selected_payment']);
require_once DIR_WS_MODULES. 'payment/'.$selected_payment.'.php';
$payment_form = new $selected_payment;
$checkout_id = $payment_form->getCheckoutIdRecurringPayment($payment_widget_url);

if (!$checkout_id) {
    xtc_redirect(xtc_href_link('account_payment_information.php?error=ERROR_GENERAL_REDIRECT', '', "SSL", true, false));
}

switch ($_SESSION['language_code']) {
    case 'de':
        $lang = $_SESSION['language_code'];
        break;
    default:
        $lang = "en";
}

$smarty->assign('error_message', $_GET['error']);
$smarty->assign('language', $_SESSION['language']);
$smarty->assign('lang', $lang);
$smarty->assign('test_mode', $payment_form->getTestMode());
$smarty->assign('brand', $payment_form->getBrand());
$smarty->assign('redirect', $payment_form->isRedirectAccount());
$smarty->assign('payment_widget_url', $payment_widget_url);
$smarty->assign(
    'response_url',
    xtc_href_link(
        'ext/modules/payment/vrpayecommerce/checkout_response.php?payment_module='.$selected_payment,
        '',
        "SSL",
        true,
        false
    )
);
$smarty->assign('cancel_url', xtc_href_link('account_payment_information.php', '', 'SSL'));

$t_main_content = $smarty->fetch(
    CURRENT_TEMPLATE.'/module/payment/vrpayecommerce/register_payment_'
    .strtolower($payment_form->getGroupRecurring()).'.html'
);

$smarty->assign('language', $_SESSION['language']);

$coo_layout_control = MainFactory::create_object('LayoutContentControl');
$coo_layout_control->set_data('GET', $_GET);
$coo_layout_control->set_data('POST', $_POST);
$coo_layout_control->set_('coo_breadcrumb', $GLOBALS['breadcrumb']);
$coo_layout_control->set_('coo_product', $GLOBALS['product']);
$coo_layout_control->set_('coo_xtc_price', $GLOBALS['xtPrice']);
$coo_layout_control->set_('c_path', $GLOBALS['cPath']);
$coo_layout_control->set_('main_content', $t_main_content);
$coo_layout_control->set_('request_type', $GLOBALS['request_type']);
$coo_layout_control->proceed();

$t_redirect_url = $coo_layout_control->get_redirect_url();
if (empty($t_redirect_url) === false) {
    xtc_redirect($t_redirect_url);
} else {
    echo $coo_layout_control->get_response();
}
