<?php

define('MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_TEXT_TITLE', 'Credit Card');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_TEXT_DESCRIPTION',
    '<a href="http://www.vrpayecommerce.de/" target="_blank" style="text-decoration: underline; font-weight: bold;">
    <img src="../ext/modules/payment/vrpayecommerce/images/logovrpay.jpg" border="0">
    </a></br><p>VR pay eCommerce - Credit Card (Recurring)</p>'
);

define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_STATUS_TITLE',
    'Enable VR pay eCommerce Credit Card (Recurring) Module'
);
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_STATUS_DESC',
    'Please activate the module, if you want to accept Credit Card (Recurring) transactions of VR pay eCommerce.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_SERVER_MODE_TITLE', 'Server');
define('MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_SERVER_MODE_DESC', 'Choose the server for Credit Card (Recurring).');

define('MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_PAYMENT_TYPE_TITLE', 'Transaction Mode');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_PAYMENT_TYPE_DESC',
    'Select Transaction Mode for Credit Card (Recurring).'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_CARDS_TYPE_TITLE', 'Cards Types');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_CARDS_TYPE_DESC',
    'Select avaiable cards types for Credit Card (Recurring).'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_REGISTER_AMOUNT_TITLE', 'Register Amount');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_REGISTER_AMOUNT_DESC',
    'Amount that is debited and refunded when a shopper registers a payment method without purchase'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_MULTICHANNEL_TITLE', 'Enable Multichannel');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_MULTICHANNEL_DESC',
    'If activated, repeated recurring payments are handled by the alternative channel'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_CHANNEL_TITLE', 'Entity-ID');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_CHANNEL_DESC',
    'Insert the Entity-ID for Credit Card (Recurring).'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_CHANNEL_MOTO_TITLE', 'Entity-ID MOTO');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_CHANNEL_MOTO_DESC',
    'Alternative channel for recurring payments if Multichannel is activated (to bypass 3D Secure)'
);

define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_PA_ORDER_STATUS_ID_TITLE',
    'Set the Order Status for Pre-Authorization'
);
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_PA_ORDER_STATUS_ID_DESC',
    'Define here the Order Status which should be used for Pre-Authorized Payments.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_ORDER_STATUS_ID_TITLE', 'Set Order Status for Accepted payment');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_ORDER_STATUS_ID_DESC',
    'Define here the Order Status which should be used for Accepted Payments'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_RF_ORDER_STATUS_ID_TITLE', 'Set Order Status for Refund');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_RF_ORDER_STATUS_ID_DESC',
    'Define here the Order Status which should be used for Refund'
);

define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_ZONE_TITLE',
    'Payment Zone'
);
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_ZONE_DESC',
    'When a country is selected, this payment method will be enabled for that zone only.'
);

define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_ALLOWED_TITLE',
    'Allowed Countries'
);
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_ALLOWED_DESC',
    'Please enter the countries <b>individually</b>
    that should be allowed to use this module (e.g. US, UK (leave blank to allow all countries)).'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_SORT_ORDER_TITLE', 'Display Sort Order');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_SORT_ORDER_DESC',
    'Display sort order; the lowest value is displayed first.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_TT_TRANSACTION_ID', 'Transaction ID');

define('FRONTEND_MC_ENDING', 'ending in:');
define('FRONTEND_MC_VALIDITY', 'expires on:');
define('FRONTEND_MC_CARDNEW', 'Add a new credit card');
define('FRONTEND_MC_BT_CHANGE', 'Change');
define('FRONTEND_MC_BT_SELECT', 'Select');
define('FRONTEND_MC_CCSELECT', 'Select Credit Card');
