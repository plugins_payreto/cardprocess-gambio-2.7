## 2.0.0 - 2015-10-22 - shop version compatible gambio 2.7.2.0
* initial plugin from 1.0.6

## 2.1.0 - 2015-06-30 - shop version compatible gambio 2.7.2.0
*shippingAmount parameter is removed from paydirekt payment request, this parameter is included in version 2.0.0

## 2.1.01 - 2015-07-19 - shop version compatible gambio 2.7.2.0
* show transaction id under order detail.

## 2.1.02 - 2015-08-31 - shop version compatible gambio 2.7.2.0
* remove fraud validation.

## 2.1.02 - 2016-09-29 - shop version compatible gambio 2.5.3.1 - 2.7.2.0
* additional instruction to install this plugin to gambio 2.5.

## 2.1.03 - 2016-10-03 - shop version compatible gambio 2.5.3.1 - 2.7.2.0
* add an option to enable and disable version tracker at backend configuration.

## 2.1.03 - 2016-10-04 - shop version compatible gambio 2.5.3.1 - 2.7.2.0
* fix bug: cannot display admin message after installing plugin.

## 2.1.03 - 2016-10-06 - shop version compatible gambio 2.5.3.1 - 2.7.2.0
* fix bug: payment widget cannot display correctly when lightbox configuration is activated.

## 2.1.03 - 2016-10-26 - shop version compatible gambio 2.5.3.1 - 2.7.2.0
* fix violations in PSR2 Coding Standard.

## 2.1.04 - 2016-12-15 - shop version compatible gambio 2.5.3.1 - 2.7.2.0
* fix bug: transaction are not sent correctly when using the paydirekt payment method.

## 2.1.05 - 2017-01-02- shop version compatible gambio 2.5.3.1 - 2.7.2.0
* implement easycredit payment method.
* add dockblock and implement PSR2 coding standard.

## 2.1.05 - 2017-01-04- shop version compatible gambio 2.5.3.1 - 2.7.2.0
* add currency validation for easycredit.

## 2.1.06 - 2017-02-21- shop version compatible gambio 2.5.3.1 - 2.7.2.0
* fix redeclare function issue.
* make gender field mandatory for easycredit payment method.

## 2.1.07 - 2017-03-16- shop version compatible gambio 2.5.3.1 - 2.7.2.0
* Remove mandate parameter in SEPA payment method - (PMC-651)
* remove unused code

## 2.1.08 - 2017-04-05- shop version compatible gambio 2.5.3.1 - 2.7.2.0
* Update validation translation for ratenkauf. - PMC-765

## 2.1.08 - 2017-04-06- shop version compatible gambio 2.5.3.1 - 2.7.2.0
* Change MasterCard branding logo. - PMC-812

## 2.1.09 - 2017-05-05- shop version compatible gambio 2.5.3.1 - 2.7.2.0
* fix bug : order with payment method easyCredit will be failed when registered customer make order for the first time. - PMC-941

## 2.1.10 - 2017-05-17 - shop version compatible gambio 2.3.3.1
* Change SEPA brand from DIRECTDEBIT_SEPA_MIX_DE into DIRECTDEBIT_SEPA - PMC-1014

## 2.1.11 - 2017-05-31 - shop version compatible gambio 2.5.3.1 - 2.7.2.0
* Update gambio 2.5 and 2.7 readme text file for implement backend operation. - PMC-1106

## 2.1.11 - 2017-06-02 - shop version compatible gambio 2.5.3.1 - 2.7.2.0
* Bug fix: wrong rule for date of birth for easycredit. - PMC-1131

## 2.1.11 - 2017-06-05 - shop version compatible gambio 2.5.3.1 - 2.7.2.0
* Fix wrong translations at error message validation easycredit when General setting is not filled. - PMC-1131

## 2.1.11 - 2017-06-05 - shop version compatible gambio 2.5.3.1 - 2.7.2.0
* fix bug : make http_build_query separator always use "&" - GSST-262

## 2.1.11 - 2017-06-05 - shop version compatible gambio 2.5.3.1 - 2.7.2.0
* make curl verify ssl true when in live mode and false when in test mode. - PMC-1156
* fix bug : wrong order total amount when creating order from admin - PMC-1156

## 2.1.11 - 2017-06-12 - shop version compatible gambio 2.5.3.1 - 2.7.2.0
* fix bug : can not select SEPA and Paypal payment method when do backend order - PMC-1156

## 2.1.11 - 2017-06-14 - shop version compatible gambio 2.5.3.1 - 2.7.2.0
* add logging function for every payment process - PMC-1156
* rename function and variable that not clear - PMC-1156