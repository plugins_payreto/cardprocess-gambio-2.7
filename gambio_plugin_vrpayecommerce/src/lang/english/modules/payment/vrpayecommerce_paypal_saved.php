<?php

define('MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_TEXT_TITLE', 'PayPal');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_TEXT_DESCRIPTION',
    '<a href="http://www.vrpayecommerce.de/" target="_blank" style="text-decoration: underline; font-weight: bold;">
    <img src="../ext/modules/payment/vrpayecommerce/images/logovrpay.jpg" border="0">
    </a></br><p>VR pay eCommerce - PayPal (Recurring)</p>'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_STATUS_TITLE', 'Enable VR pay eCommerce PayPal (Recurring) Module');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_STATUS_DESC',
    'Please activate the module, if you want to accept PayPal (Recurring) transactions of VR pay eCommerce.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_SERVER_MODE_TITLE', 'Server');
define('MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_SERVER_MODE_DESC', 'Choose the server for PayPal (Recurring).');

define('MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_REGISTER_AMOUNT_TITLE', 'Register Amount');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_REGISTER_AMOUNT_DESC',
    'Amount that is debited and refunded when a shopper registers a payment method without purchase'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_CHANNEL_TITLE', 'Entity-ID');
define('MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_CHANNEL_DESC', 'Insert the Entity-ID for PayPal (Recurring).');

define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_ORDER_STATUS_ID_TITLE',
    'Set Order Status for Accepted payment'
);
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_ORDER_STATUS_ID_DESC',
    'Define here the Order Status which should be used for Accepted Payments'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_RF_ORDER_STATUS_ID_TITLE', 'Set Order Status for Refund');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_RF_ORDER_STATUS_ID_DESC',
    'Define here the Order Status which should be used for Refund'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_ZONE_TITLE', 'Payment Zone');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_ZONE_DESC',
    'When a country is selected, this payment method will be enabled for that zone only.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_ALLOWED_TITLE', 'Allowed Countries');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_ALLOWED_DESC',
    'Please enter the countries <b>individually</b>
    that should be allowed to use this module (e.g. US, UK (leave blank to allow all countries)).'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_SORT_ORDER_TITLE', 'Display Sort Order');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_SORT_ORDER_DESC',
    'Display sort order; the lowest value is displayed first.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_TT_TRANSACTION_ID', 'Transaction ID');

define('FRONTEND_MC_EMAIL', 'Email:');
define('FRONTEND_MC_HOLDER', 'Holder:');
define('FRONTEND_MC_PAYPALNEW', 'Add a new PayPal account');
define('FRONTEND_MC_BT_CHANGE', 'Change');
define('FRONTEND_MC_BT_SELECT', 'Select');
define('FRONTEND_MC_PAYPALSELECT', 'Select PayPal account');
