<?php
/* --------------------------------------------------------------
  checkout_confirmation.php 2014-02-11 gm
  Gambio GmbH
  http://www.gambio.de
  Copyright (c) 2014 Gambio GmbH
  Released under the GNU General Public License (Version 2)
  [http://www.gnu.org/licenses/gpl-2.0.html]
  --------------------------------------------------------------


  based on:
  (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
  (c) 2002-2003 osCommerce(checkout_confirmation.php,v 1.137 2003/05/07); www.oscommerce.com
  (c) 2003   nextcommerce (checkout_confirmation.php,v 1.21 2003/08/17); www.nextcommerce.org
  (c) 2003 XT-Commerce - community made shopping http://www.xt-commerce.com
  ($Id: checkout_confirmation.php 1277 2005-10-01 17:02:59Z mz $)

  Released under the GNU General Public License
  -----------------------------------------------------------------------------------------
  Third Party contributions:
  agree_conditions_1.01         Autor:  Thomas Ploenkers (webmaster@oscommerce.at)

  Customers Status v3.x  (c) 2002-2003 Copyright Elari elari@free.fr
  | www.unlockgsm.com/dload-osc/ | CVS : http://cvs.sourceforge.net/cgi-bin/viewcvs.cgi/elari/?sortby=date#dirlist

  Credit Class/Gift Vouchers/Discount Coupons (Version 5.10)
  http://www.oscommerce.com/community/contributions,282
  Copyright (c) Strider | Strider@oscworks.com
  Copyright (c  Nick Stanko of UkiDev.com, nick@ukidev.com
  Copyright (c) Andre ambidex@gmx.net
  Copyright (c) 2001,2002 Ian C Wilson http://www.phesis.org

  Released under the GNU General Public License
  --------------------------------------------------------------------------------------- */

require 'includes/application_top.php';

$payment_module = $_SESSION['payment'];
require_once DIR_WS_MODULES. 'payment/'. $payment_module . '.php';
$payment_form = new $payment_module;

$vrpaycommerce_logger = $payment_form->vrpaycommerce_logger;
$vrpaycommerce_logger->notice('start render payment widgetd in checkout page', 'security', 'vrpayecommerce_log');

$GLOBALS['breadcrumb']->add(
    NAVBAR_TITLE_1_CHECKOUT_CONFIRMATION,
    xtc_href_link(
        FILENAME_CHECKOUT_SHIPPING,
        '',
        'SSL'
    )
);
$GLOBALS['breadcrumb']->add(NAVBAR_TITLE_2_CHECKOUT_CONFIRMATION);

// if the customer is not logged on, redirect them to the login page
if (isset($_SESSION['customer_id']) === false) {
    $vrpaycommerce_logger->notice('redirect to login page because customer is not logged on', 'security', 'vrpayecommerce_log');
    xtc_redirect(xtc_href_link(FILENAME_LOGIN, '', 'SSL'));
}


// mediafinanz
require_once DIR_FS_CATALOG . 'includes/modules/mediafinanz/include_checkout_confirmation.php';

$coo_checkout_confirmation_control = MainFactory::create_object('CheckoutConfirmationContentControl');
$coo_checkout_confirmation_control->set_data('GET', $_GET);
$coo_checkout_confirmation_control->set_data('POST', $_POST);

$coo_checkout_confirmation_control->proceed();

$t_redirect_url = $coo_checkout_confirmation_control->get_redirect_url();
if (empty($t_redirect_url) == false) {
    xtc_redirect($t_redirect_url);
} else {
    $t_main_content = $coo_checkout_confirmation_control->get_response();
}
$vrpaycommerce_logger->notice('is success load checkout template : '.print_r(empty($t_redirect_url),1), 'security', 'vrpayecommerce_log');

//add vrpayecommerce payment form ---------
$smarty = new smarty;
$smarty->assign('LIGHTBOX', gm_get_conf('GM_LIGHTBOX_CHECKOUT'));
$vrpaycommerce_logger->notice('is  LIGHTBOX active : '.gm_get_conf('GM_LIGHTBOX_CHECKOUT'), 'security', 'vrpayecommerce_log');
$smarty->assign('LIGHTBOX_CLOSE', xtc_href_link(FILENAME_DEFAULT, '', 'NONSSL'));
$vrpaycommerce_logger->notice('is  LIGHTBOX close button link : '.xtc_href_link(FILENAME_DEFAULT, '', 'NONSSL'), 'security', 'vrpayecommerce_log');
$smarty->assign('language', $_SESSION['language']);
$vrpaycommerce_logger->notice('get shop language : '.$_SESSION['language'], 'security', 'vrpayecommerce_log');
$smarty->assign('PAYMENT_BLOCK', $payment_block);
$vrpaycommerce_logger->notice('get PAYMENT_BLOCK : '.print_r($payment_block,1), 'security', 'vrpayecommerce_log');
$smarty->caching = 0;


$vrpaycommerce_logger->notice('get payment_module : '.print_r($payment_module,1), 'security', 'vrpayecommerce_log');

if ($payment_module == 'vrpayecommerce_easycredit') {
    $server_to_server_parameter = $payment_form->getServerToServerParameter($payment_widget_url);
    $server_to_server_response = VRpayecommercePaymentCore::getServerToServerResponse($server_to_server_parameter);

    if ($server_to_server_response == 'ERROR_MERCHANT_SSL_CERTIFICATE') {
        $vrpaycommerce_logger->notice('error because of ssl certificate', 'security', 'vrpayecommerce_log');
        $payment_form->redirectFailure('ERROR_MERCHANT_SSL_CERTIFICATE');
    }
    if (!$server_to_server_response) {
        $vrpaycommerce_logger->notice('error because of no response from the gateway', 'security', 'vrpayecommerce_log');
        $payment_form->redirectFailure('ERROR_GENERAL_REDIRECT');
    }

    $transaction_result = VRpayecommercePaymentCore::getTransactionResult($server_to_server_response['result']['code']);
    $vrpaycommerce_logger->notice('get transaction_result '.print_r($transaction_result,1), 'security', 'vrpayecommerce_log');

    if ($transaction_result == 'NOK') {
        $error_identifier = VRpayecommercePaymentCore::getErrorIdentifier($server_to_server_response['result']['code']);
        $vrpaycommerce_logger->notice('transaction failed with error : '.print_r($error_identifier,1), 'security', 'vrpayecommerce_log');
        $payment_form->redirectFailure($error_identifier);
    }

    $smarty->assign('redirect_url', $server_to_server_response['redirect']['url']);
    $vrpaycommerce_logger->notice('get server to server redirect url : '.print_r($server_to_server_response['redirect']['url'],1), 'security', 'vrpayecommerce_log');
    $smarty->assign('parameters', $server_to_server_response['redirect']['parameters']);
    $vrpaycommerce_logger->notice('get server to server redirect url parameters: '.print_r($server_to_server_response['redirect']['parameters'],1), 'security', 'vrpayecommerce_log');

} else {
    $checkout_id = $payment_form->getCheckoutId($payment_widget_url);

    if ($payment_widget_url == 'ERROR_MERCHANT_SSL_CERTIFICATE') {
        $vrpaycommerce_logger->notice('error because of ssl certificate', 'security', 'vrpayecommerce_log');
        xtc_redirect(
            xtc_href_link(
                'checkout_payment.php?payment_error='.$payment_module.'&error=ERROR_MERCHANT_SSL_CERTIFICATE',
                '',
                "SSL",
                true,
                false
            )
        );
    }

    if (!$checkout_id) {
        $vrpaycommerce_logger->notice('error because checkout_id is empty', 'security', 'vrpayecommerce_log');
        xtc_redirect(
            xtc_href_link(
                'checkout_payment.php?payment_error='.$payment_module.'&error=ERROR_GENERAL_REDIRECT',
                '',
                "SSL",
                true,
                false
            )
        );
    }
}

switch ($_SESSION['language_code']) {
    case 'de':
        $lang = $_SESSION['language_code'];
        break;
    default:
        $lang = "en";
}

$error_message = $payment_form->get_error();

$smarty->assign('error_message', $error_message['error']);
$vrpaycommerce_logger->notice('get error_message : '.print_r($error['error'],1), 'security', 'vrpayecommerce_log');

$smarty->assign('lang', $lang);
$vrpaycommerce_logger->notice('get shop language code : '.print_r($lang,1), 'security', 'vrpayecommerce_log');

$smarty->assign('test_mode', $payment_form->getTestMode());
$vrpaycommerce_logger->notice('get payment test_mode : '.print_r($payment_form->getTestMode(),1), 'security', 'vrpayecommerce_log');

$smarty->assign('brand', $payment_form->getBrand());
$vrpaycommerce_logger->notice('get payment brand : '.print_r($payment_form->getBrand(),1), 'security', 'vrpayecommerce_log');

$smarty->assign('recurring', $payment_form->isRecurring());
$vrpaycommerce_logger->notice('is recurring payment method : '.print_r($payment_form->isRecurring(),1), 'security', 'vrpayecommerce_log');

$smarty->assign('redirect', $payment_form->isRedirect());
$vrpaycommerce_logger->notice('get shop language code : '.print_r($payment_form->isRedirect(),1), 'security', 'vrpayecommerce_log');

$smarty->assign('payment_widget_url', $payment_widget_url);
$vrpaycommerce_logger->notice('get payment_widget_url : '.print_r($payment_widget_url,1), 'security', 'vrpayecommerce_log');

$smarty->assign(
    'response_url',
    xtc_href_link(
        'ext/modules/payment/vrpayecommerce/checkout_response.php?payment_module='
        .$payment_module.'&response_from=checkout',
        '',
        "SSL",
        true,
        false
    )
);
$smarty->assign('cancel_url', xtc_href_link(FILENAME_CHECKOUT_PAYMENT, '', 'SSL'));
$vrpaycommerce_logger->notice('get cancel url : '.print_r(xtc_href_link(FILENAME_CHECKOUT_PAYMENT, '', 'SSL'),1), 'security', 'vrpayecommerce_log');

$smarty->assign('registrations', $payment_form->getRecurringLists());
$vrpaycommerce_logger->notice('get regitered account : '.print_r($payment_form->getRecurringLists(),1), 'security', 'vrpayecommerce_log');

$t_main_content = $smarty->fetch(CURRENT_TEMPLATE . '/module/payment/vrpayecommerce/'.$payment_form->getTemplate());

//-------------------------------

$coo_layout_control = MainFactory::create_object('LayoutContentControl');
$coo_layout_control->set_data('GET', $_GET);
$coo_layout_control->set_data('POST', $_POST);
$coo_layout_control->set_('coo_breadcrumb', $GLOBALS['breadcrumb']);
$coo_layout_control->set_('coo_product', $GLOBALS['product']);
$coo_layout_control->set_('coo_xtc_price', $GLOBALS['xtPrice']);
$coo_layout_control->set_('c_path', $GLOBALS['cPath']);
$coo_layout_control->set_('main_content', $t_main_content);
$coo_layout_control->set_('request_type', $GLOBALS['request_type']);
$coo_layout_control->proceed();

$t_redirect_url = $coo_layout_control->get_redirect_url();
if (empty($t_redirect_url) === false) {
    xtc_redirect($t_redirect_url);
} else {
    echo $coo_layout_control->get_response();
}
