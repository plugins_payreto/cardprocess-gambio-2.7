<?php

define('MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_TEXT_TITLE', 'Kreditkarte');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_TEXT_DESCRIPTION',
    '<a href="http://www.vrpayecommerce.de" target="_blank" style="text-decoration: underline; font-weight: bold;">
    <img src="../ext/modules/payment/vrpayecommerce/images/logovrpay.jpg" border="0">
    </a></br><p>VR pay eCommerce - Kreditkarte (Recurring)</p>'
);

define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_STATUS_TITLE',
    'Aktivieren Sie das VR pay eCommerce Kreditkarte (Recurring) Modul'
);
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_STATUS_DESC',
    'Bitte aktivieren Sie das Modul,
    wenn Sie Kreditkarte (Recurring) Transaktionen von VR pay eCommerce akzeptieren wollen.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_SERVER_MODE_TITLE', 'Server');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_SERVER_MODE_DESC',
    'Wählen Sie den Server für gespeicherte Kreditkarte.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_PAYMENT_TYPE_TITLE', 'Transaktions Modus');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_PAYMENT_TYPE_DESC',
    'Transaktions Modus für gespeicherte Kreditkarte auswählen.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_CARDS_TYPE_TITLE', 'Kartenarten');
define('MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_CARDS_TYPE_DESC', 'Kartenarten für Kreditkarte (Recurring).');

define('MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_REGISTER_AMOUNT_TITLE', 'Betrag für Registrierung');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_REGISTER_AMOUNT_DESC',
    'Betrag, der bei der Registrierung von Zahlungsarten gebucht und gutgeschrieben wird
    (wenn die Registrierung ohne Checkout erfolgt)'
);

define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_MULTICHANNEL_TITLE',
    'Multichannel aktivieren'
);
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_MULTICHANNEL_DESC',
    'Wenn aktiviert, werden wiederkehrende Zahlungen über den alternativen Kanal abgewickelt'
);

define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_CHANNEL_TITLE',
    'Entity-ID'
);
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_CHANNEL_DESC',
    'Entity-ID für gespeicherte Kreditkarte hinterlegen.'
);

define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_CHANNEL_MOTO_TITLE',
    'Entity-ID MOTO'
);
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_CHANNEL_MOTO_DESC',
    'Alternativer Kanal (z.B. ohne 3D Secure)
    für wiederkehrende Zahlungen (nur bei aktiviertem Multichannel benötigt)'
);

define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_PA_ORDER_STATUS_ID_TITLE',
    'Bestellstatus für Pre-Authorization festlegen'
);
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_PA_ORDER_STATUS_ID_DESC',
    'Hinterlegen Sie hier den Bestellstatus, der bei Pre-Authorisierten Zahlungen angezeigt werden soll.'
);

define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_ORDER_STATUS_ID_TITLE',
    'Bestellstatus für akzeptierte Zahlungen festlegen'
);
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_ORDER_STATUS_ID_DESC',
    'Hinterlegen Sie hier den Bestellstatus, der bei akzeptierten Zahlungen angezeigt werden soll.'
);

define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_RF_ORDER_STATUS_ID_TITLE',
    'Bestellstatus für Refunds hinterlegen'
);
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_RF_ORDER_STATUS_ID_DESC',
    'Hinterlegen Sie hier den Bestellstatus, der bei Refunds angezeigt werden soll.'
);

define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_ZONE_TITLE',
    'Zahlungszone'
);
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_ZONE_DESC',
    'Wenn ein Land ausgewählt wurde, ist die Zahlungsart nur für dieses Land freigeschaltet.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_ALLOWED_TITLE', 'Erlaubte Länder');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_ALLOWED_DESC',
    'Bitte wählen Sie die Länder aus, in den dieses Modul verfügbar sein soll.'
);

define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_SORT_ORDER_TITLE',
    'Legen Sie die Reihenfolge der Zahlungsarten fest'
);

define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_SORT_ORDER_DESC',
    'Die Reihenfolge in der die Zahlungsarten im Bestellprozess angezeigt werden,
    wird im Feld &bdquo;Reihenfolge&rdquo; mit bei &bdquo;1&rdquo;
    beginnenden Ziffern festgelegt und kann individuell angepasst werden.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_TT_TRANSACTION_ID', 'Transaktions-ID');

define('FRONTEND_MC_ENDING', 'endet auf:');
define('FRONTEND_MC_VALIDITY', 'g&uuml;ltig bis:');
define('FRONTEND_MC_CARDNEW', 'Eine neue Karte hinzuf&uuml;gen');
define('FRONTEND_MC_BT_CHANGE', '&Auml;ndern');
define('FRONTEND_MC_BT_SELECT', 'Ausw&auml;hlen');
define('FRONTEND_MC_CCSELECT', 'Kreditkarte ausw&auml;hlen');
