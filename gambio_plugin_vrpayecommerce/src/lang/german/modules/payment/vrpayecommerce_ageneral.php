<?php

define('VRPAYECOMMERCE_BACKEND_GENERAL_VRPAY', 'VR pay eCommerce');

define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_TEXT_TITLE', 'VR pay eCommerce - Allgemeine Einstellungen');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_TEXT_DESCRIPTION',
    '<a href="http://www.vrpayecommerce.de/" target="_blank" style="text-decoration: underline; font-weight: bold;">
    <img src="../ext/modules/payment/vrpayecommerce/images/logovrpay.jpg" border="0">
    </a></br><p>VR pay eCommerce v2.1.11 - Allgemeine Einstellungen</p>'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_STATUS_TITLE', 'VR pay eCommerce - Allgemeine Einstellungen');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_STATUS_DESC',
    'Allgemeine Einstellungen für das VR pay eCommerce Modul'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_USER_LOGIN_TITLE', 'User-ID');
define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_USER_LOGIN_DESC', 'User-ID hinterlegen');

define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_USER_PWD_TITLE', 'Passwort');
define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_USER_PWD_DESC', 'Passwort hinterlegen');

define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_RECURRING_TITLE', 'Recurring');
define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_RECURRING_DESC', 'Recurring aktivieren');

define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_MERCHANT_EMAIL_TITLE', 'Händler E-Mail Adresse');
define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_MERCHANT_EMAIL_DESC', 'Händler E-Mail Adresse hinterlegen');

define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_MERCHANT_NO_TITLE', 'Händler Nr. (VR pay)');
define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_MERCHANT_NO_DESC', 'Ihre Kundennummer/Händlernummer bei VR pay');

define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_SHOP_URL_TITLE', 'Shop URL');
define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_SHOP_URL_DESC', 'Shop URL hinterlegen');

define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_VERSION_TRACKER_TITLE', 'Version Tracker');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_VERSION_TRACKER_DESC',
    'When enabled, you accept to share your IP, email address, etc with Cardprocess.'
);

define(
    'VRPAYECOMMERCE_TT_VERSIONTRACKER',
    'WARNING For providing the best service to you,
    to inform you about newer versions of the plugin and also about security issues,
    VR pay is gathering some basic and technical information from the shop system (for details please see the manual).
    The information will under no circumstances be used for marketing and/or advertising purposes.
    Please be aware that deactivating the version tracker may affect the service quality
    and also important security and update information.'
);
define('VRPAYECOMMERCE_BACKEND_BT_OK', 'OK');

define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_ALLOWED_TITLE', 'Erlaubte Länder');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_ALLOWED_DESC',
    'Bitte wählen Sie die Länder aus, in den dieses Modul verfügbar sein soll.'
);
