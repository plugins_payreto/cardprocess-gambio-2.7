<?php
/* --------------------------------------------------------------
   modules.php 2015-09-28 gm
   Gambio GmbH
   http://www.gambio.de
   Copyright (c) 2015 Gambio GmbH
   Released under the GNU General Public License (Version 2)
   [http://www.gnu.org/licenses/gpl-2.0.html]

   IMPORTANT! THIS FILE IS DEPRECATED AND WILL BE REPLACED IN THE FUTURE.
   MODIFY IT ONLY FOR FIXES. DO NOT APPEND IT WITH NEW FEATURES, USE THE
   NEW GX-ENGINE LIBRARIES INSTEAD.
   --------------------------------------------------------------

   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(modules.php,v 1.45 2003/05/28); www.oscommerce.com
   (c) 2003  nextcommerce (modules.php,v 1.23 2003/08/19); www.nextcommerce.org
   (c) 2003 XT-Commerce - community made shopping
   http://www.xt-commerce.com ($Id: modules.php 1060 2005-07-21 18:32:58Z mz $)

   Released under the GNU General Public License
   --------------------------------------------------------------*/

require 'includes/application_top.php';
require dirname(__FILE__).'/../lang/'.$_SESSION['language'].'/modules/payment/vrpayecommerce_ageneral.php';
require dirname(__FILE__).'/../lang/'.$_SESSION['language'].'/original_sections/admin/modules.lang.inc.php';
define('HEADING_TITLE', $t_language_text_section_content_array['HEADING_TITLE_MODULES_PAYMENT']);
// include needed functions (for modules)

//Eingefügt um Fehler in CC Modul zu unterdrücken.
require DIR_FS_CATALOG.DIR_WS_CLASSES . 'xtcPrice.php';
$xtPrice = new xtcPrice($_SESSION['currency'], '');

?>
  <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
  <html <?php echo HTML_PARAMS; ?>>
    <head>
      <meta http-equiv="x-ua-compatible" content="IE=edge">
      <meta http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['language_charset']; ?>">
      <title><?php echo TITLE; ?></title>
      <link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
      <link href=<?php echo DIR_WS_CATALOG.'ext/modules/payment/vrpayecommerce/css/version_tracker_message.css'?>
      rel="stylesheet" type="text/css">
    </head>
    <body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0"
    leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
      <!-- header //-->
        <?php require DIR_WS_INCLUDES . 'header.php'; ?>
      <script type="text/javascript" src="gm/javascript/gm_modules.js"></script>
      <!-- header_eof //-->

      <!-- body //-->
     <table border="0" width="100%" cellspacing="2" cellpadding="2">
        <tr>
          <td class="columnLeft2" width="<?php echo BOX_WIDTH; ?>" valign="top">
            <table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
              <!-- left_navigation //-->
                <?php require DIR_WS_INCLUDES . 'column_left.php'; ?>
              <!-- left_navigation_eof //-->
            </table></td>
          <!-- body_text //-->
        <td class="boxCenter" width="100%" valign="top">
          <table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr class="hidden">
                <td>
                  <div class="pageHeading" style="background-image:url(images/gm_icons/module.png); float: left;">
                        <?php echo HEADING_TITLE; ?>
                  </div>
                  <br />
                </td>
              </tr>
              <tr>
                <td>
                  <td valign="top">
                    <div class='content'>
                      <span class='heading_content'><b><?php echo VRPAYECOMMERCE_BACKEND_GENERAL_VRPAY ?></b></span>
                      <div class='message_content'><?php echo VRPAYECOMMERCE_TT_VERSIONTRACKER ?>
                      </div>
                      <div class='btn_content'>
                        <a class="btn btn-edit btn-primary" onClick="this.blur();"
                        href=<?php echo xtc_href_link(
                            FILENAME_MODULES,
                            'set=' . $_GET['set'] . '&module=vrpayecommerce_ageneral'
                        ) ?>>
                        <?php echo VRPAYECOMMERCE_BACKEND_BT_OK ?></a>
                      </div>
                      </div><br /><br />
                    </td>
              </tr>
            </table></td>
        </tr>
    </table>
        <!-- body_eof //-->

        <!-- footer //-->
        <?php require DIR_WS_INCLUDES . 'footer.php'; ?>
        <!-- footer_eof //-->
    </body>
  </html>
<?php require DIR_WS_INCLUDES . 'application_bottom.php'; ?>
