Please follow the instructions for Gambio version 2.7

## additional instruction for add capture and refund at shop admin.
1. go to file {your shop root folder}/admin/orders.php

before line 347 :
  switch ($_GET['action']) {

add this code :
  // VR pay ecommerce add code for Capture and Refund
  require_once (DIR_FS_CATALOG.'ext/modules/payment/vrpayecommerce/backend_payment.php');


## additional instruction for change payment method name at shop admin.
1. go to file {your shop root folder}/admin/orders.php

at line 957 :
  <?php echo $order->info['payment_method']; ?>

change to this code :
  <!-- VR pay ecommerce change payment method name -->
    <?php 
      if (substr($order->info['payment_method'],0,14) == 'vrpayecommerce') 
      { 
        include(DIR_FS_CATALOG.'lang/'.$order->info['language'].'/modules/payment/'.$order->info['payment_method'].'.php');
        echo @constant('MODULE_PAYMENT_'.strtoupper($order->info['payment_method']).'_TEXT_TITLE');
      } 
      else 
      {
        echo $order->info['payment_method']; 
      } 
    ?>

at line 1705:
      <td class="dataTableContent" <?php echo $gm_td_action; ?> align="left" title="<?php echo $orders['payment_method']; ?>"><?php if(!empty($orders['payment_method'])){ echo get_payment_title($orders['payment_method']); } ?></td> 

change to this code:
      <td class="dataTableContent" <?php echo $gm_td_action; ?> align="left" title="<?php echo $orders['payment_method']; ?>"><?php if(!empty($orders['payment_method'])){
        if($orders['payment_method'] == 'vrpayecommerce_cards_saved' || $orders['payment_method'] == 'vrpayecommerce_dd_saved' || $orders['payment_method'] == 'vrpayecommerce_paypal_saved') {
          echo get_payment_title($orders['payment_method']).' (Recurring)';
        } else {
          echo get_payment_title($orders['payment_method']);
        }
        } ?>
      </td>

at line 1922 :
  $contents[] = array ('text' => '<br />'.TEXT_INFO_PAYMENT_METHOD.' '.$oInfo->payment_method);

change to this code :
    // VR pay ecommerce change payment method name
    if (substr($oInfo->payment_method,0,14) == 'vrpayecommerce')
    {
      include(DIR_FS_CATALOG.'lang/'.$_SESSION['language'].'/modules/payment/'.$oInfo->payment_method.'.php');
      $contents[] = array ('text' => '<br />'.TEXT_INFO_PAYMENT_METHOD.' '.@constant('MODULE_PAYMENT_'.strtoupper($oInfo->payment_method).'_TEXT_TITLE'));
    }
    else
    {
      $contents[] = array ('text' => '<br />'.TEXT_INFO_PAYMENT_METHOD.' '.$oInfo->payment_method); 
    }

2. go to file {your shop root folder}/admin/html/compatibility/order_details.php
at line 103:
  <label class="value" title="<?php if(!empty($GLOBALS['order']->info['payment_method'])){ echo $GLOBALS['order']->info['payment_method']; } ?>"> <?php if(!empty($GLOBALS['order']->info['payment_method'])){
          echo get_payment_title($GLOBALS['order']->info['payment_method']); } ?> </label>

change to this code:
  <label class="value" title="<?php if(!empty($GLOBALS['order']->info['payment_method'])){ echo $GLOBALS['order']->info['payment_method']; } ?>"> <?php if(!empty($GLOBALS['order']->info['payment_method'])){ 
      if($GLOBALS['order']->info['payment_method'] == 'vrpayecommerce_cards_saved' || $GLOBALS['order']->info['payment_method'] == 'vrpayecommerce_dd_saved' || $GLOBALS['order']->info['payment_method'] == 'vrpayecommerce_paypal_saved') {
          echo get_payment_title($GLOBALS['order']->info['payment_method']).' (Recurring)';
      } else {
          echo get_payment_title($GLOBALS['order']->info['payment_method']);
      }          
  } ?> </label>

## addtional instruction to add transaction ID at shop admin.
1.go to file {your shop root folder}/admin/html/compatibility/order_details.php

at line 540:
           <div class="span6">
            <div class="title">
              <label><?php echo str_replace(':', '', ENTRY_EMAIL_ADDRESS) ?></label>
            </div>
            <div class="content">
              <a href="admin.php?do=Emails&mailto=<?php echo $GLOBALS['order']->customer['email_address']; ?>">
                <?php echo $GLOBALS['order']->customer['email_address'] ?>
            </div>
          </div>

change to this code :
  <div class="span6">
      <div class="title">
        <label><?php echo str_replace(':', '', ENTRY_EMAIL_ADDRESS) ?></label>
      </div>
      <div class="content">
        <a href="admin.php?do=Emails&mailto=<?php echo $GLOBALS['order']->customer['email_address']; ?>">
          <?php echo $GLOBALS['order']->customer['email_address'] ?> </a>
      </div>
    </div>
<?php
    $query = xtc_db_query("select transaction_id from payment_vrpayecommerce_orders where orders_id = '".xtc_db_input($oID)."'");
    $order_vrpay = xtc_db_fetch_array($query);
?>
  
    <?php if ($order_vrpay['transaction_id']) { ?>
      <div class="span12">
        <div class="title">
          <label><?php echo @constant('MODULE_PAYMENT_'.strtoupper($order->info['payment_method']).'_TT_TRANSACTION_ID'); ?></label>
        </div>
        <div class="content">
          <span><?php echo $order_vrpay['transaction_id']; ?></span>
        </div>
      </div>
    <?php } ?>

<?php
  if ($order->info['payment_method'] == 'vrpayecommerce_dd' || $order->info['payment_method'] == 'vrpayecommerce_dd_saved') {
?>
<?php } ?>

## additional instruction to add text "(Recurring)" at shop admin
1. go to file {your shop root folder}/admin/gm/classes/GMModulesManager.php

before line 264:
  $t_module_data = array('code' => $coo_module->code,

add this code :
  if($coo_module->code == 'vrpayecommerce_cards_saved' || $coo_module->code == 'vrpayecommerce_dd_saved' || $coo_module->code == 'vrpayecommerce_paypal_saved')
    $coo_module->title .= ' (Recurring)';

## additional instruction for add menu my payment information at shopper account.

1. go to file {your shop root folder}/system/classes/accounts/AccountContentView.inc.php

after line 141 : 
  $this->content_array['LINK_PASSWORD'] = xtc_href_link(FILENAME_ACCOUNT_PASSWORD, '', 'SSL');

add this code :
  // VR pay ecommerce add code for recurring payment
  if (MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_RECURRING == 'True')
      $this->content_array['PAYMENT_INFORMATION'] = xtc_href_link('account_payment_information.php', '', 'SSL');

2. go to file {your shop root folder}/templates/EyeCandy/module/account.html

after line 24 : 
  <li><a href="{$LINK_DELETE_ACCOUNT}">{$txt.text_delete_account}</a></li>
  {/if}

add this code :
  <!-- VR pay ecommerce add code for recurring payment -->
  {if $PAYMENT_INFORMATION}
    {config_load file="$language/modules/payment/vrpayecommerce.conf" section="recurring"}
    <li><a href="{$PAYMENT_INFORMATION}">{#FRONTEND_MC_INFO#}</a></li>
  {/if}

## how to fix database after upgrade from v1.0.0 to v1.0.1
1. go to your gambio database
2. do this sql : DELETE FROM configuration WHERE configuration_key like 'MODULE_PAYMENT_VRPAYECOMMERCE%'


## additional instruction for payment recurring at backend order
1. go to file {your shop root folder}/admin/orders_edit.php

after line 530:
 if($_GET['action'] == "payment_edit")
 {
  $orderWriteService->updatePaymentType(new IdType($_POST['oID']), 
                                        MainFactory::create('OrderPaymentType', 
                                                            new StringType(xtc_db_prepare_input($_POST['payment'])),
                                                            new StringType(xtc_db_prepare_input($_POST['payment']))));

add this code :
  $recurring_id = 0;
  switch ($_POST['payment']) {
    case 'vrpayecommerce_cards_saved':
      $recurring_id = $_POST['cc_id'];
      break;
    case 'vrpayecommerce_dd_saved':
      $recurring_id = $_POST['dd_id'];
      break;
    case 'vrpayecommerce_paypal_saved':
      $recurring_id = $_POST['paypal_id'];
      break;
  }
  $check_recurring_query = xtc_db_query("SELECT id FROM payment_vrpayecommerce_orders WHERE orders_id = '" . (int)$_POST['oID'] . "'");
  $recurring_data_array = array('orders_id' => (int)$_POST['oID'], 'unique_id' => (int)$recurring_id, 'payment_type' => 'NO');
  if(xtc_db_num_rows($check_recurring_query))
  {
    xtc_db_perform('payment_vrpayecommerce_orders', $recurring_data_array, 'update', 'orders_id = \'' . (int)$_POST['oID'] . '\' AND payment_type="NO"');
  }
  else
  {
    xtc_db_perform('payment_vrpayecommerce_orders', $recurring_data_array);
  }

2. go to file {your shop root folder}/admin/orders_edit_other.php

after line 100 :
  $t_payment = substr($payments[$i], 0, strrpos($payments[$i], '.'));

add this code :
  if ($t_payment == 'vrpayecommerce_ageneral')
    continue;

at line 122 :
  <form class="remove-padding"
            name="payment_edit"
            action="<?php echo xtc_href_link(FILENAME_ORDERS_EDIT, 'action=payment_edit'); ?>"
            method="post">

change to this code :
  <form class="remove-padding"
            name="payment_edit"
            action="<?php echo xtc_href_link(FILENAME_ORDERS_EDIT, 'action=payment_edit'); ?>"
            method="post"
            onsubmit="return validateForm()">

after line 139:
    <div class="grid">
      <div class="span4">
        <label for="customers_company"><?php echo TEXT_NEW; ?></label>
      </div>
      <div class="span8">
        <?php echo xtc_draw_pull_down_menu('payment', $payment_array, $order_payment); ?>
      </div>
    </div>

add this code :
<div class="grid">
<script>
    var payment_selection = document.getElementsByName('payment')[0];
    payment_selection.onchange = function() {
      document.getElementById('cc_saved').style.display = "none";
      document.getElementById('dd_saved').style.display = "none";
      document.getElementById('paypal_saved').style.display = "none";
      if (payment_selection.value == 'vrpayecommerce_cards_saved')
        document.getElementById('cc_saved').style.display = "";
      if (payment_selection.value == 'vrpayecommerce_dd_saved')
        document.getElementById('dd_saved').style.display = "";
      if (payment_selection.value == 'vrpayecommerce_paypal_saved')
        document.getElementById('paypal_saved').style.display = "";     
    };
    function validateForm() {
      var name = '';name
      if (payment_selection.value == 'vrpayecommerce_cards_saved')
        name = 'cc_id';
      if (payment_selection.value == 'vrpayecommerce_dd_saved')
        name = 'dd_id';
      if (payment_selection.value == 'vrpayecommerce_paypal_saved')
        name = 'paypal_id';
        var id = document.forms["payment_edit"][name];
        if (id.checked == null) {
          if (id.value === null || id.value === "" || id.value === "undefined") {
              alert("Please select registered payment");
              return false;
            }
        }
        if (id.checked == false) {
              alert("Please select registered payment");
              return false;         
        } 
        return true;
    }
</script>
   <?php
      $query = xtc_db_query("select customers_id from orders where orders_id = '".(int)$_GET['oID']."'");  
      $row = xtc_db_fetch_array($query);
      $customer_id = $row['customers_id'];
      $recurring_id = 0;
      $get_reccuring_id_query = xtc_db_query("SELECT unique_id FROM payment_vrpayecommerce_orders WHERE orders_id = '" . (int)$_GET['oID'] . "' AND payment_type='NO'");
      if(xtc_db_num_rows($get_reccuring_id_query))
      {
        $row = xtc_db_fetch_array($get_reccuring_id_query);
        $recurring_id = (int)$row['unique_id'];
      }
      $CardsSavedChannel =  trim(@constant('MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_CHANNEL'));
      $CardsSavedServerMode =  trim(@constant('MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_SERVER_MODE'));
      $DDSavedChannel =  trim(@constant('MODULE_PAYMENT_VRPAYECOMMERCE_DD_SAVED_CHANNEL'));
      $DDSavedServerMode =  trim(@constant('MODULE_PAYMENT_VRPAYECOMMERCE_DD_SAVED_SERVER_MODE'));
      $PayPalSavedChannel =  trim(@constant('MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_CHANNEL'));
      $PayPalSavedServerMode =  trim(@constant('MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_SERVER_MODE'));
  ?>
    <div class="span12" id="cc_saved" <?php if ($order_payment != 'vrpayecommerce_cards_saved') echo 'style="display:none"'; ?>>
        <?php 
          $query = xtc_db_query("SELECT * from payment_vrpayecommerce_recurring WHERE cust_id = '".$customer_id."' AND payment_group = 'CC' AND server_mode = '".$CardsSavedServerMode."' AND channel_id = '".$CardsSavedChannel."'");
          if(xtc_db_num_rows($query))
          {
            while ($row = xtc_db_fetch_array($query)) {
              $checked= '';
              if ($row['id'] == $recurring_id)
                $checked = 'checked="checked"';
        ?>
              <input type="radio" name="cc_id" value="<?=$row['id']?>" <?php echo $checked; ?>><img valign="middle" height="35" src="../ext/modules/payment/vrpayecommerce/images/<?php echo strtolower($row['brand']).'.png';?>"><?php echo FRONTEND_MC_ENDING.' '.$row['last4digits'].' '.FRONTEND_MC_VALIDITY.' '.$row['expiry_month'].'/'.$row['expiry_year'];?><br/>
        <?php 
            }
          } 
        ?>
      </div>
      <div class="span12" id="dd_saved" <?php if ($order_payment != 'vrpayecommerce_dd_saved') echo 'style="display:none"'; ?>>
        <?php 
          $query = xtc_db_query("SELECT * from payment_vrpayecommerce_recurring WHERE cust_id = '".$customer_id."' AND payment_group = 'DD' AND server_mode = '".$DDSavedServerMode."' AND channel_id = '".$DDSavedChannel."'");
          if(xtc_db_num_rows($query))
          {
            while ($row = xtc_db_fetch_array($query)) {
              $checked= '';
              if ($row['id'] == $recurring_id)
                $checked = 'checked="checked"';
        ?>
              <input type="radio" name="dd_id" value="<?=$row['id']?>" <?php echo $checked; ?>><img valign="middle" height="35" src="../ext/modules/payment/vrpayecommerce/images/sepa.png"><?php echo FRONTEND_MC_ACCOUNT.' '.$row['last4digits'];?><br/>
        <?php 
            }
          } 
        ?>
      </div>
      <div class="span12" id="paypal_saved" <?php if ($order_payment != 'vrpayecommerce_paypal_saved') echo 'style="display:none"'; ?>>
        <?php 
          $query = xtc_db_query("SELECT * from payment_vrpayecommerce_recurring WHERE cust_id = '".$customer_id."' AND payment_group = 'OT' AND server_mode = '".$PayPalSavedServerMode."' AND channel_id = '".$PayPalSavedChannel."'");
          if(xtc_db_num_rows($query))
          {
            while ($row = xtc_db_fetch_array($query)) {
              $checked= '';
              if ($row['id'] == $recurring_id)
                $checked = 'checked="checked"';
        ?>
              <input type="radio" name="paypal_id" value="<?=$row['id']?>" <?php echo $checked; ?>><img valign="middle" height="35" src="../ext/modules/payment/vrpayecommerce/images/paypal.png"><?php echo FRONTEND_MC_EMAIL.' '.$row['email'];?><br/>
        <?php 
            }
          } 
        ?>
      </div>
  </div>

---------------------------------------------------------------------------------
How to get status :
1. Go to admin
2. Select menu CUSTOMERS->Orders
3. Edit orders that have status In Review.
4. Select Status to In Review
5. Click UPDATE button.

---------------------------------------------------------------------------------
How to create order from admin :
1. Go to admin
2. Select menu CUSTOMERS->Customers
3. Click button NEW ORDER
4. Click button EDIT
5. To change address click button EDIT ADDRESS
5. To add products click button EDIT PRODUCTS
6. To add payment method, shipping, etc click button EDIT TOTALS
7. On Tab PAYMENT select payment methods then select registered payments then click button SAVE
8. On Tab SHIPPING select shipping method then input price then click button SAVE
9. Checklist Finish and recalculate then click button CLOSE
10. To make payment change Status from pending to Pre-Authorization of payment / Payment Accepted then click button UPDATE
----------------------------------------------------------------------------------

Notes:
By default this plugin supports only the default theme of Gambio shopsystem (EyeCandy).
If you want to use a different/customized theme, please copy everything under EyeCandy folder to your theme folder.