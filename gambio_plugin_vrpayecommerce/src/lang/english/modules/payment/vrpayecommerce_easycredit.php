<?php

define('MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_TEXT_TITLE', 'Easycredit');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_TEXT_DESCRIPTION',
    '<a href="http://www.vrpayecommerce.de/" target="_blank" style="text-decoration: underline; font-weight: bold;">
    <img src="../ext/modules/payment/vrpayecommerce/images/logovrpay.jpg" border="0">
    </a></br><p>VR pay eCommerce - Easycredit</p>'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_STATUS_TITLE', 'Enable VR pay eCommerce Easycredit Module');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_STATUS_DESC',
    'Please activate the module, if you want to accept Easycredit transactions of VR pay eCommerce.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_SERVER_MODE_TITLE', 'Server');
define('MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_SERVER_MODE_DESC', 'Choose the server for Easycredit.');

define('MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_CHANNEL_TITLE', 'Entity-ID');
define('MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_CHANNEL_DESC', 'Insert the Entity-ID for Easycredit.');

define('MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_ORDER_STATUS_ID_TITLE', 'Set Order Status for Accepted payment');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_ORDER_STATUS_ID_DESC',
    'Define here the Order Status which should be used for Accepted Payments'
);

define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_PA_ORDER_STATUS_ID_TITLE',
    'Set the Order Status for Pre-Authorization'
);
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_PA_ORDER_STATUS_ID_DESC',
    'Define here the Order Status which should be used for Pre-Authorized Payments.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_RF_ORDER_STATUS_ID_TITLE', 'Set Order Status for Refund');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_RF_ORDER_STATUS_ID_DESC',
    'Define here the Order Status which should be used for Refund'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_ZONE_TITLE', 'Payment Zone');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_ZONE_DESC',
    'When a country is selected, this payment method will be enabled for that zone only.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_ALLOWED_TITLE', 'Allowed Countries');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_ALLOWED_DESC',
    'Please enter the countries <b>individually</b>
    that should be allowed to use this module (e.g. US, UK (leave blank to allow all countries)).'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_SORT_ORDER_TITLE', 'Display Sort Order');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_SORT_ORDER_DESC',
    'Display sort order; the lowest value is displayed first.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_TT_TRANSACTION_ID', 'Transaction ID');

define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_TEXT_ERROR_CREDENTIALS',
    'VR pay eCommerce General Setting must be filled'
);

define(
    'ERROR_EASYCREDIT_FUTURE_DOB',
    'Date of birth should not be set in the future'
);

define(
    'ERROR_MESSAGE_EASYCREDIT_AMOUNT_NOTALLOWED',
    'The financing amount is outside the permitted amounts (200 - 3,000 EUR)'
);

define(
    'ERROR_EASYCREDIT_BILLING_NOTEQUAL_SHIPPING',
    'In order to be able to pay with easyCredit, the delivery address must match the invoice address.'
);

define(
    'FRONTEND_EASYCREDIT_LINK',
    'Read pre-contractual information on Installments'
);

define(
    'ERROR_EASYCREDIT_PARAMETER_DOB',
    'Please enter your date of birth in order to use easyCredit.'
);

define(
    'ERROR_MESSAGE_EASYCREDIT_PARAMETER_GENDER',
    'Please enter your gender to make payment with easyCredit.'
);
