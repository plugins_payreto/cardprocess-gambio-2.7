<?php
/* 
* Should be included in admin/orders.php, line 153 (before 'switch ($_GET['action']) {')
*/

$payment_methods_query = xtc_db_query(
    "select payment_method from ".TABLE_ORDERS."
    where orders_id = '".(int) $_POST['gm_multi_status'][0]."'"
);
$payment_methods_result  = xtc_db_fetch_array($payment_methods_query);

$payment_method = $payment_methods_result['payment_method'];

if (substr($payment_method, 0, 14) == "vrpayecommerce") {
    include_once DIR_FS_CATALOG. 'lang/'.$_SESSION['language'].'/modules/payment/vrpayecommerce_error.php';
    include_once DIR_FS_CATALOG. 'includes/modules/payment/'.$payment_method.'.php';
    $current_payment = new $payment_method;
    $accept_status = $current_payment->getAcceptStatus();
    $pa_status = $current_payment->getPaStatus();
    $refund_status = $current_payment->getRefundStatus();
    $inreview_status = $current_payment->getReviewStatus();

    switch ($_GET['action']) {
        case 'gm_multi_status':
            $order_updated = false;
            $status = xtc_db_prepare_input($_POST['gm_status']);
            $comments = xtc_db_prepare_input($_POST['gm_comments']);

            for ($i = 0; $i < count($_POST['gm_multi_status']); $i++) {
                $oID = xtc_db_prepare_input($_POST['gm_multi_status'][$i]);
            
                $check_status_query = xtc_db_query(
                    "
                                                    SELECT
                                                        o.customers_name,
                                                        o.customers_gender,
                                                        o.customers_email_address,
                                                        o.orders_status,
                                                        o.language,
                                                        o.date_purchased,
                                                        l.languages_id
                                                    FROM " .
                                                    TABLE_ORDERS . " o
                                                    LEFT JOIN languages AS l ON (o.language = l.directory)
                                                    WHERE
                                                        orders_id = '" . xtc_db_input($oID) . "'
                                                    "
                );

                $check_status = xtc_db_fetch_array($check_status_query);

                if (($check_status['orders_status'] != $status
                    && $check_status['orders_status'] != gm_get_conf('GM_ORDER_STATUS_CANCEL_ID'))
                    || ($check_status['orders_status'] == $inreview_status && $status == $inreview_status)
                    || $comments != ''
                ) {
                    if (xtc_db_input($status) == gm_get_conf('GM_ORDER_STATUS_CANCEL_ID')) {
                        $gm_update = "gm_cancel_date = now(),";
                    }
                    
                        // vrpayecommerce process create order, capture, refund, in review
                    if ($check_status['orders_status'] == '1' && $status == $pa_status) {
                        $update = $current_payment->backendCreateOrder($oID, 'PA', $error_identifier);
                    } elseif ($check_status['orders_status'] == '1' && $status == $accept_status) {
                        $update = $current_payment->backendCreateOrder($oID, 'DB', $error_identifier);
                    } elseif ($check_status['orders_status'] == $pa_status && $status == $accept_status) {
                        $update = $current_payment->backendPaymentAction($oID, 'CP', $error_identifier);
                    } elseif ($check_status['orders_status'] == $accept_status && $status == $refund_status) {
                        $update = $current_payment->backendPaymentAction($oID, 'RF', $error_identifier);
                    } elseif ($check_status['orders_status'] == $inreview_status && $status == $inreview_status) {
                        $update = $current_payment->backendUpdateStatus($oID, $paymentType, $error_identifier);
                        if ($paymentType == 'PA') {
                            $status = $pa_status;
                        } elseif ($paymentType == 'DB') {
                            $status = $accept_status;
                        }
                    } else {
                        $update = true;
                    }

                    if ($update) {
                        // ============================

                        xtc_db_query(
                            "
                                UPDATE " .
                              TABLE_ORDERS . "
                                SET
                                  " . $gm_update . "
                                  orders_status = '".xtc_db_input($status)."',
                                  last_modified = now()
                                WHERE
                                  orders_id = '".xtc_db_input($oID)."'
                                "
                        );

                        // cancel order
                        if (xtc_db_input($gm_status) == gm_get_conf('GM_ORDER_STATUS_CANCEL_ID')) {
                                    xtc_remove_order(xtc_db_input($oID), true, true);
                        }

                        $customer_notified = '0';
                        if ($_POST['gm_notify'] == 'on') {
                                    $notify_comments = '';
                            if ($_POST['gm_notify_comments'] == 'on') {
                                //$gm_notify_comments = sprintf(EMAIL_TEXT_COMMENTS_UPDATE, $comments)."\n\n";
                                $gm_notify_comments = $comments;
                            } else {
                                $gm_notify_comments = '';
                            }

                                    // assign language to template for caching
                                    $smarty->assign('language', $_SESSION['language']);
                                    $smarty->caching = false;

                                    // set dirs manual
                                    $smarty->template_dir = DIR_FS_CATALOG.'templates';
                                    $smarty->compile_dir = DIR_FS_CATALOG.'templates_c';
                                    $smarty->config_dir = DIR_FS_CATALOG.'lang';

                                    $smarty->assign('tpl_path', 'templates/'.CURRENT_TEMPLATE.'/');
                                    $smarty->assign(
                                        'logo_path',
                                        HTTP_SERVER.DIR_WS_CATALOG.'templates/'.CURRENT_TEMPLATE.'/img/'
                                    );

                                    $smarty->assign('NAME', $check_status['customers_name']);
                                    $smarty->assign('GENDER', $order->customer['gender']);
                                    $smarty->assign('ORDER_NR', $oID);
                                    $smarty->assign(
                                        'ORDER_LINK',
                                        xtc_catalog_href_link(
                                            FILENAME_CATALOG_ACCOUNT_HISTORY_INFO,
                                            'order_id='.$oID,
                                            'SSL'
                                        )
                                    );
                                    $smarty->assign('ORDER_DATE', xtc_date_long($check_status['date_purchased']));

                                    $smarty->assign('ORDER_STATUS', $orders_status_array[$status]);

                            if (defined('EMAIL_SIGNATURE')) {
                                                $smarty->assign('EMAIL_SIGNATURE_HTML', nl2br(EMAIL_SIGNATURE));
                                                $smarty->assign('EMAIL_SIGNATURE_TEXT', EMAIL_SIGNATURE);
                            }

                                    // START Parcel Tracking Code
                                    /**
*
                         *
 * @var ParcelTrackingCode $coo_parcel_tracking_code_item
*/
                                    $coo_parcel_tracking_code_item = MainFactory::create_object('ParcelTrackingCode');
                                    /**
*
                         *
 * @var ParcelTrackingCodeReader $coo_parcel_tracking_code_reader
*/
                                    $coo_parcel_tracking_code_reader =
                                        MainFactory::create_object('ParcelTrackingCodeReader');
                                    $t_parcel_tracking_codes_array =
                                    $coo_parcel_tracking_code_reader->getTackingCodeItemsByOrderId(
                                        $coo_parcel_tracking_code_item,
                                        $oID
                                    );
                                    $smarty->assign('PARCEL_TRACKING_CODES_ARRAY', $t_parcel_tracking_codes_array);
                                    $send_parcel_tracking_codes = '';
                            if ($_POST['send_parcel_tracking_codes'] == 'on') {
                                                $send_parcel_tracking_codes = 'true';
                            }
                                    $smarty->assign('PARCEL_TRACKING_CODES', $send_parcel_tracking_codes);
                                    // END Parcel Tracking Code

                                    // (xycons.de - Additional Extenders) (START)
                                    $coo_order_status_mail_extender_component->set_data(
                                        'action',
                                        $_GET['action']
                                    );
                                    $coo_order_status_mail_extender_component->proceed();
                            if (is_array($coo_order_status_mail_extender_component->v_output_buffer)) {
                                $output_buffer = $coo_order_status_mail_extender_component->v_output_buffer;
                                foreach ($output_buffer as $t_key => $t_value) {
                                    $smarty->assign($t_key, $t_value);
                                }
                            }
                                    // (xycons.de - Additional Extenders) (END)

                                    $smarty->assign('NOTIFY_COMMENTS', nl2br($notify_comments));
                                    $html_mail = fetch_email_template($smarty, 'change_order_mail', 'html');
                                    $smarty->assign('NOTIFY_COMMENTS', $notify_comments);
                                    $txt_mail = fetch_email_template($smarty, 'change_order_mail', 'txt');

                                    // BOF GM_MOD

                            if ($_SESSION['language'] == 'german') {
                                                xtc_php_mail(
                                                    EMAIL_BILLING_ADDRESS,
                                                    EMAIL_BILLING_NAME,
                                                    $check_status['customers_email_address'],
                                                    $check_status['customers_name'],
                                                    '',
                                                    EMAIL_BILLING_REPLY_ADDRESS,
                                                    EMAIL_BILLING_REPLY_ADDRESS_NAME,
                                                    '',
                                                    '',
                                                    'Ihre Bestellung '.$oID.',
                                    '.xtc_date_long($check_status['date_purchased']).',
                                    '.$check_status['customers_name'],
                                                    $html_mail,
                                                    $txt_mail
                                                );
                            } else {
                                                xtc_php_mail(
                                                    EMAIL_BILLING_ADDRESS,
                                                    EMAIL_BILLING_NAME,
                                                    $check_status['customers_email_address'],
                                                    $check_status['customers_name'],
                                                    '',
                                                    EMAIL_BILLING_REPLY_ADDRESS,
                                                    EMAIL_BILLING_REPLY_ADDRESS_NAME,
                                                    '',
                                                    '',
                                                    'Your Order '.$oID.',
                                    '.xtc_date_long($check_status['date_purchased']).',
                                    '.$check_status['customers_name'],
                                                    $html_mail,
                                                    $txt_mail
                                                );
                            }
                                    // EOF GM_MOD

                                    $customer_notified = '1';
                        }

                        xtc_db_query(
                            "insert into " . TABLE_ORDERS_STATUS_HISTORY . "(
                            orders_id,
                            orders_status_id,
                            date_added,
                            customer_notified,
                            comments
                            ) values (
                            '".xtc_db_input($oID)."',
                            '".xtc_db_input($status)."',
                            now(),
                            '".$customer_notified."',
                            '".xtc_db_input($comments)."')"
                        );
                        $order_updated = true;
                    }
                }
            }

            if ($order_updated) {
                $messageStack->add_session(SUCCESS_ORDER_UPDATED, 'success');
            } else {
                $messageStack->add_session(
                    WARNING_ORDER_NOT_UPDATED.' '.defined($error_identifier) ? constant($error_identifier) : '',
                    'warning'
                );
            }

                $coo_order_action_extender_component->set_data('order_updated', $order_updated);

                xtc_redirect(xtc_href_link(FILENAME_ORDERS, xtc_get_all_get_params(array ('action')).'action=edit'));

            break;
    }
}
