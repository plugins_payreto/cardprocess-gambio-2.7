<?php

require_once 'includes/application_top.php';
require DIR_WS_LANGUAGES.$_SESSION['language'].'/modules/payment/vrpayecommerce_info.php';

$GLOBALS['breadcrumb']->add(NAVBAR_TITLE_1_ACCOUNT_PASSWORD, xtc_href_link(FILENAME_ACCOUNT, '', 'SSL'));
$GLOBALS['breadcrumb']->add(FRONTEND_MC_INFO, xtc_href_link('account_payment_information.php', '', 'SSL'));

$set_default = xtc_db_prepare_input($_POST['set_default']);
$id = xtc_db_prepare_input($_POST['id']);
$payment_group = xtc_db_prepare_input($_POST['payment_group']);

if ($set_default) {
    xtc_db_query(
        "UPDATE payment_vrpayecommerce_recurring SET payment_default = '0'
        WHERE payment_default = '1' AND payment_group = '".xtc_db_input($payment_group)."'"
    );
    xtc_db_query(
        "UPDATE payment_vrpayecommerce_recurring SET payment_default = '1'
        WHERE id = '".(int) $id."' AND payment_group = '".xtc_db_input($payment_group)."'"
    );
}

$is_recurring_active = MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_RECURRING == 'True';
$is_cards_saved_active = MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_STATUS == 'True';
$is_dd_saved_active = MODULE_PAYMENT_VRPAYECOMMERCE_DD_SAVED_STATUS == 'True';
$is_paypal_saved_active = MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_STATUS == 'True';

$smarty = new smarty;
$smarty->assign('is_recurring_active', $is_recurring_active);
$smarty->assign('is_cards_saved_active', $is_cards_saved_active);
$smarty->assign('is_dd_saved_active', $is_dd_saved_active);
$smarty->assign('is_paypal_saved_active', $is_paypal_saved_active);

if (isset($_GET['success'])) {
    $smarty->assign('success_message', $_GET['success']);
}

if (isset($_GET['error'])) {
    $smarty->assign('error_message', $_GET['error']);
}

if ($is_recurring_active) {
    if ($is_cards_saved_active) {
        $cards_saved_channel =
        defined('MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_CHANNEL')
        ? MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_CHANNEL : '';
        $cards_saved_server_mode =
        defined('MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_SERVER_MODE')
        ? MODULE_PAYMENT_VRPAYECOMMERCE_CARDS_SAVED_SERVER_MODE : '';
        $query = xtc_db_query(
            "SELECT * from payment_vrpayecommerce_recurring
            WHERE cust_id = '".(int) $_SESSION['customer_id']."'
            AND payment_group = 'CC'
            AND server_mode = '".$cards_saved_server_mode."'
            AND channel_id = '".$cards_saved_channel."'"
        );
        while ($row = xtc_db_fetch_array($query)) {
            $customer_data_cc[] = $row;
        }
        $smarty->assign('customer_data_cc', $customer_data_cc);
    }
    if ($is_dd_saved_active) {
        $dd_saved_channel =
        defined('MODULE_PAYMENT_VRPAYECOMMERCE_DD_SAVED_CHANNEL')
        ? MODULE_PAYMENT_VRPAYECOMMERCE_DD_SAVED_CHANNEL : '';
        $dd_saved_server_mode =
        defined('MODULE_PAYMENT_VRPAYECOMMERCE_DD_SAVED_SERVER_MODE')
        ? MODULE_PAYMENT_VRPAYECOMMERCE_DD_SAVED_SERVER_MODE : '';
        $query = xtc_db_query(
            "SELECT * from payment_vrpayecommerce_recurring
            WHERE cust_id = '".(int) $_SESSION['customer_id']."'
            AND payment_group = 'DD'
            AND server_mode = '".$dd_saved_server_mode."'
            AND channel_id = '".$dd_saved_channel."'"
        );
        while ($row = xtc_db_fetch_array($query)) {
            $customer_data_dd[] = $row;
        }
        $smarty->assign('customer_data_dd', $customer_data_dd);
    }
    if ($is_paypal_saved_active) {
        $paypal_saved_channel =
        defined('MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_CHANNEL')
        ? MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_CHANNEL : '';
        $paypal_saved_server_mode =
        defined('MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_SERVER_MODE')
        ? MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_SERVER_MODE : '';
        $query = xtc_db_query(
            "SELECT * from payment_vrpayecommerce_recurring
            WHERE cust_id = '".(int) $_SESSION['customer_id']."'
            AND payment_group = 'OT'
            AND server_mode = '".$paypal_saved_server_mode."'
            AND channel_id = '".$paypal_saved_channel."'"
        );
        while ($row = xtc_db_fetch_array($query)) {
            $customer_data_paypal[] = $row;
        }
        $smarty->assign('customer_data_paypal', $customer_data_paypal);
    }
}

$smarty->assign('language', $_SESSION['language']);
$t_main_content = $smarty->fetch(CURRENT_TEMPLATE.'/module/payment/vrpayecommerce/payment_information.html');

$coo_layout_control = MainFactory::create_object('LayoutContentControl');
$coo_layout_control->set_data('GET', $_GET);
$coo_layout_control->set_data('POST', $_POST);
$coo_layout_control->set_('coo_breadcrumb', $GLOBALS['breadcrumb']);
$coo_layout_control->set_('coo_product', $GLOBALS['product']);
$coo_layout_control->set_('coo_xtc_price', $GLOBALS['xtPrice']);
$coo_layout_control->set_('c_path', $GLOBALS['cPath']);
$coo_layout_control->set_('main_content', $t_main_content);
$coo_layout_control->set_('request_type', $GLOBALS['request_type']);
$coo_layout_control->proceed();

$t_redirect_url = $coo_layout_control->get_redirect_url();
if (empty($t_redirect_url) === false) {
    xtc_redirect($t_redirect_url);
} else {
    echo $coo_layout_control->get_response();
}
