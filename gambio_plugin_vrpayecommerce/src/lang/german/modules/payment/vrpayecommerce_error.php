<?php

define(
    'ERROR_CC_ACCOUNT',
    'Sie sind nicht der Inhaber des eingegebenen Kontos.
    Bitte w&auml;hlen Sie ein Konto das auf Ihren Namen l&auml;uft.'
);
define(
    'ERROR_CC_INVALIDDATA',
    'Ihre Karten-/Kontodaten sind leider nicht korrekt. Bitte versuchen Sie es erneut.'
);
define(
    'ERROR_CC_BLACKLIST',
    'Leider kann die eingegebene Kreditkarte nicht akzeptiert werden.
    Bitte w&auml;hlen Sie eine andere Karte oder Bezahlungsmethode.'
);
define(
    'ERROR_CC_DECLINED_CARD',
    'Leider kann die eingegebene Kreditkarte nicht akzeptiert werden.
    Bitte w&auml;hlen Sie eine andere Karte oder Bezahlungsmethode.'
);
define(
    'ERROR_CC_EXPIRED',
    'Leider ist die eingegebene Kreditkarte abgelaufen.
    Bitte w&auml;hlen Sie eine andere Karte oder Bezahlungsmethode.'
);
define(
    'ERROR_CC_INVALIDCVV',
    'Leider ist die eingegebene Kartenpr&uuml;fnummer nicht korrekt. Bitte versuchen Sie es erneut.'
);
define(
    'ERROR_CC_EXPIRY',
    'Leider ist das eingegebene Ablaufdatum nicht korrekt. Bitte versuchen Sie es erneut.'
);
define(
    'ERROR_CC_LIMIT_EXCEED',
    'Leider &uuml;bersteigt der zu zahlende Betrag das Limit Ihrer Kreditkarte.
    Bitte w&auml;hlen Sie eine andere Karte oder Bezahlsmethode.'
);
define(
    'ERROR_CC_3DAUTH',
    'Ihr Passwort wurde leider nicht korrekt eingegeben. Bitte versuchen Sie es erneut.'
);
define(
    'ERROR_CC_3DERROR',
    'Leider gab es einen Fehler bei der Durchf&uuml;hrung Ihrer Zahlung. Bitte versuchen Sie es erneut.'
);
define(
    'ERROR_CC_NOBRAND',
    'Leider gab es einen Fehler bei der Durchf&uuml;hrung Ihrer Zahlung. Bitte versuchen Sie es erneut.'
);
define(
    'ERROR_GENERAL_LIMIT_AMOUNT',
    'Leider &uuml;bersteigt der zu zahlende Betrag Ihr Limit. Bitte w&auml;hlen Sie eine andere Bezahlsmethode.'
);
define(
    'ERROR_GENERAL_LIMIT_TRANSACTIONS',
    'Leider &uuml;bersteigt der zu zahlende Betrag Ihr Transaktionslimit.
    Bitte w&auml;hlen Sie eine andere Bezahlsmethode.'
);
define(
    'ERROR_CC_DECLINED_AUTH',
    'Leider ist Ihre Zahlung fehlgeschlagen. Bitte versuchen Sie es erneut.'
);
define(
    'ERROR_GENERAL_DECLINED_RISK',
    'Leider ist Ihre Zahlung fehlgeschlagen. Bitte versuchen Sie es erneut.'
);
define(
    'ERROR_CC_ADDRESS',
    'Leider konnten wir Ihre Kartendaten nicht akzeptieren.
    Ihre Adresse stimmt nicht mit der Herkunft Ihrer Karte &uuml;berein.'
);
define(
    'ERROR_GENERAL_CANCEL',
    'Der Vorgang wurde auf Ihren Wunsch abgebrochen. Bitte versuchen Sie es erneut.'
);
define(
    'ERROR_CC_RECURRING',
    'F&uuml;r die gewählte Karte wurden wiederkehrende Zahlungen deaktiviert.
    Bitte w&auml;len Sie eine andere Bezahlmethode.'
);
define(
    'ERROR_CC_REPEATED',
    'Leider ist Ihre Zahlung fehlgeschlagen, da Sie mehrfach fehlerhafte Angaben gemacht haben.
    Bitte w&auml;len Sie eine andere Bezahlmethode.'
);
define(
    'ERROR_GENERAL_ADDRESS',
    'Leider ist Ihre Zahlung fehlgeschlagen. Bitte kontrollieren Sie Ihre pers&ouml;nlichen Angaben.'
);
define(
    'ERROR_GENERAL_BLACKLIST',
    'Die gew&auml;hlte Bezahlmethode steht leider nicht zur Verfügung. Bitte w&auml;len Sie eine andere Bezahlmethode.'
);
define(
    'ERROR_GENERAL_GENERAL',
    'Leider konnten wir Ihre Transaktion nicht durchf&uuml;hren. Bitte versuchen Sie es erneut.'
);
define(
    'ERROR_GENERAL_TIMEOUT',
    'Leider konnten wir Ihre Transaktion nicht durchf&uuml;hren. Bitte versuchen Sie es erneut.'
);
define(
    'ERROR_GIRO_NOSUPPORT',
    'Giropay wird leider f&uuml;r diese Transaktion nicht unterstützt. Bitte w&auml;len Sie eine andere Bezahlmethode.'
);
define(
    'ERROR_UNKNOWN',
    'Leider konnten wir Ihre Transaktion nicht durchf&uuml;hren. Bitte versuchen Sie es erneut.'
);
define(
    'ERROR_GENERAL_NORESPONSE',
    'Leider konnte ihre Zahlung nicht best&auml;tigt werden. Bitte setzen Sie sich mit dem Händler in Verbindung.'
);
define(
    'ERROR_GENERAL_FRAUD_DETECTION',
    'Leider kam es bei der Abwicklung Ihrer Bestellung zu einem Fehler.
    Eventuell geleistete Zahlungen werden erstattet.'
);
define(
    'ERROR_GENERAL_REDIRECT',
    'Leider kam es zu einem Fehler bei der Weiterleitung'
);
define(
    'ERROR_GENERAL_PROCESSING',
    'Bei der Bearbeitung ist ein Fehler aufgetreten'
);
define(
    'ERROR_CAPTURE_BACKEND',
    'Die Transaktion kann nicht gecaptured werden'
);
define(
    'ERROR_REORDER_BACKEND',
    'Der Kunde hat seine Bank angewiesen, keine wiederkehrenden Zahlungen mehr zuzulassen.'
);
define(
    'ERROR_REFUND_BACKEND',
    'Die Transaktion kann nicht refunded oder reversed werden'
);
define(
    'ERROR_RECEIPT_BACKEND',
    'Das Receipt kann nicht prozessiert werden'
);
define(
    'ERROR_ADDRESS_PHONE',
    'Leider ist Ihre Zahlung fehlgeschlagen. Bitte geben Sie eine korrekte Telefonnummer an.'
);
define(
    'ERROR_MERCHANT_SSL_CERTIFICATE',
    'SSL-Zertifikat Problem, wenden Sie sich bitte an den Händler.'
);
