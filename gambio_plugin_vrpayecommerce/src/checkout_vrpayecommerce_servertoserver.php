<?php

require 'includes/application_top.php';
require_once DIR_WS_MODULES. 'payment/'. $_GET['payment_module']. '.php';
$classname = ucfirst($_GET['payment_module']);
$current_payment = new $classname;

$current_payment->capturePayment();
