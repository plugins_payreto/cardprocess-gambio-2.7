<?php

define(
    'ERROR_CC_ACCOUNT',
    'The account holder entered does not match your name. Please use an account that is registered on your name.'
);
define(
    'ERROR_CC_INVALIDDATA',
    'Unfortunately, the card/account data you entered was not correct. Please try again.'
);
define(
    'ERROR_CC_BLACKLIST',
    'Unfortunately, the credit card you entered can not be accepted. Please choose a different card or payment method.'
);
define(
    'ERROR_CC_DECLINED_CARD',
    'Unfortunately, the credit card you entered can not be accepted. Please choose a different card or payment method.'
);
define(
    'ERROR_CC_EXPIRED',
    'Unfortunately, the credit card you entered is expired. Please choose a different card or payment method.'
);
define(
    'ERROR_CC_INVALIDCVV',
    'Unfortunately, the CVV/CVC you entered is not correct. Please try again.'
);
define(
    'ERROR_CC_EXPIRY',
    'Unfortunately, the expiration date you entered is not correct. Please try again.'
);
define(
    'ERROR_CC_LIMIT_EXCEED',
    'Unfortunately, the limit of your credit card is exceeded. Please choose a different card or payment method.'
);
define(
    'ERROR_CC_3DAUTH',
    'Unfortunately, the password you entered was not correct. Please try again.'
);
define(
    'ERROR_CC_3DERROR',
    'Unfortunately, there has been an error while processing your request. Please try again.'
);
define(
    'ERROR_CC_NOBRAND',
    'Unfortunately, there has been an error while processing your request. Please try again.'
);
define(
    'ERROR_GENERAL_LIMIT_AMOUNT',
    'Unfortunately, your credit limit is exceeded. Please choose a different card or payment method.'
);
define(
    'ERROR_GENERAL_LIMIT_TRANSACTIONS',
    'Unfortunately, your limit of transaction is exceeded. Please try again later. '
);
define(
    'ERROR_CC_DECLINED_AUTH',
    'Unfortunately, your transaction has failed. Please choose a different card or payment method.'
);
define(
    'ERROR_GENERAL_DECLINED_RISK',
    'Unfortunately, your transaction has failed. Please choose a different card or payment method.'
);
define(
    'ERROR_CC_ADDRESS',
    'We are sorry. We could no accept your card as its origin does not match your address.'
);
define(
    'ERROR_GENERAL_CANCEL',
    'You cancelled the payment prior to its execution. Please try again.'
);
define(
    'ERROR_CC_RECURRING',
    'Recurring transactions have been deactivated for this credit card.
    Please choose a different card or payment method.'
);
define(
    'ERROR_CC_REPEATED',
    'Unfortunately, your transaction has been declined due to invalid data.
    Please choose a different card or payment method.'
);
define(
    'ERROR_GENERAL_ADDRESS',
    'Unfortunately, your transaction has failed. Please check the personal data you entered.'
);
define(
    'ERROR_GENERAL_BLACKLIST',
    'The chosen payment method is not available at the moment.
    Please choose a different card or payment method.'
);
define(
    'ERROR_GENERAL_GENERAL',
    'Unfortunately, your transaction has failed. Please try again.'
);
define(
    'ERROR_GENERAL_TIMEOUT',
    'Unfortunately, your transaction has failed. Please try again. '
);
define(
    'ERROR_GIRO_NOSUPPORT',
    'Giropay is not supported for this transaction. Please choose a different payment method.'
);
define(
    'ERROR_UNKNOWN',
    'Unfortunately, your transaction has failed. Please try again.'
);
define(
    'ERROR_GENERAL_NORESPONSE',
    'Unfortunately, the confirmation of your payment failed.
    Please contact your merchant for clarification.'
);
define(
    'ERROR_GENERAL_FRAUD_DETECTION',
    'Unfortunately, there was an error while processing your order.
    In case a payment has been made, it will be automatically refunded.'
);
define(
    'ERROR_GENERAL_REDIRECT',
    'Error before redirect'
);
define(
    'ERROR_GENERAL_PROCESSING',
    'An error occurred while processing'
);
define(
    'ERROR_CAPTURE_BACKEND',
    'Transaction can not be captured'
);
define(
    'ERROR_REORDER_BACKEND',
    'Card holder has advised his bank to stop this recurring payment'
);
define(
    'ERROR_REFUND_BACKEND',
    'Transaction can not be refunded or reversed.'
);
define(
    'ERROR_RECEIPT_BACKEND',
    'Receipt can not be performed'
);
define(
    'ERROR_ADDRESS_PHONE',
    'Unfortunately, your transaction has failed. Please enter a valid telephone number.'
);
define(
    'ERROR_MERCHANT_SSL_CERTIFICATE',
    'SSL certificate problem, please contact the merchant.'
);
