<?php
/**
 * VersionTracker class to create payment widget, send payment request and receive payment response from payment gateway
 *
 * @package    modules.payment
 * @subpackage vrpayecommerce
 * @since      1.0.0
 */

require_once dirname(__FILE__).'/core.php';

class VersionTracker
{
    private static $version_tracker_url = 'http://api.dbserver.payreto.eu/v1/tracker';

    /**
     * Get version tracker URL
     *
     * @return string
     */
    private static function getVersionTrackerUrl()
    {
        return self::$version_tracker_url;
    }

    /**
     * Get version tracker parameter
     *
     * @param  array $version_data
     * @return string
     */
    private static function getVersionTrackerParameter($version_data)
    {
        $data = 'transaction_mode=' .$version_data['transaction_mode'].
                '&ip_address=' .$version_data['ip_address'].
                '&shop_version=' .$version_data['shop_version'].
                '&plugin_version=' .$version_data['plugin_version'].
                '&client=' .$version_data['client'].
                '&hash=' .md5($version_data['shop_version'].$version_data['plugin_version'].$version_data['client']);

        if ($version_data['shop_system']) {
            $data .= '&shop_system=' .$version_data['shop_system'];
        }
        if ($version_data['email']) {
            $data .= '&email=' .$version_data['email'];
        }
        if ($version_data['merchant_id']) {
            $data .= '&merchant_id=' .$version_data['merchant_id'];
        }
        if ($version_data['shop_url']) {
            $data .= '&shop_url=' .$version_data['shop_url'];
        }

        return $data;
    }

    /**
     * Send version tracker to version tracker gateway
     *
     * @param  array $version_data
     * @return string
     */
    public static function sendVersionTracker($version_data)
    {
        $vrpaycommerce_logger = VRpayecommercePaymentCore::instanceLoggerClass();

        $post_data = self::getVersionTrackerParameter($version_data);
        $vrpaycommerce_logger->notice('get version tracker required parameter : '.
            print_r($post_data, 1), 'security', 'vrpayecommerce_log');

        $url = self::getVersionTrackerUrl();
        $vrpaycommerce_logger->notice('get version tracker url : '.
            print_r($url, 1), 'security', 'vrpayecommerce_log');

        $vesionTrackerResponse = VRpayecommercePaymentCore::getResponseData($post_data, $url, $version_data['transaction_mode']);
        $vrpaycommerce_logger->notice('get response from version tracker : '.
            print_r($vesionTrackerResponse, 1), 'security', 'vrpayecommerce_log');

        return $vesionTrackerResponse;
    }
}
