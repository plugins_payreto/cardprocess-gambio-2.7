<?php

require_once 'vrpayecommerce/vrpayecommerce_base.php';

/**
 * vrpayecommerce_cards class to create VR pay eCommerce Credit Cards recurring payment method in Gambio
 *
 * @package    modules.payment
 * @subpackage vrpayecommerce
 */
class vrpayecommerce_cards_saved extends vrpayecommerce_base
{
    protected $account_type = 'card';
    protected $group_recurring = 'CC';

    /**
     * This function is constructor of vrpayecommerce_cards_saved class
     *
     * @return void
     */
    public function __construct()
    {
        $this->code = "vrpayecommerce_cards_saved";
        $this->recurring = true;
        $this->brand = "VISA MASTER AMEX DINERS JCB";
        $this->template = "form_cc.html";
        parent::__construct();
    }

    /**
     * Get VR pay eCommerce Credit Cards recurring keys
     *
     * @return integer
     */
    public function keys()
    {
        return array(
          'MODULE_PAYMENT_'.$this->code_upper_case.'_STATUS',
          'MODULE_PAYMENT_'.$this->code_upper_case.'_SERVER_MODE',
          'MODULE_PAYMENT_'.$this->code_upper_case.'_PAYMENT_TYPE',
          'MODULE_PAYMENT_'.$this->code_upper_case.'_CARDS_TYPE',
          'MODULE_PAYMENT_'.$this->code_upper_case.'_REGISTER_AMOUNT',
          'MODULE_PAYMENT_'.$this->code_upper_case.'_MULTICHANNEL',
          'MODULE_PAYMENT_'.$this->code_upper_case.'_CHANNEL',
          'MODULE_PAYMENT_'.$this->code_upper_case.'_CHANNEL_MOTO',
          'MODULE_PAYMENT_'.$this->code_upper_case.'_ZONE',
          'MODULE_PAYMENT_'.$this->code_upper_case.'_ALLOWED',
          'MODULE_PAYMENT_'.$this->code_upper_case.'_SORT_ORDER'
        );
    }

    /**
     * Install VR pay eCommerce Credit Cards recurring payment method
     *
     * @return void
     */
    public function install()
    {
        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            date_added
            ) values (
            'MODULE_PAYMENT_".$this->code_upper_case."_REGISTER_AMOUNT',
            '',
            '6',
            '0',
            now())"
        );
        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            set_function, date_added
            ) values (
            'MODULE_PAYMENT_".$this->code_upper_case."_MULTICHANNEL',
            'False',
            '6',
            '0',
            'xtc_cfg_select_option(array(\'True\', \'False\'), ',
              now())"
        );
        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            date_added
            ) values (
            'MODULE_PAYMENT_".$this->code_upper_case."_CHANNEL_MOTO',
            '',
            '6',
            '0',
            now())"
        );
        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            set_function,
            date_added
            ) values (
            'MODULE_PAYMENT_".$this->code_upper_case."_PAYMENT_TYPE',
            'Debit',
            '6',
            '0',
            'xtc_cfg_select_option(array(\'Debit\', \'Pre-Authorization\'), ',
            now())"
        );
        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            set_function,
            date_added
            ) values (
            'MODULE_PAYMENT_".$this->code_upper_case."_CARDS_TYPE',
            'VISA,MASTER,AMEX,DINERS,JCB',
            '6',
            '0',
            'vrpay_cfg_multiselect_option(array(\'VISA\', \'MASTER\', \'AMEX\', \'DINERS\', \'JCB\'), ',
            now())"
        );
        parent::install();
    }

    /**
     * Get VR pay eCommerce Credit Cards recurring brand
     *
     * @return string
     */
    public function getBrand()
    {
        return $this->getBrandCards();
    }

    /**
     * Get VR pay eCommerce Credit Cards recurring title
     *
     * @return html
     */
    public function getTitle()
    {
        return $this->getTitleCards();
    }

    /**
     * Get VR pay eCommerce Credit Cards recurring payment type
     *
     * @return string
     */
    public function getPaymentType()
    {
        return $this->getPaymentTypeSelection();
    }
}
MainFactory::load_origin_class('vrpayecommerce_cards_saved');
