<?php
chdir('../../../../');
require 'includes/application_top.php';
require_once DIR_WS_MODULES. 'payment/'. $_GET['payment_module']. '.php';

$current_payment = new $_GET['payment_module'];

if (isset($_GET['response_from']) && $_GET['response_from'] == 'checkout') {
    $current_payment->getShopUrl();
} elseif ($current_payment->isRecurring()) {
    if (isset($_GET['recurring_id'])) {
        $current_payment->getShopUrlRecurring($_GET['recurring_id']);
    } else {
        $current_payment->getShopUrlRecurring();
    }
}
