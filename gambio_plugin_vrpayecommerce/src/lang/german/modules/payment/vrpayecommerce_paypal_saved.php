<?php

define('MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_TEXT_TITLE', 'PayPal');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_TEXT_DESCRIPTION',
    '<a href="http://www.vrpayecommerce.de" target="_blank" style="text-decoration: underline; font-weight: bold;">
    <img src="../ext/modules/payment/vrpayecommerce/images/logovrpay.jpg" border="0">
    </a></br><p>VR pay eCommerce - PayPal (Recurring)</p>'
);

define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_STATUS_TITLE',
    'Aktivieren Sie das VR pay eCommerce PayPal (Recurring) Modul'
);
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_STATUS_DESC',
    'Bitte aktivieren Sie das Modul,
    wenn Sie PayPal (Recurring) Transaktionen von VR pay eCommerce akzeptieren wollen.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_SERVER_MODE_TITLE', 'Server');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_SERVER_MODE_DESC',
    'Wählen Sie den Server für PayPal (Recurring).'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_REGISTER_AMOUNT_TITLE', 'Betrag für Registrierung');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_REGISTER_AMOUNT_DESC',
    'Betrag, der bei der Registrierung von Zahlungsarten gebucht und gutgeschrieben wird
    (wenn die Registrierung ohne Checkout erfolgt)'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_CHANNEL_TITLE', 'Entity-ID');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_CHANNEL_DESC',
    'Entity-ID für PayPal (Recurring) hinterlegen.'
);

define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_ORDER_STATUS_ID_TITLE',
    'Bestellstatus für akzeptierte Zahlungen festlegen'
);
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_ORDER_STATUS_ID_DESC',
    'Hinterlegen Sie hier den Bestellstatus, der bei akzeptierten Zahlungen angezeigt werden soll.'
);

define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_RF_ORDER_STATUS_ID_TITLE',
    'Bestellstatus für Refunds hinterlegen'
);
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_RF_ORDER_STATUS_ID_DESC',
    'Hinterlegen Sie hier den Bestellstatus, der bei Refunds angezeigt werden soll.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_ZONE_TITLE', 'Zahlungszone');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_ZONE_DESC',
    'Wenn ein Land ausgewählt wurde, ist die Zahlungsart nur für dieses Land freigeschaltet.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_ALLOWED_TITLE', 'Erlaubte Länder');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_ALLOWED_DESC',
    'Bitte wählen Sie die Länder aus, in den dieses Modul verfügbar sein soll.'
);

define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_SORT_ORDER_TITLE',
    'Legen Sie die Reihenfolge der Zahlungsarten fest'
);
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_SORT_ORDER_DESC',
    'Die Reihenfolge in der die Zahlungsarten im Bestellprozess angezeigt werden,
    wird im Feld &bdquo;Reihenfolge&rdquo; mit bei &bdquo;1&rdquo;
    beginnenden Ziffern festgelegt und kann individuell angepasst werden.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SAVED_TT_TRANSACTION_ID', 'Transaktions-ID');

define('FRONTEND_MC_EMAIL', 'Email:');
define('FRONTEND_MC_HOLDER', 'Inhaber:');
define('FRONTEND_MC_PAYPALNEW', 'PayPal Konto hinzuf&uuml;gen');
define('FRONTEND_MC_BT_CHANGE', '&Auml;ndern');
define('FRONTEND_MC_BT_SELECT', 'Ausw&auml;hlen');
define('FRONTEND_MC_PAYPALSELECT', 'PayPal Konto ausw&auml;hlen');
