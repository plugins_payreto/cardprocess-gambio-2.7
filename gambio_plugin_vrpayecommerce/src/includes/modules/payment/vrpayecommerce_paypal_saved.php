<?php

require_once 'vrpayecommerce/vrpayecommerce_base.php';

/**
 * vrpayecommerce_cards class to create VR pay eCommerce Paypal recurring payment method in Gambio
 *
 * @package    modules.payment
 * @subpackage vrpayecommerce
 */
class vrpayecommerce_paypal_saved extends vrpayecommerce_base
{
    protected $account_type = 'virtualAccount';
    protected $group_recurring = 'OT';

    /**
     * This function is constructor of vrpayecommerce_paypal_saved class
     *
     * @return void
     */
    public function __construct()
    {
        $this->code = "vrpayecommerce_paypal_saved";
        $this->recurring = true;
        $this->redirect_account = true;
        $this->brand = 'PAYPAL';
        $this->template = 'form_paypalsaved.html'; //template form paypal (recurring) one-click checkout
        $this->logo = "paypal.png";
        parent::__construct();
    }

    /**
     * Get VR pay eCommerce Paypal recurring keys
     *
     * @return integer
     */
    public function keys()
    {
        return array(
          'MODULE_PAYMENT_'.$this->code_upper_case.'_STATUS',
          'MODULE_PAYMENT_'.$this->code_upper_case.'_SERVER_MODE',
          'MODULE_PAYMENT_'.$this->code_upper_case.'_REGISTER_AMOUNT',
          'MODULE_PAYMENT_'.$this->code_upper_case.'_CHANNEL',
          'MODULE_PAYMENT_'.$this->code_upper_case.'_ZONE',
          'MODULE_PAYMENT_'.$this->code_upper_case.'_ALLOWED',
          'MODULE_PAYMENT_'.$this->code_upper_case.'_SORT_ORDER'
        );
    }

    /**
     * Install VR pay eCommerce Paypal recurring payment method
     *
     * @return void
     */
    public function install()
    {
        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            date_added
            ) values (
            'MODULE_PAYMENT_".$this->code_upper_case."_REGISTER_AMOUNT',
            '',
            '6',
            '0',
            now())"
        );
        parent::install();
    }

    /**
     * Get VR pay eCommerce Paypal recurring account
     *
     * @param  array $resultJson
     * @return array
     */
    public function getAccount($resultJson)
    {
        $account = $resultJson[$this->account_type];
        $account['email'] = $account['accountId'];
        return $account;
    }
}
MainFactory::load_origin_class('vrpayecommerce_paypal_saved');
