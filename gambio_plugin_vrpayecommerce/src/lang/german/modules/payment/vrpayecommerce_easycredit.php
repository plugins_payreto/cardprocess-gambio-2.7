<?php

define('MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_TEXT_TITLE', 'Easycredit');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_TEXT_DESCRIPTION',
    '<a href="http://www.vrpayecommerce.de" target="_blank" style="text-decoration: underline; font-weight: bold;">
    <img src="../ext/modules/payment/vrpayecommerce/images/logovrpay.jpg" border="0">
    </a></br><p>VR pay eCommerce - Easycredit</p>'
);

define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_STATUS_TITLE',
    'Aktivieren Sie das VR pay eCommerce Easycredit Modul'
);
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_STATUS_DESC',
    'Bitte aktivieren Sie das Modul, wenn Sie Easycredit Transaktionen von VR pay eCommerce akzeptieren wollen.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_SERVER_MODE_TITLE', 'Server');
define('MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_SERVER_MODE_DESC', 'Wählen Sie den Server für Easycredit.');

define('MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_CHANNEL_TITLE', 'Entity-ID');
define('MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_CHANNEL_DESC', 'Entity-ID für Easycredit hinterlegen.');

define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_PA_ORDER_STATUS_ID_TITLE',
    'Bestellstatus für Pre-Authorization festlegen'
);
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_PA_ORDER_STATUS_ID_DESC',
    'Hinterlegen Sie hier den Bestellstatus, der bei Pre-Authorisierten Zahlungen angezeigt werden soll.'
);

define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_ORDER_STATUS_ID_TITLE',
    'Bestellstatus für akzeptierte Zahlungen festlegen'
);
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_ORDER_STATUS_ID_DESC',
    'Hinterlegen Sie hier den Bestellstatus, der bei akzeptierten Zahlungen angezeigt werden soll.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_RF_ORDER_STATUS_ID_TITLE', 'Bestellstatus für Refunds hinterlegen');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_RF_ORDER_STATUS_ID_DESC',
    'Hinterlegen Sie hier den Bestellstatus, der bei Refunds angezeigt werden soll.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_ZONE_TITLE', 'Zahlungszone');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_ZONE_DESC',
    'Wenn ein Land ausgewählt wurde, ist die Zahlungsart nur für dieses Land freigeschaltet.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_ALLOWED_TITLE', 'Erlaubte Länder');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_ALLOWED_DESC',
    'Bitte wählen Sie die Länder aus, in den dieses Modul verfügbar sein soll.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_SORT_ORDER_TITLE', 'Legen Sie die Reihenfolge der Zahlungsarten fest');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_SORT_ORDER_DESC',
    'Die Reihenfolge in der die Zahlungsarten im Bestellprozess angezeigt werden,
    wird im Feld &bdquo;Reihenfolge&rdquo; mit bei &bdquo;1&rdquo;
    beginnenden Ziffern festgelegt und kann individuell angepasst werden.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_TT_TRANSACTION_ID', 'Transaktions-ID');

define(
    'ERROR_EASYCREDIT_FUTURE_DOB',
    'Das Geburtsdatum sollte nicht in der Zukunft liegen'
);

define(
    'ERROR_MESSAGE_EASYCREDIT_AMOUNT_NOTALLOWED',
    'Der Finanzierungsbetrag liegt außerhalb der zulässigen Beträge (200 - 3.000 EUR).'
);

define(
    'ERROR_EASYCREDIT_BILLING_NOTEQUAL_SHIPPING',
    'Um mit easyCredit bezahlen zu können, muss die Lieferadresse mit der Rechnungsadresse übereinstimmen.'
);

define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_TEXT_ERROR_CREDENTIALS',
    'VR pay eCommerce General Setting Müssen ausgefüllt werden'
);

define(
    'FRONTEND_EASYCREDIT_LINK',
    'Vorvertragliche Informationen zum Ratenkauf hier abrufen'
);

define(
    'ERROR_EASYCREDIT_PARAMETER_DOB',
    'Bitte geben Sie Ihr Geburtsdatum an, um easyCredit nutzen zu können.'
);

define(
    'ERROR_MESSAGE_EASYCREDIT_PARAMETER_GENDER',
    'Bitte geben Sie Ihr Geschlecht an um die Zahlung mit easyCredit durchzuführen.'
);
