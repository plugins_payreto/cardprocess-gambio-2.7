<?php

require_once 'vrpayecommerce/vrpayecommerce_base.php';

/**
 * vrpayecommerce_cards class to create VR pay eCommerce Paypal payment method in Gambio
 *
 * @package    modules.payment
 * @subpackage vrpayecommerce
 */
class vrpayecommerce_paypal extends vrpayecommerce_base
{
    /**
     * This function is constructor of vrpayecommerce_paypal class
     *
     * @return void
     */
    public function __construct()
    {
        $this->code = "vrpayecommerce_paypal";
        $this->brand = 'PAYPAL';
        $this->redirect = true;
        $this->logo = "paypal.png";
        parent::__construct();
    }
}
MainFactory::load_origin_class('vrpayecommerce_paypal');
