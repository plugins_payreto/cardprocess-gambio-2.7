<?php

require_once 'vrpayecommerce/vrpayecommerce_base.php';

/**
 * vrpayecommerce_cards class to create VR pay eCommerce Giropay payment method in Gambio
 *
 * @package    modules.payment
 * @subpackage vrpayecommerce
 */
class vrpayecommerce_giropay extends vrpayecommerce_base
{
    /**
     * This function is constructor of vrpayecommerce_giropay class
     *
     * @return void
     */
    public function __construct()
    {
        $this->code = "vrpayecommerce_giropay";
        $this->brand = 'GIROPAY';
        $this->test_mode = 'INTERNAL';
        $this->logo = "giropay.png";
        parent::__construct();
    }
}
MainFactory::load_origin_class('vrpayecommerce_giropay');
