<?php

require_once dirname(__FILE__).'/../../../../ext/modules/payment/vrpayecommerce/classes/core.php';
require_once dirname(__FILE__).'/../../../../ext/modules/payment/vrpayecommerce/classes/versiontracker.php';
require_once dirname(__FILE__).'/../../../../ext/modules/payment/vrpayecommerce/includes/vrpay_configuration.inc.php';
require_once DIR_FS_INC . 'xtc_get_countries.inc.php';

/**
 * vrpayecommerce_base class to create payment method plugin in Gambio
 *
 * @package    modules.payment
 * @subpackage vrpayecommerce
 */
class vrpayecommerce_base
{
    public $code;
    public $title;
    public $description;
    public $enabled;
    public $vrpaycommerce_logger; 

    protected $client = 'CardProcess';
    protected $shop_system = 'Gambio';
    protected $recurring = false;
    protected $account_type = '';
    protected $group_recurring = '';
    protected $redirect = false;
    protected $redirect_account = false;
    protected $test_mode = 'EXTERNAL';
    protected $template = 'form_cp.html';
    protected $payment_type = 'DB';
    protected $recurring_id = 0;
    protected $logo = '';


    /**
     * This function is constructor of vrpayecommerce_base class
     *
     * @return void
     */
    public function __construct()
    {
        global $order;
       
        include DIR_FS_CATALOG.'release_info.php';

        $this->shop_version = $gx_version;
        $this->version = '2.1.11';
        $this->code_upper_case = strtoupper($this->code);
        $this->title = $this->getConstantValue('MODULE_PAYMENT_'.$this->code_upper_case.'_TEXT_TITLE');
        $this->description = $this->getConstantValue('MODULE_PAYMENT_'.$this->code_upper_case.'_TEXT_DESCRIPTION');
        $this->enabled = $this->getConstantValue('MODULE_PAYMENT_'.$this->code_upper_case.'_STATUS') == 'True';
        $this->vrpaycommerce_logger = VRpayecommercePaymentCore::instanceLoggerClass();


        // if user checkout as guest show non recurring payment.
        if ($this->isRecurringActive()) {
            if ($_SESSION['account_type'] == 1) {
                if ($this->isRecurring()) {
                    $this->enabled = false;
                }
            } else {
                if ($this->code == 'vrpayecommerce_cards'
                    || $this->code == 'vrpayecommerce_dd'
                    || $this->code == 'vrpayecommerce_paypal'
                ) {
                    $this->enabled = false;
                }
            }
        } else {
            if ($this->isRecurring()) {
                $this->enabled = false;
            }
        }
        if (!$this->getBrand()) {
            $this->enabled = false;
        }

        $this->sort_order = $this->getConstantValue('MODULE_PAYMENT_'.$this->code_upper_case.'_SORT_ORDER');

        if ((int) $this->getConstantValue('MODULE_PAYMENT_'.$this->code_upper_case.'_ORDER_STATUS_ID') > 0) {
            $this->order_status =
            $this->getConstantValue('MODULE_PAYMENT_'.$this->code_upper_case.'_ORDER_STATUS_ID');
        }

        if (is_object($order)) {
            $this->updateStatus();
        }

        $this->form_action_url = xtc_href_link('checkout_payment_vrpayecommerce.php', '', 'SSL');
    }

    /**
     * Get constant value
     *
     * @param  string $value
     * @return string
     */
    public function getConstantValue($value)
    {
        return defined($value) ? constant($value) : '';
    }

    /**
     * check payment method status
     *
     * @return integer
     */
    public function check()
    {
        if (!isset($this->_check)) {
            $check_query = xtc_db_query(
                "select configuration_value from ".TABLE_CONFIGURATION."
                where configuration_key = 'MODULE_PAYMENT_".$this->code_upper_case."_STATUS'"
            );
            $this->_check = xtc_db_num_rows($check_query);
        }
        return $this->_check;
    }

    /**
     * Get payment methods keys
     *
     * @return integer
     */
    public function keys()
    {
        return array(
            'MODULE_PAYMENT_'.$this->code_upper_case.'_STATUS',
            'MODULE_PAYMENT_'.$this->code_upper_case.'_SERVER_MODE',
            'MODULE_PAYMENT_'.$this->code_upper_case.'_CHANNEL',
            'MODULE_PAYMENT_'.$this->code_upper_case.'_ZONE',
            'MODULE_PAYMENT_'.$this->code_upper_case.'_ALLOWED',
            'MODULE_PAYMENT_'.$this->code_upper_case.'_SORT_ORDER'
        );
    }

    /**
     * Set VR pay eCommerce order status
     *
     * @param  string $order_status_en
     * @param  string $order_status_de
     * @param  string $set_to_public
     * @return integer
     */
    public function setOrderStatus($order_status_en, $order_status_de, $set_to_public)
    {
        $status_id = 0;
        $check_query = xtc_db_query(
            "select orders_status_id from ".TABLE_ORDERS_STATUS."
            where orders_status_name = '".$order_status_en."' limit 1"
        );
        if (xtc_db_num_rows($check_query) < 1) {
            $status_query = xtc_db_query("select max(orders_status_id) as status_id from ".TABLE_ORDERS_STATUS);
            $status = xtc_db_fetch_array($status_query);
            $status_id = $status['status_id']+1;
            $languages = xtc_get_languages();
            $flags_query = xtc_db_query("describe " . TABLE_ORDERS_STATUS . " public_flag");
            if (xtc_db_num_rows($flags_query) == 1) {
                foreach ($languages as $lang) {
                    if ($lang['code'] == "de") {
                        $order_status = $order_status_de;
                    } else {
                        $order_status = $order_status_en;
                    }
                    xtc_db_query(
                        "insert into ".TABLE_ORDERS_STATUS."(
                            orders_status_id,
                            language_id,
                            orders_status_name,
                            public_flag
                            ) values (
                            '".$status_id."',
                            '".$lang['id']."',
                            "."'".$order_status."',
                            1)"
                    );
                }
            } else {
                foreach ($languages as $lang) {
                    if ($lang['code'] == "de") {
                        $order_status = $order_status_de;
                    } else {
                        $order_status = $order_status_en;
                    }
                    xtc_db_query(
                        "insert into ".TABLE_ORDERS_STATUS." (
                        orders_status_id,
                        language_id,
                        orders_status_name
                        ) values (
                        '".$status_id."',
                        '".$lang['id']."',
                        "."'".$order_status."')"
                    );
                }
            }
        } else {
            $check = xtc_db_fetch_array($check_query);
            $status_id = $check['orders_status_id'];
        }
        return $status_id;
    }

    /**
     * Install VR pay eCommerce payment methods
     *
     * @return void
     */
    public function install()
    {
        $review_status_id = $this->setOrderStatus('In Review', 'Wird überprüft', true);
        $remotely_status_id = $this->setOrderStatus(
            'Pre-Authorization of Payment',
            'Pre-Authorisierung der Zahlung',
            true
        );
        $accepted_status_id = $this->setOrderStatus('Payment Accepted', 'Zahlung akzeptiert', true);
        $refund_status_id = $this->setOrderStatus('Refund', 'Gutschrift', true);

        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            set_function,
            date_added
            ) values (
            'MODULE_PAYMENT_".$this->code_upper_case."_STATUS',
            'True',
            '6',
            '0',
            'xtc_cfg_select_option(array(\'True\', \'False\'), ',
            now())"
        );
        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            set_function,
            date_added
            ) values (
            'MODULE_PAYMENT_".$this->code_upper_case."_SERVER_MODE',
            'TEST',
            '6',
            '0',
            'xtc_cfg_select_option(array(\'TEST\', \'LIVE\'), ',
            now())"
        );
        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            date_added
            ) values (
            'MODULE_PAYMENT_".$this->code_upper_case."_CHANNEL',
            '',
            '6',
            '0',
            now())"
        );
        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            date_added
            ) values (
            'MODULE_PAYMENT_".$this->code_upper_case."_IR_ORDER_STATUS_ID',
            '" . $review_status_id . "',
            '6',
            '0',
            now())"
        );
        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            date_added
            ) values (
            'MODULE_PAYMENT_".$this->code_upper_case."_PA_ORDER_STATUS_ID',
            '" . $remotely_status_id . "',
            '6',
            '0',
            now())"
        );
        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            date_added
            ) values (
            'MODULE_PAYMENT_".$this->code_upper_case."_ORDER_STATUS_ID',
            '" . $accepted_status_id . "',
            '6',
            '0',
            now())"
        );
        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            date_added
            ) values (
            'MODULE_PAYMENT_".$this->code_upper_case."_RF_ORDER_STATUS_ID',
            '" . $refund_status_id . "',
            '6',
            '0',
            now())"
        );
        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            use_function,
            set_function
            , date_added
            ) values (
            'MODULE_PAYMENT_".$this->code_upper_case."_ZONE',
            '0',
            '6',
            '0',
            'xtc_get_zone_class_title',
            'xtc_cfg_pull_down_zone_classes(',
            now())"
        );
        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            date_added
            ) values (
            'MODULE_PAYMENT_".$this->code_upper_case."_ALLOWED',
            '',
            '6',
            '0',
            now())"
        );
        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            date_added
            ) values (
            'MODULE_PAYMENT_".$this->code_upper_case."_SORT_ORDER',
            '".$this->sort_order."',
            '6',
            '0',
            now())"
        );

        $this->installTable();
    }

    /**
     * Uninstall and remove VR pay eCommerce payment methods
     *
     * @return void
     */
    public function remove()
    {
        $keys = $this->keys();
        $keys[] = 'MODULE_PAYMENT_'.$this->code_upper_case.'_IR_ORDER_STATUS_ID';
        $keys[] = 'MODULE_PAYMENT_'.$this->code_upper_case.'_PA_ORDER_STATUS_ID';
        $keys[] = 'MODULE_PAYMENT_'.$this->code_upper_case.'_ORDER_STATUS_ID';
        $keys[] = 'MODULE_PAYMENT_'.$this->code_upper_case.'_RF_ORDER_STATUS_ID';
        xtc_db_query(
            "delete from " . TABLE_CONFIGURATION . "
            where configuration_key in ('" . implode("', '", $keys) . "')"
        );
    }

    /**
     * This function is Gambio function
     *
     * @return void
     */
    public function updateStatus()
    {
        global $order;

        if (($this->enabled == true)
            && ((int) $this->getConstantValue('MODULE_PAYMENT_'.$this->code_upper_case.'_ZONE') > 0)
        ) {
            $check_flag = false;
            $check_query = xtc_db_query(
                "select zone_id from " . TABLE_ZONES_TO_GEO_ZONES . "
                where geo_zone_id = '".$this->getConstantValue('MODULE_PAYMENT_'.$this->code_upper_case.'_ZONE')."'
                and zone_country_id = '" . $order->billing['country']['id'] . "' order by zone_id"
            );
            while ($check = xtc_db_fetch_array($check_query)) {
                if ($check['zone_id'] < 1) {
                    $check_flag = true;
                    break;
                } elseif ($check['zone_id'] == $order->billing['zone_id']) {
                    $check_flag = true;
                    break;
                }
            }

            if ($check_flag == false) {
                $this->enabled = false;
            }
        }
    }

    /**
     * This function is Gambio function
     *
     * @return void
     */
    public function javascript_validation()
    {
        return false;
    }

    /**
     * Get payment methods title
     *
     * @return html
     */
    public function getTitle()
    {
        $title = '<img src="'.DIR_WS_CATALOG.'ext/modules/payment/vrpayecommerce/images/'.$this->logo.'"
        border="0" height="35px">';
        return $title;
    }

    /**
     * Get payment methods selection
     *
     * @return array
     */
    public function selection()
    {
        return array(
            'id' => $this->code,
            'module' => $this->getTitle()
        );
    }

    /**
     * This function is Gambio function
     *
     * @return array
     */
    public function pre_confirmation_check()
    {
        return false;
    }

    /**
     * Order confirmation
     * This function is Gambio function
     *
     * @return boolean
     */
    public function confirmation()
    {
        return false;
    }

    /**
     * This function is Gambio function
     *
     * @return boolean
     */
    public function process_button()
    {
        return false;
    }

    /**
     * Before order confirmation process
     *
     * @return void
     */
    public function before_process()
    {
        global $order;

        $payment_response = $_SESSION['vrpayecommerce']['resultJson'];
        if ($payment_response) {
            if (isset($_SESSION['vrpayecommerce']['resultJson']['registrationId'])) {
                $payment_brand = $payment_response['paymentBrand'];
                $registration_id = $payment_response['registrationId'];
                $account = $this->getAccount($payment_response);
                $default = $this->checkDefault();

                $reference_id = $this->isReferenceIdExist($registration_id);
                if (!$reference_id) {
                    $this->insertRecurringPayment($payment_brand, $account, $registration_id, $default);
                }
            }
        }

        if ($_SESSION['vrpayecommerce']['isInReview']) {
            $order->info['order_status'] = $this->getReviewStatus();
        } elseif ($this->getPaymentType() == "PA") {
            if ($order->info['payment_method'] == 'vrpayecommerce_easycredit') {
                $order->info['order_status'] = $this->getAcceptStatus();
            } else {
                $order->info['order_status'] = $this->getPaStatus();
            }
        } else {
            $order->info['order_status'] = $this->getAcceptStatus();
        }
    }

    /**
     * After order confirmation process
     *
     * @return void
     */
    public function after_process()
    {
        global $insert_id, $order, $customer_notification;

        $orders_query = xtc_db_query(
            "SELECT currency FROM " . TABLE_ORDERS . "
            WHERE `orders_id`='" .(int)$insert_id. "' "
        );
        $orders_result = xtc_db_fetch_array($orders_query);

        $orders_total_query = xtc_db_query(
            "SELECT value FROM " . TABLE_ORDERS_TOTAL . "
            WHERE `orders_id`='" .(int)$insert_id. "'
            AND `class`='ot_total'"
        );
        $orders_total_result = xtc_db_fetch_array($orders_total_query);

        $sql_data_array = array (
            'transaction_id' => $_SESSION['vrpayecommerce']['resultJson']['merchantTransactionId'],
            'orders_id' => (int)$insert_id,
            'unique_id' => $_SESSION['vrpayecommerce']['resultJson']['id'],
            'amount' => $orders_total_result['value'],
            'currency' => $orders_result['currency'],
            'payment_type' => $_SESSION['vrpayecommerce']['resultJson']['paymentType']
        );
        xtc_db_perform('payment_vrpayecommerce_orders', $sql_data_array);

        if ($order->info['order_status']) {
            xtc_db_query(
                "UPDATE ".TABLE_ORDERS."
                SET orders_status='".$order->info['order_status']."' WHERE orders_id='".$insert_id."'"
            );
        }

        unset($_SESSION['vrpayecommerce']);
    }

    /**
     * Get error message when doing order
     *
     * @return array
     */
    public function get_error()
    {
        include_once DIR_WS_LANGUAGES.$_SESSION['language'].'/modules/payment/vrpayecommerce_error.php';
        $error = array('error' => $this->getConstantValue($_GET['error']));
        return $error;
    }

    /**
     * Install VR pay eCommerce table
     *
     * @return void
     */
    public function installTable()
    {
        xtc_db_query(
            "
            CREATE TABLE IF NOT EXISTS `payment_vrpayecommerce_orders` (
                    `id` INT(11) NOT NULL AUTO_INCREMENT,
                    `orders_id` INT(11) NOT NULL,
                    `unique_id` VARCHAR(32) NOT NULL,
                    `amount` DECIMAL(15,4) NOT NULL,
                    `currency` VARCHAR(3) NOT NULL,
                    `payment_type` VARCHAR(2) NOT NULL,
                    PRIMARY KEY (`id`)
            ) ENGINE=MyISAM; "
        );

        $check_query = xtc_db_query("SHOW columns FROM `payment_vrpayecommerce_orders` LIKE 'transaction_id'");

        if (xtc_db_num_rows($check_query) < 1) {
            xtc_db_query(
                "
                ALTER TABLE `payment_vrpayecommerce_orders`
                    ADD COLUMN `transaction_id` VARCHAR(32) NOT NULL AFTER `id`"
            );
        }

        xtc_db_query(
            "
            CREATE TABLE IF NOT EXISTS `payment_vrpayecommerce_recurring` (
                    `id` INT(11) NOT NULL AUTO_INCREMENT,
                    `cust_id` INT(11) NOT NULL,
                    `payment_group` VARCHAR(32),
                    `brand` VARCHAR(100),
                    `holder` VARCHAR(100) NULL default NULL,
                    `email` VARCHAR(100) NULL default NULL,
                    `last4digits` VARCHAR(4),
                    `expiry_month` VARCHAR(2),
                    `expiry_year` VARCHAR(4),
                    `ref_id` VARCHAR(32),
                    `payment_default` boolean NOT NULL default '0',
                    PRIMARY KEY (`id`)
            ) ENGINE=MyISAM; "
        );

        $check_query = xtc_db_query("SHOW columns FROM `payment_vrpayecommerce_recurring` LIKE 'server_mode'");

        if (xtc_db_num_rows($check_query) < 1) {
            xtc_db_query(
                "
                ALTER TABLE `payment_vrpayecommerce_recurring`
                    ADD COLUMN `server_mode` VARCHAR(4) NOT NULL AFTER `expiry_year`,
                    ADD COLUMN `channel_id` VARCHAR(32) NOT NULL AFTER `server_mode`"
            );
        }
    }

    /**
     * Get payment accepted status
     *
     * @return string
     */
    public function getAcceptStatus()
    {
        return $this->getConstantValue('MODULE_PAYMENT_'.$this->code_upper_case.'_ORDER_STATUS_ID');
    }

    /**
     * Get pre-authorization status
     *
     * @return string
     */
    public function getPaStatus()
    {
        return $this->getConstantValue('MODULE_PAYMENT_'.$this->code_upper_case.'_PA_ORDER_STATUS_ID');
    }

    /**
     * Get in review status
     *
     * @return string
     */
    public function getReviewStatus()
    {
        return $this->getConstantValue('MODULE_PAYMENT_'.$this->code_upper_case.'_IR_ORDER_STATUS_ID');
    }

    /**
     * Get refund status
     *
     * @return string
     */
    public function getRefundStatus()
    {
        return $this->getConstantValue('MODULE_PAYMENT_'.$this->code_upper_case.'_RF_ORDER_STATUS_ID');
    }

    /**
     * Get PayDirekt payment is partial
     *
     * @return string
     */
    protected function getPaymentIsPartial()
    {
        $payment_is_partial = trim(
            $this->getConstantValue('MODULE_PAYMENT_'.$this->code_upper_case.'_PAYMENT_IS_PARTIAL')
        );
        if ($payment_is_partial == "Yes") {
            return 'true';
        } else {
            return 'false';
        }
    }

    /**
     * Get PayDirekt minimum age
     *
     * @return string
     */
    protected function getMinimumAge()
    {
        return $this->getConstantValue('MODULE_PAYMENT_'.$this->code_upper_case.'_MINIMUM_AGE');
    }

    /**
     * Check recurring activation
     *
     * @return string
     */
    public function isRecurringActive()
    {
        return $this->getConstantValue('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_RECURRING') == 'True';
    }

    /**
     * Check version tracker activation
     *
     * @return string
     */
    public function isVersionTrackerActive()
    {
        return $this->getConstantValue('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_VERSION_TRACKER') == 'True';
    }

    /**
     * Check recurring of payment methods
     *
     * @return boolean
     */
    public function isRecurring()
    {
        return $this->recurring;
    }

    /**
     * Get group of recurring payment
     *
     * @return string
     */
    public function getGroupRecurring()
    {
        return $this->group_recurring;
    }

    /**
     * Get brand
     *
     * @return string
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Get template
     *
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Check redirect of payment methods
     *
     * @return boolean
     */
    public function isRedirect()
    {
        return $this->redirect;
    }

    /**
     * Check redirect of recurring payment methods
     *
     * @return boolean
     */
    public function isRedirectAccount()
    {
        return $this->redirect_account;
    }

    /**
     * Get payment methods brands
     *
     * @return string
     */
    public function getBrandCards()
    {
        $brand = '';
        $brands = $this->getConstantValue('MODULE_PAYMENT_'.$this->code_upper_case.'_CARDS_TYPE');
        $brands = explode(',', $brands);
        foreach ($brands as $value) {
            $brand .= $value.' ';
        }
        return trim($brand);
    }

    /**
     * Get payment methods title
     *
     * @return html
     */
    public function getTitleCards()
    {
        $card = $this->getBrand();
        $cards = explode(' ', $card);
        if (is_array($cards)) {
            foreach ($cards as $value) {
                $logo = strtolower($value).'.png';
                $title .= "<img src='".DIR_WS_CATALOG."ext/modules/payment/vrpayecommerce/images/{$logo}'
                alt='{$value}' border='0' height='35px' style='margin-right:5px'>";
            }
        }
        return $title;
    }

    /**
     * Get payment type selection
     *
     * @return string
     */
    public function getPaymentTypeSelection()
    {
        $payment_type = trim($this->getConstantValue('MODULE_PAYMENT_'.$this->code_upper_case.'_PAYMENT_TYPE'));
        if ($payment_type == "Debit") {
            return 'DB';
        } else {
            return 'PA';
        }
    }

    /**
     * Get admin email
     *
     * @return string
     */
    public function getAdminEmail()
    {
        $query = xtc_db_query("SELECT customers_email_address from customers where customers_status = '0'");
        $row = xtc_db_fetch_array($query);
        return $row['customers_email_address'];
    }

    /**
     * Get merchant email
     *
     * @return string
     */
    public function getMerchantEmail()
    {
        $merchant_email = $this->getConstantValue('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_MERCHANT_EMAIL');
        if (empty($merchant_email)) {
            return $this->getAdminEmail();
        } else {
            return $merchant_email;
        }
    }

    /**
     * Get merchant data
     *
     * @return array
     */
    public function getMerchantData()
    {
        return array(
            'merchant_email' => $this->getMerchantEmail(),
            'merchant_no' => trim($this->getConstantValue('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_MERCHANT_NO')),
            'shop_url' => trim($this->getConstantValue('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_SHOP_URL'))
        );
    }

    /**
     * Check multichannel activation of payment methods
     *
     * @return boolean
     */
    public function isMultichannel()
    {
        $multichannel = $this->getConstantValue('MODULE_PAYMENT_'.$this->code_upper_case.'_MULTICHANNEL') == 'True';
        return $multichannel;
    }

    /**
     * Get credentials of payment methods
     *
     * @return array
     */
    public function getCredentials()
    {
        $credentials = array(
            'server_mode' => $this->getServerMode(),
            'channel_id'  => trim($this->getConstantValue('MODULE_PAYMENT_'.$this->code_upper_case.'_CHANNEL')),
            'login'       => trim($this->getConstantValue('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_USER_LOGIN')),
            'password'    => trim($this->getConstantValue('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_USER_PWD'))
        );

        if ($this->isMultichannel()) {
            $credentials['channel_id_moto'] = trim(
                $this->getConstantValue('MODULE_PAYMENT_'.$this->code_upper_case.'_CHANNEL_MOTO')
            );
        }
        VRpayecommercePaymentCore::instanceLoggerClass()->notice('get credentials data '.
            print_r($credentials, 1), 'security', 'vrpayecommerce_log');

        return $credentials;
    }

    /**
     * Get server mode of payment methods
     *
     * @return string
     */
    public function getServerMode()
    {
        return trim($this->getConstantValue('MODULE_PAYMENT_'.$this->code_upper_case.'_SERVER_MODE'));
    }

    /**
     * Get register amount of recurring payment method
     *
     * @return string
     */
    protected function getRegisterAmount()
    {
        return trim($this->getConstantValue('MODULE_PAYMENT_'.$this->code_upper_case.'_REGISTER_AMOUNT'));
    }

    /**
     * Get Klarna PClass
     *
     * @return string
     */
    protected function getPclass()
    {
        return trim($this->getConstantValue('MODULE_PAYMENT_'.$this->code_upper_case.'_PCLASS'));
    }

    /**
     * Get payment type
     *
     * @return string
     */
    public function getPaymentType()
    {
        return $this->payment_type;
    }

    /**
     * Get test mode
     *
     * @return boolean
     */
    public function getTestMode()
    {
        if ($this->getServerMode() == "LIVE") {
            return false;
        }
        return $this->test_mode;
    }

    /**
     * Get customer ip
     *
     * @return string
     */
    protected function getCustomerIp()
    {
        if ($_SERVER['REMOTE_ADDR'] == '::1') {
            return "127.0.0.1";
        }
        return $_SERVER['REMOTE_ADDR'];
    }

    /**
     * Get customer data
     *
     * @return array
     */
    protected function getCustomerData()
    {
        $customer = array();
        $customer_query = xtc_db_query(
            "select * from ".TABLE_CUSTOMERS."
            where customers_id = '".(int) $_SESSION['customer_id']."'"
        );
        while ($row = xtc_db_fetch_array($customer_query)) {
            $customer[] = $row;
        }
        return $customer[0];
    }

    /**
     * Get customer address
     *
     * @return array
     */
    protected function getCustomerAddress()
    {
        $addresses = array ();
        $addresses_query = xtc_db_query(
            "select * from ".TABLE_ADDRESS_BOOK."
            where customers_id = '".(int) $_SESSION['customer_id']."'
            and address_book_id = '".(int) $_SESSION['customer_default_address_id']."'"
        );
        while ($row = xtc_db_fetch_array($addresses_query)) {
            $addresses[] = $row;
        }
        return $addresses[0];
    }

    /**
     * Get country code 2 based on $country_id
     *
     * @param  string $country_id
     * @return string
     */
    protected function getCountryIsoCode($country_id)
    {
        $country_array = xtc_get_countriesList($country_id, true);
        return $country_array['countries_iso_code_2'];
    }

    /**
     * Get account of recurring payment method
     *
     * @param  array $payment_status
     * @return array
     */
    protected function getAccount($payment_status)
    {
        return $payment_status[$this->account_type];
    }

    /**
     * Get reference id
     *
     * @param  string $recurring_id
     * @return string
     */
    protected function getReferenceId($recurring_id)
    {
        $query = xtc_db_query(
            "select ref_id from payment_vrpayecommerce_recurring
            where cust_id = '".(int) $_SESSION['customer_id']."'
            and id = '".(int) $recurring_id."'"
        );
        $row = xtc_db_fetch_array($query);
        return $row['ref_id'];
    }

    /**
     * Check availability reference id
     *
     * @param  string $ref_id
     * @return string
     */
    protected function isReferenceIdExist($reference_id)
    {
        $query = xtc_db_query(
            "select ref_id from payment_vrpayecommerce_recurring
            where cust_id = '".(int) $_SESSION['customer_id']."'
            and ref_id='".$reference_id."'"
        );
        $row = xtc_db_fetch_array($query);
        return $row['ref_id'];
    }

    /**
     * Check recurring payment default
     *
     * @return integer
     */
    public function checkDefault()
    {
        $credentials = $this->getCredentials();
        $count_row = xtc_db_num_rows(
            xtc_db_query(
                "select * from payment_vrpayecommerce_recurring
                where cust_id = '".(int) $_SESSION['customer_id']."'
                and server_mode = '".$credentials['server_mode']."'
                and channel_id = '".$credentials['channel_id']."'
                and payment_group = '".$this->getGroupRecurring()."'
                and payment_default = '1'"
            )
        );
        if ($count_row > 0) {
            return 0;
        }
        return 1;
    }

    /**
     * Get recurring payment data list
     *
     * @return array
     */
    public function getRecurringLists()
    {
        $credentials = $this->getCredentials();
        $query = xtc_db_query(
            "select * from payment_vrpayecommerce_recurring
            where cust_id = '".(int) $_SESSION['customer_id']."'
            and payment_group = '".$this->getGroupRecurring()."'
            and server_mode = '".$credentials['server_mode']."'
            and channel_id = '".$credentials['channel_id']."'"
        );

        while ($row = xtc_db_fetch_array($query)) {
            $recurring_list[] = $row;
        }
        return $recurring_list;
    }

    /**
     * Get checkout ID and create payment widget at my payment information page
     *
     * @param  string $payment_widget_url
     * @param  string $id
     * @return string
     */
    public function getCheckoutIdRecurringPayment(&$payment_widget_url, $id = false)
    {
        $customer = $this->getCustomerData();
        $customer_address = $this->getCustomerAddress();

        $transaction_data = $this->getCredentials();
        $transaction_data['customer']['email'] = $customer['customers_email_address'];
        $transaction_data['customer']['first_name'] = $customer_address['entry_firstname'];
        $transaction_data['customer']['last_name'] = $customer_address['entry_lastname'];
        $transaction_data['billing']['street'] = $customer_address['entry_street_address'];
        $transaction_data['billing']['city'] = $customer_address['entry_city'];
        $transaction_data['billing']['zip'] = $customer_address['entry_postcode'];
        $transaction_data['billing']['country_code'] = $this->getCountryIsoCode($customer_address['entry_country_id']);
        $transaction_data['amount'] = $this->getRegisterAmount();
        $transaction_data['currency'] = $_SESSION['currency'];
        $transaction_data['customer_ip'] = $this->getCustomerIp();

        if ($this->code == 'vrpayecommerce_cards_saved') {
            $transaction_data['3D']['amount'] = $transaction_data['amount'];
            $transaction_data['3D']['currency'] = $transaction_data['currency'];
        }
        if ($this->getGroupRecurring() != 'OT') {
            $transaction_data['payment_type'] = $this->getPaymentType();
        }

        $transaction_data['test_mode'] = $this->getTestMode();
        $transaction_data['payment_recurring'] = 'INITIAL';
        $transaction_data['payment_registration'] = 'true';

        if ($id) { // change payment
            $transaction_data['transaction_id'] = $this->getReferenceId($id);
        } else {
            $transaction_data['transaction_id'] = $_SESSION['customer_id'];
        }
        $payment_widget_url = VRpayecommercePaymentCore::getPaymentWidgetUrl($transaction_data, $checkout_id);

        return $checkout_id;
    }

    /**
     * Get cart items
     *
     * @return array
     */
    protected function getCartItems()
    {
        global $order;

        $cart_items = array();
        $i = 0;
        foreach ($order->products as $item) {
            $cart_items[$i]['merchant_item_id'] = $item['id'];
            $cart_items[$i]['discount'] = $_SESSION['customers_status']['customers_status_ot_discount'];
            $cart_items[$i]['quantity'] = $item['qty'];
            $cart_items[$i]['name'] = $item['name'];
            $cart_items[$i]['price'] = $item['price'];
            $cart_items[$i]['tax'] = $item['tax'];
            $i = $i+1;
        }
        return $cart_items;
    }

    /**
     * Get klarna parameters
     *
     * @return array
     */
    protected function getKlarnaParameters()
    {
        $klarna_parameters = array();
        $customer_data = $this->getCustomerData();

        if ($this->code == 'vrpayecommerce_klarnainv' || $this->code == 'vrpayecommerce_klarnains') {
            $klarna_parameters['customer']['sex'] = strtoupper($customer_data['customers_gender']);
            $klarna_parameters['customer']['birthdate'] = substr($customer_data['customers_dob'], 0, 10);
            $klarna_parameters['customer']['phone'] = $customer_data['customers_telephone'];
            $klarna_parameters['cartItems'] = $this->getCartItems();
            $klarna_parameters['customParameters']['KLARNA_CART_ITEM1_FLAGS'] = 32;

            if ($this->code == 'vrpayecommerce_klarnains') {
                $klarna_parameters['customParameters']['KLARNA_PCLASS_FLAG'] = $this->getPclass();
            }
        }
        return $klarna_parameters;
    }

    protected function getEasycreditParameters()
    {
        global $order;
        $easycredit_parameters = array();
        $customer_data = $this->getCustomerData();

        if ($this->code == 'vrpayecommerce_easycredit') {
            $easycredit_parameters['customer']['sex'] = strtoupper($customer_data['customers_gender']);
            $easycredit_parameters['customer']['birthdate'] = substr($customer_data['customers_dob'], 0, 10);
            $easycredit_parameters['customer']['phone'] = $customer_data['customers_telephone'];
            $easycredit_parameters['cartItems'] = $this->getCartItems();
            $easycredit_parameters['customParameters']['RISK_ANZAHLBESTELLUNGEN'] = $this->getCustomerTotalOrder();
            $easycredit_parameters['customParameters']['RISK_BESTELLUNGERFOLGTUEBERLOGIN'] =
                $this->isCustomerLogin();
            $easycredit_parameters['customParameters']['RISK_KUNDENSTATUS'] = $this->getRiskKundenStatus();
            $easycredit_parameters['customParameters']['RISK_KUNDESEIT'] = $this->getCustomerCreatedDate();
            $easycredit_parameters['paymentBrand'] = $this->getBrand();
            $easycredit_parameters['shipping']['city'] = $order->delivery['city'];
            $easycredit_parameters['shipping']['country'] = $order->delivery['country']['iso_code_2'];
            $easycredit_parameters['shipping']['postcode'] = $order->delivery['postcode'];
            $easycredit_parameters['shipping']['street1'] = $order->delivery['street_address'];

            $easycredit_parameters['shopperResultUrl'] = xtc_href_link(
                'checkout_confirmation_vrpayecommerce.php?payment_module='
                    . $this->code . '&response_from=servertoserver',
                '',
                "SSL",
                true,
                false
            );
        }
        return $easycredit_parameters;
    }

    /**
     * Get customer created date
     *
     * @return string|boolean
     */
    protected function getCustomerCreatedDate()
    {
        $query = xtc_db_query(
            "SELECT customers_date_added FROM `customers`
            WHERE customers_id = '".$_SESSION['customer_id']."'"
        );
        $result = xtc_db_fetch_array($query);

        if (isset($result['customers_date_added'])) {
            return date('Y-m-d', strtotime($result['customers_date_added']));
        }
        return date('Y-m-d');
    }

    /**
     * Check customer login status
     *
     * @return string
     */
    protected function isCustomerLogin()
    {
        if ($_SESSION['customers_status']['customers_status_id'] == '1') {
            return 'false';
        }
        return 'true';
    }
    /**
     * Get risk kunden status
     *
     * @return string|boolean
     */
    protected function getRiskKundenStatus()
    {
        if ($this->getCustomerTotalOrder() > 0) {
            return 'BESTANDSKUNDE';
        }
        return 'NEUKUNDE';
    }

    /**
     * Get customer total order from database
     *
     * @return string
     */
    protected function getCustomerTotalOrder()
    {
        if ($this->isCustomerLogin() == 'true') {
            $query = xtc_db_query(
                "SELECT COUNT(orders_id) as order_count FROM `orders` where customers_id ='".
                $_SESSION['customer_id']."'"
            );
            $result = xtc_db_fetch_array($query);

            return $result['order_count'];
        }

        return '0';
    }

    /**
     * Get PayDirekt parameters
     *
     * @return array
     */
    protected function getPaydirektParameters()
    {
        global $order;

        $transactionData = array();
        if ($this->code == 'vrpayecommerce_paydirekt') {
            $transactionData['customParameters']['PAYDIREKT_minimumAge'] = $this->getMinimumAge();
            $transactionData['customParameters']['PAYDIREKT_payment.isPartial'] = $this->getPaymentIsPartial();
        }
        return $transactionData;
    }

    /**
     * Get registration parameters
     *
     * @return array
     */
    protected function getRegistrationParameters()
    {
        $registrations_parameters = array();

        if ($this->isRecurring()) {
            $registrations_parameters['payment_recurring'] = 'INITIAL';
            $registrations_parameters['payment_registration'] = 'true';

            $reccuring_list = $this->getRecurringLists();
            if ($reccuring_list) {
                foreach ($reccuring_list as $key => $value) {
                    $registrations_parameters['registrations'][$key] = $value['ref_id'];
                }
            }
        }
        return $registrations_parameters;
    }

    /**
     * Get checkout ID and create payment widget at checkout payment page
     *
     * @param  string $payment_widget_url
     * @return string
     */
    public function getCheckoutId(&$payment_widget_url)
    {
        global $order;

        $transaction_data = $this->getCredentials();
        $transaction_data['customer']['email'] = $order->customer['email_address'];
        $transaction_data['customer']['first_name'] = $order->billing['firstname'];
        $transaction_data['customer']['last_name'] = $order->billing['lastname'];
        $transaction_data['billing']['street'] = $order->billing['street_address'];
        $transaction_data['billing']['city'] = $order->billing['city'];
        $transaction_data['billing']['zip'] = $order->billing['postcode'];
        $transaction_data['billing']['country_code'] = $order->billing['country']['iso_code_2'];
        $transaction_data['amount'] = $order->info['total'];
        $transaction_data['currency'] = $order->info['currency'];
        $transaction_data['customer_ip'] = $this->getCustomerIp();

        if ($this->code == 'vrpayecommerce_cards_saved') {
            $transaction_data['3D']['amount'] = $transaction_data['amount'];
            $transaction_data['3D']['currency'] = $transaction_data['currency'];
        }
        $transaction_data['test_mode'] = $this->getTestMode();
        $transaction_data['payment_type'] = $this->getPaymentType();
        $_SESSION['vrpayecommerce']['transaction_id'] = date('YmdHis') . xtc_create_random_value(5, 'digits');
        $transaction_data['transaction_id'] = $_SESSION['vrpayecommerce']['transaction_id'];

        $transaction_data = array_merge_recursive(
            $transaction_data,
            $this->getKlarnaParameters(),
            $this->getPaydirektParameters(),
            $this->getRegistrationParameters(),
            $this->getEasycreditParameters()
        );
        if ($this->isRecurring() && $this->getGroupRecurring() == 'OT') {
            unset($transaction_data['payment_type']);
        }

        $_SESSION['vrpayecommerce']['transactionData'] = $transaction_data;
        VRpayecommercePaymentCore::instanceLoggerClass()->notice('get transaction_data : '.
            print_r($transaction_data, 1), 'security', 'vrpayecommerce_log');

        $payment_widget_url = VRpayecommercePaymentCore::getPaymentWidgetUrl($transaction_data, $checkout_id);

        return $checkout_id;
    }

    /**
     * get required parameter for server to sever payment mode
     *
     * @return string
     */
    public function getServerToServerParameter(&$payment_widget_url)
    {
        global $order;

        $server_to_server_parameter = $this->getCredentials();
        $server_to_server_parameter['customer']['email'] = $order->customer['email_address'];
        $server_to_server_parameter['customer']['first_name'] = $order->billing['firstname'];
        $server_to_server_parameter['customer']['last_name'] = $order->billing['lastname'];
        $server_to_server_parameter['billing']['street'] = $order->billing['street_address'];
        $server_to_server_parameter['billing']['city'] = $order->billing['city'];
        $server_to_server_parameter['billing']['zip'] = $order->billing['postcode'];
        $server_to_server_parameter['billing']['country_code'] = $order->billing['country']['iso_code_2'];
        $server_to_server_parameter['amount'] = $order->info['total'];
        $server_to_server_parameter['currency'] = $order->info['currency'];
        $server_to_server_parameter['customer_ip'] = $this->getCustomerIp();

        if ($this->code == 'vrpayecommerce_cards_saved') {
            $server_to_server_parameter['3D']['amount'] = $server_to_server_parameter['amount'];
            $server_to_server_parameter['3D']['currency'] = $server_to_server_parameter['currency'];
        }
        $server_to_server_parameter['test_mode'] = $this->getTestMode();

        $_SESSION['vrpayecommerce']['transaction_id'] = date('YmdHis') . xtc_create_random_value(5, 'digits');
        $server_to_server_parameter['transaction_id'] = $_SESSION['vrpayecommerce']['transaction_id'];

        $server_to_server_parameter = array_merge_recursive(
            $server_to_server_parameter,
            $this->getKlarnaParameters(),
            $this->getPaydirektParameters(),
            $this->getRegistrationParameters(),
            $this->getEasycreditParameters()
        );
        if ($this->isRecurring() && $this->getGroupRecurring() == 'OT') {
            unset($server_to_server_parameter['payment_type']);
        }

        $_SESSION['vrpayecommerce']['transactionData'] = $server_to_server_parameter;
        $server_to_server_parameter['payment_type'] = $this->getPaymentType();

        VRpayecommercePaymentCore::instanceLoggerClass()->notice('get server to server parameter : '.
            print_r($server_to_server_parameter, 1), 'security', 'vrpayecommerce_log');
        
        return $server_to_server_parameter;
    }


    /**
     * Capture Payment server to server method
     *
     * @return void
     */
    public function capturePayment()
    {
        if ($this->isVersionTrackerActive()) {
            VersionTracker::sendVersionTracker($this->getVersionData());
        }
        $checkout_id = xtc_db_prepare_input($_POST['vrpay_id']);
        VRpayecommercePaymentCore::instanceLoggerClass()->notice('get checkout_id : '.
            print_r($checkout_id, 1), 'security', 'vrpayecommerce_log');

        $transaction_data = $this->getCredentials();
        $transaction_data['test_mode'] = $this->getTestMode();
        $transaction_data['payment_type'] = "CP";
        $transaction_data['amount'] = $_POST['amount'];
        $transaction_data['currency'] = $_POST['currency'];
        VRpayecommercePaymentCore::instanceLoggerClass()->notice('get transaction data : '.
            print_r($transaction_data, 1), 'security', 'vrpayecommerce_log');

        $payment_status = VRpayecommercePaymentCore::backOfficeOperation($checkout_id, $transaction_data);
        VRpayecommercePaymentCore::instanceLoggerClass()->notice('get payment status : '.
            print_r($result_delete_registration, 1), 'security', 'vrpayecommerce_log');

        if (!$payment_status) {
            $this->redirectFailure('ERROR_GENERAL_NORESPONSE');
        } else {
            $_SESSION['vrpayecommerce']['resultJson'] = $payment_status;
            $code_result = $payment_status["result"]["code"];
            $is_in_review = VRpayecommercePaymentCore::isSuccessReview($code_result);
            $_SESSION['vrpayecommerce']['isInReview'] = $is_in_review;
            $error_identifier = VRpayecommercePaymentCore::getErrorIdentifier($code_result);
            $transaction_result = VRpayecommercePaymentCore::getTransactionResult($code_result);
            VRpayecommercePaymentCore::instanceLoggerClass()->notice('get transaction result code : '.
                print_r($transaction_result, 1), 'security', 'vrpayecommerce_log');

            if ($transaction_result == "ACK") {
                $this->processPaymentSuccess($payment_status);
            } elseif ($transaction_result == "NOK") {
                $this->redirectFailure($error_identifier);
            } else {
                $this->redirectFailure('ERROR_UNKNOWN');
            }
        }
    }

    /**
     * Delete reccuring payment account
     *
     * @param  string $recurring_id
     * @param  string $type
     * @return string
     */
    public function deletePaymentAccount($recurring_id = false, $type = false)
    {
        if ($recurring_id) {
            if ($type == "reference") {
                $reference_id = $recurring_id;
            } else {
                $reference_id = $this->getReferenceId($recurring_id);
            }
            VRpayecommercePaymentCore::instanceLoggerClass()->notice('get reference_id : '.
                print_r($reference_id, 1), 'security', 'vrpayecommerce_log');

            $transaction_data = $this->getCredentials();
            $transaction_data['transaction_id'] = (int) $_SESSION['customer_id'];
            $transaction_data['test_mode'] = $this->getTestMode();
            VRpayecommercePaymentCore::instanceLoggerClass()->notice('get transaction data : '.
                print_r($transaction_data, 1), 'security', 'vrpayecommerce_log');


            $result_delete_registration =
                VRpayecommercePaymentCore::deleteRegistration($reference_id, $transaction_data);
            VRpayecommercePaymentCore::instanceLoggerClass()->notice('get result delete registration: '.
                print_r($result_delete_registration, 1), 'security', 'vrpayecommerce_log');


            if (!$result_delete_registration || $result_delete_registration == 'ERROR_MERCHANT_SSL_CERTIFICATE') {
                return $result_delete_registration;
            }

            $code_result = $result_delete_registration["result"]["code"];
            $error_identifier = VRpayecommercePaymentCore::getErrorIdentifier($code_result);
            $transaction_result = VRpayecommercePaymentCore::getTransactionResult($code_result);
            VRpayecommercePaymentCore::instanceLoggerClass()->notice('get transaction result code : '.
                print_r($transaction_result, 1), 'security', 'vrpayecommerce_log');

            if ($transaction_result == "ACK") {
                xtc_db_query(
                    "delete from payment_vrpayecommerce_recurring
                    where cust_id = '".(int) $_SESSION['customer_id']."'
                    and ref_id = '". $reference_id."'"
                );
            }
            return $transaction_result;
        }
    }

    /**
     * Redirect to failure payment page
     *
     * @param  string $error_identifier
     * @return void
     */
    public function redirectFailure($error_identifier)
    {
        if ($this->isRecurring() && $this->code!='vrpayecommerce_paypal') {
            xtc_redirect(
                xtc_href_link(
                    'checkout_payment_vrpayecommerce.php?payment_error='.$this->code.'&error='.$error_identifier,
                    '',
                    "SSL",
                    true,
                    false
                )
            );
        } else {
            xtc_redirect(
                xtc_href_link(
                    'checkout_payment.php?payment_error='.$this->code.'&error='.$error_identifier,
                    '',
                    "SSL",
                    true,
                    false
                )
            );
        }
    }

    /**
     * Redirect to failure page of my payment information
     *
     * @param  string $error_identifier
     * @param  string $id
     * @return void
     */
    public function redirectFailureRecurring($error_identifier, $id = false)
    {
        if ($this->code == 'vrpayecommerce_paypal_saved') {
            if ($id) {
                xtc_redirect(xtc_href_link('account_payment_information.php?error=change', '', "SSL", true, false));
            } else {
                xtc_redirect(xtc_href_link('account_payment_information.php?error=register', '', "SSL", true, false));
            }
        } else {
            if ($id) {
                xtc_redirect(
                    xtc_href_link(
                        'ext/modules/payment/vrpayecommerce/change_payment.php?selected_payment='.$this->code.'
                        &id='.$id.'&error='.$error_identifier,
                        '',
                        "SSL",
                        true,
                        false
                    )
                );
            } else {
                xtc_redirect(
                    xtc_href_link(
                        'ext/modules/payment/vrpayecommerce/register_payment.php?selected_payment='.$this->code.'
                        &error='.$error_identifier,
                        '',
                        "SSL",
                        true,
                        false
                    )
                );
            }
        }
    }

    /**
     * Get version data
     *
     * @return array
     */
    public function getVersionData()
    {
        $merchant = $this->getMerchantData();
        $version_data['transaction_mode'] = $this->getServerMode();
        $version_data['ip_address'] = $_SERVER['SERVER_ADDR'];
        $version_data['shop_version'] = $this->shop_version;
        $version_data['plugin_version'] = $this->version;
        $version_data['client'] = $this->client;
        $version_data['email'] = $merchant['merchant_email'];
        $version_data['merchant_id'] = $merchant['merchant_no'];
        $version_data['shop_system'] = $this->shop_system;
        $version_data['shop_url'] = $merchant['shop_url'];
        return $version_data;
    }

    /**
     * Get paypal saved response
     *
     * @param  string $registration_id
     * @return void
     */
    protected function getPaypalSavedResponse($registration_id)
    {
        VRpayecommercePaymentCore::instanceLoggerClass()->notice('start payment use paypal recurring', 'security', 'vrpayecommerce_log');
        if ($registration_id && $this->code = 'vrpayecommerce_paypal_saved') {
            $transaction_data = $_SESSION['vrpayecommerce']['transactionData'];
            $transaction_data['payment_type'] = $this->getPaymentType();
            $debit_response = VRpayecommercePaymentCore::useRegistration($registration_id, $transaction_data);

            $code_result = $debit_response['result']['code'];
            $transaction_result = VRpayecommercePaymentCore::getTransactionResult($code_result);

            if ($debit_response) {
                if ($debit_response == 'ERROR_MERCHANT_SSL_CERTIFICATE') {
                    VRpayecommercePaymentCore::instanceLoggerClass()->notice(
                        'error because of ssl certificate',
                        'security',
                        'vrpayecommerce_log'
                    );
                    $this->redirectFailure('ERROR_MERCHANT_SSL_CERTIFICATE');
                }
                if ($transaction_result == 'ACK') {
                    $_SESSION['vrpayecommerce']['resultJson'] = $debit_response;
                    xtc_redirect(xtc_href_link(FILENAME_CHECKOUT_PROCESS, '', 'SSL'));
                } elseif ($transaction_result == 'NOK') {
                    $error_identifier = VRpayecommercePaymentCore::getErrorIdentifier($code_result);
                    $this->redirectFailure($error_identifier);
                } else {
                    $this->redirectFailure('ERROR_UNKNOWN');
                }
            } else {
                $this->redirectFailure('ERROR_GENERAL_NORESPONSE');
            }
        }
    }

    /**
     * Insert recurring payment data into the database
     *
     * @param  string $payment_brand
     * @param  string $account
     * @param  string $registration_id
     * @param  string $default
     * @return void
     */
    protected function insertRecurringPayment($payment_brand, $account, $registration_id, $default)
    {
        $credentials = $this->getCredentials();
        xtc_db_query(
            "insert into payment_vrpayecommerce_recurring(
            cust_id,
            payment_group,
            brand,
            email,
            holder,
            last4digits,
            expiry_month,
            expiry_year,
            server_mode,
            channel_id,
            ref_id,
            payment_default
            )values (
            '".(int) $_SESSION['customer_id']."',
            '".$this->getGroupRecurring()."',
            '".$payment_brand."',
            '".$account['email']."',
            '".$account['holder']."',
            '".$account['last4Digits']."',
            '".$account['expiryMonth']."',
            '".$account['expiryYear']."',
            '".$credentials['server_mode']."',
            '".$credentials['channel_id']."',
            '".$registration_id."',
            '".$default."')"
        );
    }

    /**
     * Update recurring payment into the database
     *
     * @param  string $payment_brand
     * @param  string $account
     * @param  string $registration_id
     * @param  string $default
     * @return void
     */
    protected function updateRecurringPayment($payment_brand, $account, $registration_id, $recurring_id)
    {
        $credentials = $this->getCredentials();
        xtc_db_query(
            "update payment_vrpayecommerce_recurring set cust_id = '".(int) $_SESSION['customer_id']."',
            payment_group = '".$this->getGroupRecurring()."',
            brand = '".$payment_brand."',
            email = '".$account['email']."',
            holder = '".$account['holder']."',
            last4digits = '".$account['last4Digits']."',
            expiry_month = '".$account['expiryMonth']."',
            expiry_year = '".$account['expiryYear']."',
            server_mode = '".$credentials['server_mode']."',
            channel_id = '".$credentials['channel_id']."',
            ref_id = '".$registration_id."' where id = '".(int) $recurring_id."'"
        );
    }

    /**
     * Process paypal saved
     *
     * @param  array $payment_status
     * @return void
     */
    protected function processPaypalSaved($payment_status)
    {
        VRpayecommercePaymentCore::instanceLoggerClass()->notice('start payment using paypal recurring', 'security', 'vrpayecommerce_log');
        $registration_id = $payment_status['id'];
        $transaction_data = $this->getCredentials();
        $transaction_data['amount'] = $_SESSION['vrpayecommerce']['transactionData']['amount'];
        $transaction_data['currency'] = $_SESSION['vrpayecommerce']['transactionData']['currency'];
        $transaction_data['transaction_id'] = $payment_status['merchantTransactionId'];
        $transaction_data['payment_recurring'] = 'INITIAL';
        $transaction_data['test_mode'] = $this->getTestMode();
        $transaction_data['payment_type'] = 'DB';

        $debit_response = VRpayecommercePaymentCore::useRegistration($registration_id, $transaction_data);
        if ($debit_response == 'ERROR_MERCHANT_SSL_CERTIFICATE') {
            VRpayecommercePaymentCore::instanceLoggerClass()->notice('error because of ssl certificate', 'security', 'vrpayecommerce_log');
            $this->redirectFailure('ERROR_MERCHANT_SSL_CERTIFICATE');
        }
        $code_result = $debit_response['result']['code'];
        $error_identifier = VRpayecommercePaymentCore::getErrorIdentifier($code_result);
        $transaction_result = VRpayecommercePaymentCore::getTransactionResult($code_result);

        if ($transaction_result == 'ACK') {
            $_SESSION['vrpayecommerce']['resultJson']['id'] = $debit_response['id'];
            $_SESSION['vrpayecommerce']['resultJson']['registrationId'] = $payment_status['id'];
            $_SESSION['vrpayecommerce']['resultJson']['amount'] = $debit_response['amount'];
            $_SESSION['vrpayecommerce']['resultJson']['currency'] = $debit_response['currency'];
            VRpayecommercePaymentCore::instanceLoggerClass()->notice('set vrpayecommerce session : '.
                print_r($_SESSION['vrpayecommerce'], 1), 'security', 'vrpayecommerce_log');
        } elseif ($transaction_result == 'NOK') {
            $this->redirectFailure($error_identifier);
        } else {
            $this->redirectFailure('ERROR_UNKNOWN');
        }
    }

    /**
     * Process payment success
     *
     * @param  array $payment_status
     * @return void
     */
    protected function processPaymentSuccess($payment_status)
    {
        if ($this->isRecurring()) {
            $payment_brand = $payment_status['paymentBrand'];

            if ($payment_brand == 'PAYPAL') {
                $this->processPaypalSaved($payment_status);
            }
        }
        xtc_redirect(xtc_href_link(FILENAME_CHECKOUT_PROCESS, '', 'SSL'));
    }

    /**
     * Get payment response after doing payment
     *
     * @return void
     */
    public function getShopUrl()
    {
        if ($this->isVersionTrackerActive()) {
            VersionTracker::sendVersionTracker($this->getVersionData());
        }
        $checkout_id = xtc_db_prepare_input($_REQUEST['id']);
        $registration_id = xtc_db_prepare_input($_POST['registrationId']);

        $this->getPaypalSavedResponse($registration_id);

        $transaction_data = $this->getCredentials();

        if ($_GET['response_from'] == 'checkout') {
            $payment_status = VRpayecommercePaymentCore::getPaymentStatus($checkout_id, $transaction_data);
        } elseif ($_GET['response_from'] == 'servertoserver') {
            $payment_status =
                VRpayecommercePaymentCore::getPaymentServerToServerStatus($checkout_id, $transaction_data);

            return $payment_status;
        }

        if (!$payment_status) {
            VRpayecommercePaymentCore::instanceLoggerClass()->notice('error payment status is empty ', 'security', 'vrpayecommerce_log');
            $this->redirectFailure('ERROR_GENERAL_NORESPONSE');
        } else {
            if ($payment_status == 'ERROR_MERCHANT_SSL_CERTIFICATE') {
                VRpayecommercePaymentCore::instanceLoggerClass()->notice(
                    'error because of ssl certificate',
                    'security',
                    'vrpayecommerce_log'
                );
                $this->redirectFailure('ERROR_MERCHANT_SSL_CERTIFICATE');
            }
            $_SESSION['vrpayecommerce']['resultJson'] = $payment_status;
            $code_result = $payment_status["result"]["code"];
            $is_in_review = VRpayecommercePaymentCore::isSuccessReview($code_result);
            $_SESSION['vrpayecommerce']['isInReview'] = $is_in_review;
            $error_identifier = VRpayecommercePaymentCore::getErrorIdentifier($code_result);
            $transaction_result = VRpayecommercePaymentCore::getTransactionResult($code_result);

            if ($transaction_result == "ACK") {
                $this->processPaymentSuccess($payment_status);
            } elseif ($transaction_result == "NOK") {
                $this->redirectFailure($error_identifier);
            } else {
                $this->redirectFailure('ERROR_UNKNOWN');
            }
        }
    }

    /**
     * Process paypal recurring at my payment information page
     *
     * @param  string $recurring_id
     * @param  array  $payment_status
     * @param  array  $transaction_data
     * @return void
     */
    public function processPaypalSavedRecurring($recurring_id, $payment_status, $transaction_data)
    {
        $payment_brand = $payment_status['paymentBrand'];
        $account = $this->getAccount($payment_status);
        $default = $this->checkDefault();

        $registration_id = $payment_status['id'];
        $transaction_data['payment_type'] = 'DB';
        $response = VRpayecommercePaymentCore::useRegistration($registration_id, $transaction_data);

        if ($response == 'ERROR_MERCHANT_SSL_CERTIFICATE') {
            VRpayecommercePaymentCore::instanceLoggerClass()->notice('error because of ssl certificate', 'security', 'vrpayecommerce_log');
            $this->redirectFailureRecurring('ERROR_MERCHANT_SSL_CERTIFICATE', $recurring_id);
        }

        $code_result = $response["result"]["code"];
        $error_identifier = VRpayecommercePaymentCore::getErrorIdentifier($code_result);
        $transaction_result = VRpayecommercePaymentCore::getTransactionResult($code_result);

        if ($transaction_result == "ACK") {
            $reference_id = $response['id'];
            $transaction_data['payment_type'] = "RF";
            $refund_response = VRpayecommercePaymentCore::backOfficeOperation($reference_id, $transaction_data);

            if ($refund_response == 'ERROR_MERCHANT_SSL_CERTIFICATE') {
                VRpayecommercePaymentCore::instanceLoggerClass()->notice(
                    'error because of ssl certificate',
                    'security',
                    'vrpayecommerce_log'
                );
                $this->redirectFailureRecurring('ERROR_MERCHANT_SSL_CERTIFICATE', $recurring_id);
            }

            VRpayecommercePaymentCore::instanceLoggerClass()->notice('get recurring_id : '.$recurring_id, 'security', 'vrpayecommerce_log');
            if ($recurring_id) {
                VRpayecommercePaymentCore::instanceLoggerClass()->notice('change paypal payment account', 'security', 'vrpayecommerce_log');
                $this->updateRecurringPayment($payment_brand, $account, $registration_id, $recurring_id);
            } else {
                VRpayecommercePaymentCore::instanceLoggerClass()->notice('add paypal payment account', 'security', 'vrpayecommerce_log');
                $this->insertRecurringPayment($payment_brand, $account, $registration_id, $default);
            }
        } elseif ($transaction_result == "NOK") {
            $this->redirectFailureRecurring($error_identifier, $recurring_id);
        } else {
            $this->redirectFailureRecurring('ERROR_UNKNOWN', $recurring_id);
        }
    }

    /**
     * Process Credit Cards and Direct Debit recurring payment at my payment information page
     *
     * @param  string $recurring_id
     * @param  array  $payment_status
     * @param  array  $transaction_data
     * @return void
     */
    public function processPaymentSavedRecurring($recurring_id, $payment_status, $transaction_data)
    {
        $payment_brand = $payment_status['paymentBrand'];
        VRpayecommercePaymentCore::instanceLoggerClass()->notice('start payment using '.
            $payment_brand, 'security', 'vrpayecommerce_log');

        $account = $this->getAccount($payment_status);
        VRpayecommercePaymentCore::instanceLoggerClass()->notice('get payment account '.
            print_r($account, 1), 'security', 'vrpayecommerce_log');

        $default = $this->checkDefault();
        VRpayecommercePaymentCore::instanceLoggerClass()->notice('get default payment account '.
            print_r($default, 1), 'security', 'vrpayecommerce_log');


        $reference_id = $payment_status['id'];
        VRpayecommercePaymentCore::instanceLoggerClass()->notice('start reference_id  '.
            $reference_id, 'security', 'vrpayecommerce_log');

        $registration_id = $payment_status['registrationId'];
        VRpayecommercePaymentCore::instanceLoggerClass()->notice('start registration_id using '.
            $registration_id, 'security', 'vrpayecommerce_log');


        if ($this->getPaymentType() == "PA") {
            $transaction_data['payment_type'] = "CP";
            $cp_response = VRpayecommercePaymentCore::backOfficeOperation($reference_id, $transaction_data);

            if ($cp_response == 'ERROR_MERCHANT_SSL_CERTIFICATE') {
                VRpayecommercePaymentCore::instanceLoggerClass()->notice(
                    'error because of ssl certificate',
                    'security',
                    'vrpayecommerce_log'
                );
                $this->redirectFailureRecurring('ERROR_MERCHANT_SSL_CERTIFICATE', $recurring_id);
            }

            $code_result = $cp_response["result"]["code"];
            $error_identifier = VRpayecommercePaymentCore::getErrorIdentifier($code_result);
            $cp_result = VRpayecommercePaymentCore::getTransactionResult($code_result);

            if ($cp_result == 'ACK') {
                $reference_id = $cp_response['id'];
            } elseif ($cp_result == 'NOK') {
                $this->redirectFailureRecurring($error_identifier, $recurring_id);
            } else {
                $this->redirectFailureRecurring('ERROR_UNKNOWN', $recurring_id);
            }
        }
        $transaction_data['payment_type'] = "RF";
        $response = VRpayecommercePaymentCore::backOfficeOperation($reference_id, $transaction_data);

        if ($response == 'ERROR_MERCHANT_SSL_CERTIFICATE') {
            VRpayecommercePaymentCore::instanceLoggerClass()->notice('error because of ssl certificate', 'security', 'vrpayecommerce_log');
            $this->redirectFailureRecurring('ERROR_MERCHANT_SSL_CERTIFICATE', $recurring_id);
        }
        if (!$response) {
            $this->redirectFailureRecurring('ERROR_UNKNOWN', $recurring_id);
        }

        if ($recurring_id) {
            $this->updateRecurringPayment($payment_brand, $account, $registration_id, $recurring_id);
        } else {
            $this->insertRecurringPayment($payment_brand, $account, $registration_id, $default);
        }
    }

    /**
     * Process payment success recurring
     *
     * @param  string $recurring_id
     * @param  array  $payment_status
     * @param  array  $transaction_data
     * @return void
     */
    public function processPaymentSuccessRecurring($recurring_id, $payment_status, $transaction_data)
    {
        $payment_brand = $payment_status['paymentBrand'];
        $transaction_data['amount'] = $this->getRegisterAmount();
        $transaction_data['currency'] = $_SESSION['currency'];
        if ($recurring_id) {
            $transaction_data['transaction_id'] = $payment_status['merchantTransactionId'];
        } else {
            $transaction_data['transaction_id'] = (int) $_SESSION['customer_id'];
        }
        $transaction_data['payment_recurring'] = 'INITIAL';
        $transaction_data['test_mode'] = $this->getTestMode();

        if ($payment_brand == 'PAYPAL') {
            $this->processPaypalSavedRecurring($recurring_id, $payment_status, $transaction_data);
        } else {
            $this->processPaymentSavedRecurring($recurring_id, $payment_status, $transaction_data);
        }
        if ($recurring_id) {
            $reference_id = $payment_status['merchantTransactionId'];
            $result = VRpayecommercePaymentCore::deleteRegistration($reference_id, $transaction_data);

            xtc_redirect(xtc_href_link('account_payment_information.php?success=change', '', 'SSL'));
        } else {
            xtc_redirect(xtc_href_link('account_payment_information.php?success=register', '', 'SSL'));
        }
    }

    /**
     * Get payment response after change and register payment
     *
     * @param  string $recurring_id
     * @return void
     */
    public function getShopUrlRecurring($recurring_id = false)
    {
        if ($this->isVersionTrackerActive()) {
            VersionTracker::sendVersionTracker($this->getVersionData());
        }
        $checkout_id = xtc_db_prepare_input($_REQUEST['id']);
        $transaction_data = $this->getCredentials();
        $payment_status = VRpayecommercePaymentCore::getPaymentStatus($checkout_id, $transaction_data);

        if (!$payment_status) {
            $this->redirectFailureRecurring('ERROR_GENERAL_NORESPONSE', $recurring_id);
        } else {
            $code_result = $payment_status["result"]["code"];
            $error_identifier = VRpayecommercePaymentCore::getErrorIdentifier($code_result);
            $transaction_result = VRpayecommercePaymentCore::getTransactionResult($code_result);

            if ($transaction_result == "ACK") {
                $this->processPaymentSuccessRecurring($recurring_id, $payment_status, $transaction_data);
            } elseif ($transaction_result == "NOK") {
                $this->redirectFailureRecurring($error_identifier, $recurring_id);
            } else {
                $this->redirectFailureRecurring('ERROR_UNKNOWN', $recurring_id);
            }
        }
    }

    /**
     * Create backend order
     *
     * @param  string $oID
     * @param  string $action
     * @param  string $error_identifier
     * @return boolean
     */
    public function backendCreateOrder($oID, $action, &$error_identifier = '')
    {
        VRpayecommercePaymentCore::instanceLoggerClass()->notice('start backend create order', 'security', 'vrpayecommerce_log');
        $transaction_data =  $this->getCredentials();

        $orders_query = xtc_db_query(
            "SELECT payment_method, currency FROM " . TABLE_ORDERS . "
            WHERE `orders_id`='" .(int)$oID. "'"
        );
        VRpayecommercePaymentCore::instanceLoggerClass()->notice('query to table orders : '.
            print_r($orders_query, 1), 'security', 'vrpayecommerce_log');

        $orders_result = xtc_db_fetch_array($orders_query);
        VRpayecommercePaymentCore::instanceLoggerClass()->notice('get data from table orders : '.
            print_r($orders_result, 1), 'security', 'vrpayecommerce_log');

        $payment_method = $orders_result['payment_method'];
        $currency = $orders_result['currency'];

        $orders_total_query = xtc_db_query(
            "SELECT value FROM " . TABLE_ORDERS_TOTAL . "
            WHERE `orders_id`='" .(int)$oID. "'
            AND `class`='ot_total'"
        );
        VRpayecommercePaymentCore::instanceLoggerClass()->notice('query to table orders_total : '.
            print_r($orders_total_query, 1), 'security', 'vrpayecommerce_log');

        $orders_total_result = xtc_db_fetch_array($orders_total_query);
        VRpayecommercePaymentCore::instanceLoggerClass()->notice('get data from table orders_total : '.
            print_r($orders_total_result, 1), 'security', 'vrpayecommerce_log');

        $amount = $orders_total_result['value'];

        $vrpayecommerce_orders_query = xtc_db_query(
            "SELECT unique_id FROM `payment_vrpayecommerce_orders`
            WHERE `orders_id`='" .(int)$oID. "'"
        );
        VRpayecommercePaymentCore::instanceLoggerClass()->notice('query to table payment_vrpayecommerce_orders  : '.
            print_r($vrpayecommerce_orders_query, 1), 'security', 'vrpayecommerce_log');

        $vrpayecommerce_orders_result = xtc_db_fetch_array($vrpayecommerce_orders_query);
        VRpayecommercePaymentCore::instanceLoggerClass()->notice('get data from table payment_vrpayecommerce_orders : '.
            print_r($vrpayecommerce_orders_result, 1), 'security', 'vrpayecommerce_log');

        $vrpayecommerce_recurring_query = xtc_db_query(
            "SELECT ref_id FROM `payment_vrpayecommerce_recurring`
            WHERE `id`='" .$vrpayecommerce_orders_result['unique_id']. "'
            AND server_mode = '".$transaction_data['server_mode']."'
            AND channel_id = '".$transaction_data['channel_id']."'"
        );
        VRpayecommercePaymentCore::instanceLoggerClass()->notice('query to table payment_vrpayecommerce_recurring  : '.
            print_r($vrpayecommerce_recurring_query, 1), 'security', 'vrpayecommerce_log');

        $vrpayecommerce_recurring_result = xtc_db_fetch_array($vrpayecommerce_recurring_query);
        VRpayecommercePaymentCore::instanceLoggerClass()->notice('get data from table payment_vrpayecommerce_recurring  : '.
            print_r($vrpayecommerce_recurring_query, 1), 'security', 'vrpayecommerce_log');
        

        $registration_id = $vrpayecommerce_recurring_result['ref_id'];
        if ($this->isMultichannel()) {
            $transaction_data['channel_id'] = $transaction_data['channel_id_moto'];
        }
        $transaction_data['currency'] = $currency;
        $transaction_data['amount'] = $amount;
        $transaction_data['test_mode'] = $this->getTestMode();
        $transaction_data['payment_type'] = $action;
        $transaction_data['transaction_id'] = (int)$oID;

        $backend_order_response = VRpayecommercePaymentCore::useRegistration($registration_id, $transaction_data);

        if ($backend_order_response == 'ERROR_MERCHANT_SSL_CERTIFICATE') {
            VRpayecommercePaymentCore::instanceLoggerClass()->notice('error because of ssl certificate', 'security', 'vrpayecommerce_log');
            $error_identifier = 'ERROR_MERCHANT_SSL_CERTIFICATE';
            return false;
        }
        if (!$backend_order_response) {
            $error_identifier = 'ERROR_GENERAL_PROCESSING';
            return false;
        }

        $code_result = $backend_order_response["result"]["code"];
        $transaction_result = VRpayecommercePaymentCore::getTransactionResult($code_result);

        if ($transaction_result == 'ACK') {
            $update_array = array(
                'unique_id' => $backend_order_response['id'],
                'amount' => $backend_order_response['amount'],
                'currency' => $backend_order_response['currency'],
                'payment_type' => $backend_order_response['paymentType']
            );
            xtc_db_perform(
                'payment_vrpayecommerce_orders',
                $update_array,
                'update',
                'orders_id = \'' . (int)$oID . '\''
            );
            return true;
        }
        $error_identifier = VRpayecommercePaymentCore::getErrorIdentifierBackend($code_result);
        return false;
    }

    /**
     * Do capture and refund Payment
     *
     * @param  string $oID
     * @param  string $action
     * @param  string $error_identifier
     * @return boolean
     */
    public function backendPaymentAction($oID, $action, &$error_identifier = '')
    {
        $vrpayecommerce_orders_query = xtc_db_query(
            "SELECT unique_id, amount, currency, payment_type FROM `payment_vrpayecommerce_orders`
            WHERE `orders_id`='" .xtc_db_input($oID). "'"
        );
        $vrpayecommerce_orders_result = xtc_db_fetch_array($vrpayecommerce_orders_query);

        if ($vrpayecommerce_orders_result['payment_type'] == $action) {
            return true;
        } else {
            $reference_id = $vrpayecommerce_orders_result['unique_id'];
            $transaction_data =  $this->getCredentials();
            if ($this->isMultichannel()) {
                $transaction_data['channel_id'] = $transaction_data['channel_id_moto'];
            }
            $transaction_data['currency'] = $vrpayecommerce_orders_result['currency'];
            $transaction_data['amount'] = $vrpayecommerce_orders_result['amount'];
            $transaction_data['test_mode'] = $this->getTestMode();
            $transaction_data['payment_type'] = $action;

            $response = VRpayecommercePaymentCore::backOfficeOperation($reference_id, $transaction_data);

            if ($response == 'ERROR_MERCHANT_SSL_CERTIFICATE') {
                $error_identifier = 'ERROR_MERCHANT_SSL_CERTIFICATE';
                return false;
            }
            if (!$response) {
                $error_identifier = 'ERROR_GENERAL_PROCESSING';
                return false;
            }

            $code_result = $response["result"]["code"];
            $transaction_result = VRpayecommercePaymentCore::getTransactionResult($code_result);

            if ($transaction_result == 'ACK') {
                xtc_db_query(
                    "UPDATE `payment_vrpayecommerce_orders` SET `payment_type`='".$action."'
                    WHERE `orders_id`='" .xtc_db_input($oID). "'"
                );
                return true;
            }
            $error_identifier = VRpayecommercePaymentCore::getErrorIdentifierBackend($code_result);
            return false;
        }
    }

    /**
     * update payment status
     *
     * @param  string $oID
     * @param  string $payment_type
     * @param  string $error_identifier
     * @return boolean
     */
    public function backendUpdateStatus($oID, &$payment_type, &$error_identifier = '')
    {
        $vrpayecommerce_orders_query = xtc_db_query(
            "SELECT unique_id, amount, currency, payment_type FROM `payment_vrpayecommerce_orders`
            WHERE `orders_id`='" .xtc_db_input($oID). "'"
        );
        $vrpayecommerce_orders_result = xtc_db_fetch_array($vrpayecommerce_orders_query);

        $reference_id = $vrpayecommerce_orders_result['unique_id'];
        $transaction_data =  $this->getCredentials();
        $transaction_data['test_mode'] = $this->getTestMode();

        $update_status_response = VRpayecommercePaymentCore::updateStatus($reference_id, $transaction_data);

        if ($update_status_response == 'ERROR_MERCHANT_SSL_CERTIFICATE') {
            $error_identifier = 'ERROR_MERCHANT_SSL_CERTIFICATE';
            return false;
        }
        if (!$update_status_response) {
            $error_identifier = 'ERROR_GENERAL_PROCESSING';
            return false;
        }

        $xmlResult = simplexml_load_string($update_status_response);
        $code = (string) $xmlResult->Result->Transaction->Processing->Return['code'];
        $status_result = VRpayecommercePaymentCore::getTransactionResult($code);

        $payment_code = (string) $xmlResult->Result->Transaction->Payment['code'];
        $payment_type = substr($payment_code, -2);

        if ($status_result == 'ACK') {
            $inReview = VRpayecommercePaymentCore::isSuccessReview($code);

            if ($inReview) {
                $error_identifier = VRpayecommercePaymentCore::getErrorIdentifierBackend($code);
                return false;
            } else {
                xtc_db_query(
                    "UPDATE `payment_vrpayecommerce_orders`
                    SET `payment_type`='".$payment_type."' WHERE `orders_id`='" .xtc_db_input($oID). "'"
                );
                return true;
            }
        }
        $error_identifier = VRpayecommercePaymentCore::getErrorIdentifierBackend($code);
        return false;
    }
}
MainFactory::load_origin_class('vrpayecommerce_base');
