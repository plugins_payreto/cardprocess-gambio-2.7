<?php

require_once 'vrpayecommerce/vrpayecommerce_base.php';

/**
 * vrpayecommerce_cards class to create VR pay eCommerce Sofort payment method in Gambio
 *
 * @package    modules.payment
 * @subpackage vrpayecommerce
 */
class vrpayecommerce_sofort extends vrpayecommerce_base
{
    /**
     * This function is constructor of vrpayecommerce_sofort class
     *
     * @return void
     */
    public function __construct()
    {
        $this->code = "vrpayecommerce_sofort";
        $this->brand = 'SOFORTUEBERWEISUNG';
        if ($_SESSION['language_code'] == 'de') {
            $this->logo = "sofortuberweisung.png";
        } else {
            $this->logo = "sofortbanking.gif";
        }
        parent::__construct();
    }
}
MainFactory::load_origin_class('vrpayecommerce_sofort');
