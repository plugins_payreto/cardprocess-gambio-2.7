<?php
require 'includes/application_top.php';
// Process Server to server confirmation

require_once DIR_FS_CATALOG . 'includes/modules/payment/vrpayecommerce_easycredit.php';
$current_payment = new Vrpayecommerce_easycredit;
$response = $current_payment->getShopUrl();


$transaction_result = VRpayecommercePaymentCore::getTransactionResult($response['result']['code']);
if ($transaction_result == 'NOK') {
    $error_identifier = VRpayecommercePaymentCore::getErrorIdentifier($response['result']['code']);
    $current_payment->redirectFailure($error_identifier);
}

$_SESSION['servertoserver_txt'] = $response['resultDetails']['tilgungsplanText'];
$_SESSION['servertoserver_amount'] = $response['amount'];
$_SESSION['servertoserver_sum'] = $response['resultDetails']['ratenplan.zinsen.anfallendeZinsen'];
$_SESSION['servertoserver_total'] = $response['resultDetails']['ratenplan.gesamtsumme'];
$_SESSION['servertoserver_currency'] = $response['currency'];
$_SESSION['servertoserver_info_url'] = $response['resultDetails']['vorvertraglicheInformationen'];

$GLOBALS['breadcrumb']->add(NAVBAR_TITLE_1_CHECKOUT_CONFIRMATION, xtc_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'SSL'));
$GLOBALS['breadcrumb']->add('Order Review');

// if the customer is not logged on, redirect them to the login page
if (isset($_SESSION['customer_id']) === false) {
    xtc_redirect(xtc_href_link(FILENAME_LOGIN, '', 'SSL'));
}

require_once DIR_FS_CATALOG . 'ext/modules/payment/vrpayecommerce/classes/vrpayecommerce_confirmation_control.php';

// mediafinanz
require_once DIR_FS_CATALOG . 'includes/modules/mediafinanz/include_checkout_confirmation.php';

$coo_checkout_confirmation_control = new VrpayecommerceConfirmationControl;

$coo_checkout_confirmation_control->set_data('GET', $_GET);
$coo_checkout_confirmation_control->set_data('POST', $_POST);

$coo_checkout_confirmation_control->proceed();

$t_redirect_url = $coo_checkout_confirmation_control->get_redirect_url();
if (empty($t_redirect_url) == false) {
    xtc_redirect($t_redirect_url);
} else {
    $t_main_content = $coo_checkout_confirmation_control->get_response();
}

$coo_layout_control = MainFactory::create_object('LayoutContentControl');

$coo_layout_control->set_data('GET', $_GET);
$coo_layout_control->set_data('POST', $_POST);
$coo_layout_control->set_('coo_breadcrumb', $GLOBALS['breadcrumb']);
$coo_layout_control->set_('coo_product', $GLOBALS['product']);
$coo_layout_control->set_('coo_xtc_price', $GLOBALS['xtPrice']);
$coo_layout_control->set_('c_path', $GLOBALS['cPath']);
$coo_layout_control->set_('main_content', $t_main_content);
$coo_layout_control->set_('request_type', $GLOBALS['request_type']);
$coo_layout_control->proceed();

$t_redirect_url = $coo_layout_control->get_redirect_url();
if (empty($t_redirect_url) === false) {
    xtc_redirect($t_redirect_url);
} else {
    echo $coo_layout_control->get_response();
}
