<?php

require_once 'vrpayecommerce/vrpayecommerce_base.php';

/**
 * vrpayecommerce_cards class to create VR pay eCommerce Klarna Installment payment method in Gambio
 *
 * @package    modules.payment
 * @subpackage vrpayecommerce
 */
class vrpayecommerce_klarnains extends vrpayecommerce_base
{
    protected $payment_type = 'PA';

    /**
     * This function is constructor of vrpayecommerce_klarnains class
     *
     * @return void
     */
    public function __construct()
    {
        $this->code = "vrpayecommerce_klarnains";
        $this->brand = 'KLARNA_INSTALLMENTS';
        if ($_SESSION['language_code'] == 'de') {
            $this->logo = "klarnains_de.png";
        } else {
            $this->logo = "klarnains_en.png";
        }
        parent::__construct();
    }

    /**
     * Get VR pay eCommerce Klarna Installment keys
     *
     * @return integer
     */
    public function keys()
    {
        return array(
          'MODULE_PAYMENT_'.$this->code_upper_case.'_STATUS',
          'MODULE_PAYMENT_'.$this->code_upper_case.'_SERVER_MODE',
          'MODULE_PAYMENT_'.$this->code_upper_case.'_CHANNEL',
          'MODULE_PAYMENT_'.$this->code_upper_case.'_PCLASS',
          'MODULE_PAYMENT_'.$this->code_upper_case.'_ZONE',
          'MODULE_PAYMENT_'.$this->code_upper_case.'_ALLOWED',
          'MODULE_PAYMENT_'.$this->code_upper_case.'_SORT_ORDER'
        );
    }

    /**
     * Install VR pay eCommerce Klarna Installment payment method
     *
     * @return void
     */
    public function install()
    {
        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            date_added
            ) values (
            'MODULE_PAYMENT_".$this->code_upper_case."_PCLASS',
            '',
            '6',
            '0',
            now())"
        );
        parent::install();
    }
}
MainFactory::load_origin_class('vrpayecommerce_klarnains');
