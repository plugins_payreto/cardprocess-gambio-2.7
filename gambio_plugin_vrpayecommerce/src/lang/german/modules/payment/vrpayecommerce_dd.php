<?php

define('MODULE_PAYMENT_VRPAYECOMMERCE_DD_TEXT_TITLE', 'Lastschrift (SEPA)');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_DD_TEXT_DESCRIPTION',
    '<a href="http://www.vrpayecommerce.de" target="_blank" style="text-decoration: underline; font-weight: bold;">
    <img src="../ext/modules/payment/vrpayecommerce/images/logovrpay.jpg" border="0">
    </a></br><p>VR pay eCommerce - Lastschrift (SEPA)</p>'
);

define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_DD_STATUS_TITLE',
    'Aktivieren Sie das VR pay eCommerce Lastschrift (SEPA) Modul'
);
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_DD_STATUS_DESC',
    'Bitte aktivieren Sie das Modul,
    wenn Sie Lastschrift (SEPA) Transaktionen von VR pay eCommerce akzeptieren wollen.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_DD_SERVER_MODE_TITLE', 'Server');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_DD_SERVER_MODE_DESC',
    'Wälen Sie den Server für Lastschrift (SEPA).'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_DD_PAYMENT_TYPE_TITLE', 'Transaktions Modus');
define('MODULE_PAYMENT_VRPAYECOMMERCE_DD_PAYMENT_TYPE_DESC', 'Transaktions Modus für Karten auswählen.');

define('MODULE_PAYMENT_VRPAYECOMMERCE_DD_CHANNEL_TITLE', 'Entity-ID');
define('MODULE_PAYMENT_VRPAYECOMMERCE_DD_CHANNEL_DESC', 'Entity-ID für Lastschrift (SEPA) hinterlegen.');

define('MODULE_PAYMENT_VRPAYECOMMERCE_DD_ORDER_STATUS_ID_TITLE', 'Bestellstatus für akzeptierte Zahlungen festlegen');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_DD_ORDER_STATUS_ID_DESC',
    'Hinterlegen Sie hier den Bestellstatus, der bei akzeptierten Zahlungen angezeigt werden soll.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_DD_RF_ORDER_STATUS_ID_TITLE', 'Bestellstatus für Refunds hinterlegen');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_DD_RF_ORDER_STATUS_ID_DESC',
    'Hinterlegen Sie hier den Bestellstatus, der bei Refunds angezeigt werden soll.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_DD_ZONE_TITLE', 'Zahlungszone');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_DD_ZONE_DESC',
    'Wenn ein Land ausgewählt wurde, ist die Zahlungsart nur für dieses Land freigeschaltet.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_DD_ALLOWED_TITLE', 'Erlaubte Länder');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_DD_ALLOWED_DESC',
    'Bitte wählen Sie die Länder aus, in den dieses Modul verfügbar sein soll.'
);

define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_DD_SORT_ORDER_TITLE',
    'Legen Sie die Reihenfolge der Zahlungsarten fest'
);
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_DD_SORT_ORDER_DESC',
    'Die Reihenfolge in der die Zahlungsarten im Bestellprozess angezeigt werden,
    wird im Feld &bdquo;Reihenfolge&rdquo; mit bei &bdquo;1&rdquo;
    beginnenden Ziffern festgelegt und kann individuell angepasst werden.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_DD_TT_TRANSACTION_ID', 'Transaktions-ID');
