<?php
/**
 * vrpayecommerce_ageneral class to create VR pay eCommerce general config in Gambio
 *
 * @package    modules.payment
 * @subpackage vrpayecommerce
 */
class vrpayecommerce_ageneral
{
    public $code;
    public $title;
    public $description;
    public $enabled;

    /**
     * This function is constructor of vrpayecommerce_ageneral class
     *
     * @return void
     */
    public function __construct()
    {
        $this->code = "vrpayecommerce_ageneral";
        $this->version = '2.1.11';
        $this->code_upper_case = strtoupper($this->code);
        $this->title = $this->getConstantValue('MODULE_PAYMENT_'.$this->code_upper_case.'_TEXT_TITLE');
        $this->description = $this->getConstantValue('MODULE_PAYMENT_'.$this->code_upper_case.'_TEXT_DESCRIPTION');
        $this->enabled = false;
    }

    /**
     * check VR pay eCommerce general status
     *
     * @return integer
     */
    public function check()
    {
        if (!isset($this->_check)) {
            $check_query = xtc_db_query(
                "select configuration_value from ".TABLE_CONFIGURATION."
                where configuration_key = 'MODULE_PAYMENT_".$this->code_upper_case."_STATUS'"
            );
            $this->_check = xtc_db_num_rows($check_query);
        }
        return $this->_check;
    }

    /**
     * Get VR pay eCommerce general keys
     *
     * @return integer
     */
    public function keys()
    {
        return array(
            'MODULE_PAYMENT_'.$this->code_upper_case.'_STATUS',
            'MODULE_PAYMENT_'.$this->code_upper_case.'_USER_LOGIN',
            'MODULE_PAYMENT_'.$this->code_upper_case.'_USER_PWD',
            'MODULE_PAYMENT_'.$this->code_upper_case.'_RECURRING',
            'MODULE_PAYMENT_'.$this->code_upper_case.'_MERCHANT_EMAIL',
            'MODULE_PAYMENT_'.$this->code_upper_case.'_MERCHANT_NO',
            'MODULE_PAYMENT_'.$this->code_upper_case.'_SHOP_URL',
            'MODULE_PAYMENT_'.$this->code_upper_case.'_VERSION_TRACKER'
        );
    }

    /**
     * Install VR pay eCommerce general config
     *
     * @return void
     */
    public function install()
    {
        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            set_function,
            date_added
            ) values (
            'MODULE_PAYMENT_".$this->code_upper_case."_STATUS',
            'True',
            '6',
            '0',
            'xtc_cfg_select_option(array(\'True\'), ',
            now())"
        );
        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            date_added
            ) values (
            'MODULE_PAYMENT_".$this->code_upper_case."_USER_LOGIN',
            '',
            '6',
            '0',
            now())"
        );
        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            date_added
            ) values (
            'MODULE_PAYMENT_".$this->code_upper_case."_USER_PWD',
            '',
            '6',
            '0',
            now())"
        );
        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            set_function,
            date_added
            ) values (
            'MODULE_PAYMENT_".$this->code_upper_case."_RECURRING',
            'True',
            '6',
            '0',
            'xtc_cfg_select_option(array(\'True\', \'False\'), ',
            now())"
        );
        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            set_function,
            date_added
            ) values (
            'MODULE_PAYMENT_".$this->code_upper_case."_MERCHANT_EMAIL',
            '',
            '6',
            '0',
            'vrpay_cfg_input_required(',
            now())"
        );
        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            set_function,
            date_added
            ) values (
            'MODULE_PAYMENT_".$this->code_upper_case."_MERCHANT_NO',
            '',
            '6',
            '0',
            'vrpay_cfg_input_required(',
            now())"
        );
        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            set_function,
            date_added
            ) values (
            'MODULE_PAYMENT_".$this->code_upper_case."_SHOP_URL',
            '',
            '6',
            '0',
            'vrpay_cfg_input_required(',
            now())"
        );
        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            set_function,
            date_added
            ) values (
            'MODULE_PAYMENT_".$this->code_upper_case."_VERSION_TRACKER',
            'True',
            '6',
            '0',
            'xtc_cfg_select_option(array(\'True\', \'False\'), ',
            now())"
        );
        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            date_added
            ) values(
            'MODULE_PAYMENT_".$this->code_upper_case."_ALLOWED',
            '',
            '6',
            '0',
            now())"
        );
        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            date_added
            ) values (
            'MODULE_PAYMENT_".$this->code_upper_case."_SORT_ORDER',
            '0',
            '6',
            '0',
            now())"
        );

        $check_query = xtc_db_query("SHOW columns FROM " . TABLE_ADMIN_ACCESS . " LIKE 'vrpayecommerce'");

        if (xtc_db_num_rows($check_query) < 1) {
            xtc_db_query("ALTER TABLE " . TABLE_ADMIN_ACCESS . " ADD COLUMN vrpayecommerce INT(1) DEFAULT 1");
        }
        xtc_redirect(xtc_href_link('vrpayecommerce.php', 'set=' . $_GET['set'] . '&module=vrpayecommerce_ageneral'));
    }

    /**
     * Uninstall and remove VR pay eCommerce general config
     *
     * @return void
     */
    public function remove()
    {
        $keys = $this->keys();
        $keys[] = 'MODULE_PAYMENT_'.$this->code_upper_case.'_ALLOWED';
        $keys[] = 'MODULE_PAYMENT_'.$this->code_upper_case.'_SORT_ORDER';
        xtc_db_query(
            "delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", $keys) . "')"
        );
    }

    public function getConstantValue($value)
    {
        return defined($value) ? constant($value) : '';
    }
}
MainFactory::load_origin_class('vrpayecommerce_ageneral');
