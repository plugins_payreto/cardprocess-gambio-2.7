<?php

define('MODULE_PAYMENT_VRPAYECOMMERCE_SOFORT_TEXT_TITLE', 'SOFORT Banking');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_SOFORT_TEXT_DESCRIPTION',
    '<a href="http://www.vrpayecommerce.de/" target="_blank" style="text-decoration: underline; font-weight: bold;">
    <img src="../ext/modules/payment/vrpayecommerce/images/logovrpay.jpg" border="0">
    </a></br><p>VR pay eCommerce - SOFORT Banking</p>'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_SOFORT_STATUS_TITLE', 'Enable VR pay eCommerce SOFORT Banking Module');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_SOFORT_STATUS_DESC',
    'Please activate the module, if you want to accept SOFORT Banking transactions of VR pay eCommerce.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_SOFORT_SERVER_MODE_TITLE', 'Server');
define('MODULE_PAYMENT_VRPAYECOMMERCE_SOFORT_SERVER_MODE_DESC', 'Choose the server for SOFORT Banking.');

define('MODULE_PAYMENT_VRPAYECOMMERCE_SOFORT_CHANNEL_TITLE', 'Entity-ID');
define('MODULE_PAYMENT_VRPAYECOMMERCE_SOFORT_CHANNEL_DESC', 'Insert the Entity-ID for SOFORT Banking.');

define('MODULE_PAYMENT_VRPAYECOMMERCE_SOFORT_ORDER_STATUS_ID_TITLE', 'Set Order Status for Accepted payment');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_SOFORT_ORDER_STATUS_ID_DESC',
    'Define here the Order Status which should be used for Accepted Payments'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_SOFORT_RF_ORDER_STATUS_ID_TITLE', 'Set Order Status for Refunds payment');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_SOFORT_RF_ORDER_STATUS_ID_DESC',
    'Define here the Order Status which should be used for Refunds Payments.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_SOFORT_ZONE_TITLE', 'Payment Zone');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_SOFORT_ZONE_DESC',
    'When a country is selected, this payment method will be enabled for that zone only.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_SOFORT_ALLOWED_TITLE', 'Allowed Countries');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_SOFORT_ALLOWED_DESC',
    'Please enter the countries <b>individually</b>
    that should be allowed to use this module (e.g. US, UK (leave blank to allow all countries)).'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_SOFORT_SORT_ORDER_TITLE', 'Display Sort Order');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_SOFORT_SORT_ORDER_DESC',
    'Display sort order; the lowest value is displayed first.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_SOFORT_TT_TRANSACTION_ID', 'Transaction ID');
