<?php

define('MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_TEXT_TITLE', 'PayPal');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_TEXT_DESCRIPTION',
    '<a href="http://www.vrpayecommerce.de" target="_blank" style="text-decoration: underline; font-weight: bold;">
    <img src="../ext/modules/payment/vrpayecommerce/images/logovrpay.jpg" border="0">
    </a></br><p>VR pay eCommerce - PayPal</p>'
);

define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_STATUS_TITLE',
    'Aktivieren Sie das VR pay eCommerce PayPal Modul'
);
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_STATUS_DESC',
    'Bitte aktivieren Sie das Modul, wenn Sie PayPal Transaktionen von VR pay eCommerce akzeptieren wollen.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SERVER_MODE_TITLE', 'Server');
define('MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SERVER_MODE_DESC', 'Wählen Sie den Server für PayPal.');

define('MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_CHANNEL_TITLE', 'Entity-ID');
define('MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_CHANNEL_DESC', 'Entity-ID für PayPal hinterlegen.');

define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_ORDER_STATUS_ID_TITLE',
    'Bestellstatus für akzeptierte Zahlungen festlegen'
);
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_ORDER_STATUS_ID_DESC',
    'Hinterlegen Sie hier den Bestellstatus, der bei akzeptierten Zahlungen angezeigt werden soll.'
);

define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_RF_ORDER_STATUS_ID_TITLE',
    'Bestellstatus für Refunds hinterlegen'
);
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_RF_ORDER_STATUS_ID_DESC',
    'Hinterlegen Sie hier den Bestellstatus, der bei Refunds angezeigt werden soll.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_ZONE_TITLE', 'Zahlungszone');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_ZONE_DESC',
    'Wenn ein Land ausgewählt wurde, ist die Zahlungsart nur für dieses Land freigeschaltet.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_ALLOWED_TITLE', 'Erlaubte Länder');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_ALLOWED_DESC',
    'Bitte wählen Sie die Länder aus, in den dieses Modul verfügbar sein soll.'
);

define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SORT_ORDER_TITLE',
    'Legen Sie die Reihenfolge der Zahlungsarten fest'
);
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_SORT_ORDER_DESC',
    'Die Reihenfolge in der die Zahlungsarten im Bestellprozess angezeigt werden,
    wird im Feld &bdquo;Reihenfolge&rdquo; mit bei &bdquo;1&rdquo;
    beginnenden Ziffern festgelegt und kann individuell angepasst werden.'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_PAYPAL_TT_TRANSACTION_ID', 'Transaktions-ID');
