<?php
/**
 * VrpayecommerceConfirmationView class for override gambio core template
 *
 * @package    modules.payment
 * @subpackage vrpayecommerce
 * @since      1.1.04
 */

class VrpayecommerceConfirmationView extends CheckoutConfirmationContentView
{
    public function __construct()
    {
        parent::__construct();

        $this->set_content_template('module/payment/vrpayecommerce/servertoserver_confirmation.html');
    }
}
