<?php

require_once 'vrpayecommerce/vrpayecommerce_base.php';

/**
 * vrpayecommerce_cards class to create VR pay eCommerce Klarna Invoice payment method in Gambio
 *
 * @package    modules.payment
 * @subpackage vrpayecommerce
 */
class vrpayecommerce_easycredit extends vrpayecommerce_base
{

    protected $payment_type = 'PA';

    /**
    * Vrpayecommerce_easycredit class to create VR pay eCommerce Easycredit payment method in Gambio
    *
    * @package    modules.payment
    * @subpackage vrpayecommerce
    */
    public function __construct()
    {
        $this->code = "vrpayecommerce_easycredit";
        $this->brand = 'RATENKAUF';
        $this->template = 'server_to_server_request.html';
        $this->logo = "easycredit.png";
        $this->info = $this->getNotify();
        parent::__construct();
    }

    /**
    * Setup easycredit info for payment method page
    *
    * @return array
    */
    public function selection()
    {
        $selection = array(
            'id' => $this->code,
            'module' => '<img src="' . DIR_WS_CATALOG . 'ext/modules/payment/vrpayecommerce/images/' . $this->logo .
            '" alt="Easycredit-Logo" style="border: none; background: white; padding: 2px; height:35px;">',
            'description' => $this->info,
        );
        return $selection;
    }

    /**
     * generate notification for easycredit validation
     *
     * @return string
     */
    public function getNotify()
    {
        $html = '';

        $validateEasycredit = $this->validateEasycredit();

        if (!empty($validateEasycredit)) {
            $html .= "<ul style='padding:10px 30px;'>";

            for ($a=0; $a<count($validateEasycredit); $a++) {
                $html .= "<li>".$validateEasycredit[$a]."</li>";
            }

            $html .= "</ul>";
            $html .= "<script type='text/javascript'>
                        var elm = getInputsByValue('vrpayecommerce_easycredit');
                        elm.disabled=true;
                        elm.checked=false;
                        elm.parentElement.parentElement.parentElement.parentElement.setAttribute('id', 'easycredit');
                        elm.parentElement.parentElement.parentElement.parentElement.classList.remove('list-group-item');
                        elm.parentElement.parentElement.classList.remove('button_checkout_module');
                        function getInputsByValue(value)
                        {
                            var allInputs = document.getElementsByTagName('input');
                            for(var x=0;x<allInputs.length;x++){
                                if(allInputs[x].value == value){
                                   return allInputs[x];
                                }
                            }
                            return false;
                        }
                     </script>";
            $html .= "<style type='text/css'>
                        li.vrpayecommerce_easycredit{
                            background:#eee !important;
                            display:block;margin-bottom:
                            15px;padding: 5px 15px 15px;
                            cursor:not-allowed;
                        }
                        li.vrpayecommerce_easycredit label{
                            cursor:not-allowed;
                        }
                        </style>";
        }

        return $html;
    }

    /**
     * validate parameter for easycredit:
     * - check if login and password credential is empty
     * - check if date of birth not empty and it's greater than now
     * - check if amount between 200 EUR to 3000 EUR
     * - check if shipping address equal to billing address
     *
     * @return array
     */
    public function validateEasycredit()
    {
        $error = array();

        if (!$this->isLoginDataFilled()) {
            $error[] = MODULE_PAYMENT_VRPAYECOMMERCE_EASYCREDIT_TEXT_ERROR_CREDENTIALS;
        }

        if (!$this->isDateOfBirthValid()) {
            $error[] = ERROR_EASYCREDIT_PARAMETER_DOB;
        }

        if (!$this->isDateOfBirthLowerThanToday()) {
            $error[] = ERROR_EASYCREDIT_FUTURE_DOB;
        }

        if (!$this->isGenderNotEmpty()) {
            $error[] = ERROR_MESSAGE_EASYCREDIT_PARAMETER_GENDER;
        }

        if (!$this->isAmountAllowed()) {
            $error[] = ERROR_MESSAGE_EASYCREDIT_AMOUNT_NOTALLOWED;
        }

        if (!$this->isBillingEqualShipping()) {
            $error[] = ERROR_EASYCREDIT_BILLING_NOTEQUAL_SHIPPING;
        }

        return $error;
    }

    /**
     * Determines if login data filled.
     *
     * @return     boolean  True if login data filled, False otherwise.
     */
    public function isLoginDataFilled()
    {
        $credentials = $this->getCredentials();

        if (!empty($credentials['login']) || !empty($credentials['password'])) {
            return true;
        }

        return false;
    }

    /**
    * get customer gender
    *
    * @return string
    */
    public function isGenderNotEmpty()
    {
        $customerData = $this->getCustomerData();
        $customerGender = $customerData['customers_gender'];

        if (!empty($customerGender)) {
            return true;
        }
        return false;
    }

    /**
     * is total amount allowed
     *
     * @return boolean
     */
    public function isAmountAllowed()
    {
        $isCurrencyEuro = $this->isCurrencyEuro();
        $totaAmount = $this->getTotalAmount();

        if ($isCurrencyEuro && $totaAmount >=200 && $totaAmount <=3000) {
            return true;
        }
        return false;
    }

    /**
     * Determines if currency euro.
     *
     * @return boolean
     */
    public function isCurrencyEuro()
    {
        global $order;
        $currency = $order->info['currency'];

        if ($currency != 'EUR') {
            return false;
        }
        return true;
    }

    /**
     * Gets the total amount.
     *
     * @return string
     */
    public function getTotalAmount()
    {
        global $order;
        $totalAmount = $order->info['total'];

        if (!empty($totalAmount)) {
            return $totalAmount;
        }

        return '0';
    }

    /**
     * is billing equal shipping
     *
     * @return boolean
     */
    public function isBillingEqualShipping()
    {
        global $order;

        $shippingAddress = $order->delivery['street_address'].' '.
                            $order->delivery['city'].' '.$order->delivery['postcode'].' '.
                            $order->delivery['state'];

        $billingAddress =  $order->billing['street_address'].' '.
                            $order->billing['city'].' '.
                            $order->billing['postcode'].' '.
                            $order->billing['state'];

        if ($billingAddress == $shippingAddress) {
            return true;
        }
        return false;
    }


    /**
     * is valid date of birth
     *
     * @return boolean
     */
    public function isDateOfBirthValid()
    {
        $customerData = $this->getCustomerData();

        $customerDateOfBirth = explode(" ", $customerData['customers_dob']);
        $customerDateOfBirth = explode("-", $customerDateOfBirth[0]);

        $year = (int)$customerDateOfBirth[0];
        $month = (int)$customerDateOfBirth[1];
        $day = (int)$customerDateOfBirth[2];

        if ($year < 1900) {
            return false;
        }

        if ($month < 1 || $month > 12) {
            return false;
        }

        if ($day < 1 || $day > 31) {
            return false;
        }

        $valid = checkdate($month, $day, $year);

        if (!$valid) {
            return false;
        }
        return true;
    }


    /**
     * is date of birth lower than today
     *
     * @return boolean
     */
    public function isDateOfBirthLowerThanToday()
    {
        $customerData = $this->getCustomerData();
        $customerDateOfBirth = explode(" ", $customerData['customers_dob']);
        $customerDateOfBirth = strtotime($customerDateOfBirth[0]);
        $today = strtotime(date('Y-m-d'));

        if ($customerDateOfBirth < $today) {
            return true;
        }
        return false;
    }
}

MainFactory::load_origin_class('vrpayecommerce_easycredit');
