<?php

/*include_once('vrpayecommerce/vrpayecommerce_base.php');

class vrpayecommerce_dc extends vrpayecommerce_base
{
    function vrpayecommerce_dc() {
        $this->code = "vrpayecommerce_dc";
		$this->brand = "VPAY MAESTRO DANKORT VISAELECTRON POSTEPAY";
		$this->template = "form_cc.html";
        parent::__construct();	  
    }

    function keys() {
      	return array(
      		'MODULE_PAYMENT_'.$this->codeUpperCase.'_STATUS', 
      		'MODULE_PAYMENT_'.$this->codeUpperCase.'_SERVER_MODE',
      		'MODULE_PAYMENT_'.$this->codeUpperCase.'_PAYMENT_TYPE',
      		'MODULE_PAYMENT_'.$this->codeUpperCase.'_CHANNEL', 
      		'MODULE_PAYMENT_'.$this->codeUpperCase.'_ZONE', 
      		'MODULE_PAYMENT_'.$this->codeUpperCase.'_ALLOWED', 
      		'MODULE_PAYMENT_'.$this->codeUpperCase.'_SORT_ORDER'
    	);
    }

    function install() {
        $remotely_status_id = $this->setOrderStatus(
            'Pre-Authorization of Payment',
            'Pre-Authorisierung der Zahlung',
            true
        );

        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            set_function,
            date_added
            ) values (
            'MODULE_PAYMENT_".$this->codeUpperCase."_PAYMENT_TYPE',
            'Debit',
            '6',
            '0',
            'xtc_cfg_select_option(array(\'Debit\', \'Pre-Authorization\'), ',
            now())"
        );    
        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            set_function,
            use_function,
            date_added
            ) values (
            'MODULE_PAYMENT_".$this->codeUpperCase."_PA_ORDER_STATUS_ID',
            '" . $remotely_status_id . "',
            '6',
            '0',
            'xtc_cfg_pull_down_order_statuses(',
            'xtc_get_order_status_name',
            now())"
        );
      
      	parent::install();
    }

    function getTitle() {
        return $this->getTitleCards();
    }

    function getPaymentType()
    {
        return $this->getPaymentTypeSelection();
    }
}
MainFactory::load_origin_class('vrpayecommerce_dc');*/
