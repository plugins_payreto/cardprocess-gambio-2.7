<?php

require_once 'vrpayecommerce/vrpayecommerce_base.php';

/**
 * vrpayecommerce_cards class to create VR pay eCommerce PayDirekt payment method in Gambio
 *
 * @package    modules.payment
 * @subpackage vrpayecommerce
 */
class vrpayecommerce_paydirekt extends vrpayecommerce_base
{

    /**
     * This function is constructor of vrpayecommerce_paydirekt class
     *
     * @return void
     */
    public function __construct()
    {
        $this->code = "vrpayecommerce_paydirekt";
        $this->brand = 'PAYDIREKT';
        $this->redirect = true;
        $this->logo = "paydirekt.png";
        parent::__construct();
    }

    /**
     * Get VR pay eCommerce PayDirekt keys
     *
     * @return integer
     */
    public function keys()
    {
        return array(
            'MODULE_PAYMENT_'.$this->code_upper_case.'_STATUS',
            'MODULE_PAYMENT_'.$this->code_upper_case.'_SERVER_MODE',
            'MODULE_PAYMENT_'.$this->code_upper_case.'_CHANNEL',
            'MODULE_PAYMENT_'.$this->code_upper_case.'_PAYMENT_TYPE',
            'MODULE_PAYMENT_'.$this->code_upper_case.'_PAYMENT_IS_PARTIAL',
            'MODULE_PAYMENT_'.$this->code_upper_case.'_MINIMUM_AGE',
            'MODULE_PAYMENT_'.$this->code_upper_case.'_ZONE',
            'MODULE_PAYMENT_'.$this->code_upper_case.'_ALLOWED',
            'MODULE_PAYMENT_'.$this->code_upper_case.'_SORT_ORDER'
        );
    }

    /**
     * Install VR pay eCommerce PayDirekt payment method
     *
     * @return void
     */
    public function install()
    {
        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            set_function,
            date_added
            ) values (
            'MODULE_PAYMENT_".$this->code_upper_case."_PAYMENT_TYPE',
            'Debit',
            '6',
            '0',
            'xtc_cfg_select_option(array(\'Debit\', \'Pre-Authorization\'), ',
            now())"
        );
        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            set_function,
            date_added
            ) values (
            'MODULE_PAYMENT_".$this->code_upper_case."_PAYMENT_IS_PARTIAL',
            'No',
            '6',
            '0',
            'xtc_cfg_select_option(array(\'Yes\', \'No\'), ',
            now())"
        );
        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            set_function,
            date_added
            ) values (
            'MODULE_PAYMENT_".$this->code_upper_case."_MINIMUM_AGE',
            '',
            '6',
            '0',
            'vrpay_cfg_input_required(',
            now())"
        );
        parent::install();
    }

    /**
     * Get VR pay eCommerce PayDirekt payment type
     *
     * @return string
     */
    public function getPaymentType()
    {
        return $this->getPaymentTypeSelection();
    }
}
MainFactory::load_origin_class('vrpayecommerce_paydirekt');
