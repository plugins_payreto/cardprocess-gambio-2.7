<?php
chdir('../../../../');
require 'includes/application_top.php';
require DIR_WS_LANGUAGES.$_SESSION['language'].'/modules/payment/vrpayecommerce_info.php';

$smarty = new Smarty;

$selected_payment = xtc_db_prepare_input($_POST['selected_payment']);
$id = xtc_db_prepare_input($_POST['id']);

if (isset($_POST['action'])) {
    include_once DIR_WS_MODULES. 'payment/'.$selected_payment.'.php';
    $payment_form = new $selected_payment;
    $response = $payment_form->deletePaymentAccount($id);
    if ($response == 'ACK') {
        xtc_redirect(xtc_href_link('account_payment_information.php?success=delete', '', 'SSL'));
    } else {
        $smarty->assign('error_message', 'delete failed');
    }
}

$GLOBALS['breadcrumb']->add(NAVBAR_TITLE_1_ACCOUNT_PASSWORD, xtc_href_link(FILENAME_ACCOUNT, '', 'SSL'));
$GLOBALS['breadcrumb']->add(FRONTEND_MC_INFO, xtc_href_link('account_payment_information.php', '', 'SSL'));

require DIR_WS_INCLUDES.'header.php';

$smarty->assign('id', $id);
$smarty->assign('selected_payment', $selected_payment);
$smarty->assign('language', $_SESSION['language']);
$smarty->assign('cancel_url', xtc_href_link('account_payment_information.php', '', 'SSL'));

$t_main_content = $smarty->fetch(CURRENT_TEMPLATE.'/module/payment/vrpayecommerce/delete_payment.html');

$smarty->assign('language', $_SESSION['language']);

/* --------------------------------------------- */
$coo_layout_control = MainFactory::create_object('LayoutContentControl');
$coo_layout_control->set_data('GET', $_GET);
$coo_layout_control->set_data('POST', $_POST);
$coo_layout_control->set_('coo_breadcrumb', $GLOBALS['breadcrumb']);
$coo_layout_control->set_('coo_product', $GLOBALS['product']);
$coo_layout_control->set_('coo_xtc_price', $GLOBALS['xtPrice']);
$coo_layout_control->set_('c_path', $GLOBALS['cPath']);
$coo_layout_control->set_('main_content', $t_main_content);
$coo_layout_control->set_('request_type', $GLOBALS['request_type']);
$coo_layout_control->proceed();

$t_redirect_url = $coo_layout_control->get_redirect_url();
if (empty($t_redirect_url) === false) {
    xtc_redirect($t_redirect_url);
} else {
    echo $coo_layout_control->get_response();
}
