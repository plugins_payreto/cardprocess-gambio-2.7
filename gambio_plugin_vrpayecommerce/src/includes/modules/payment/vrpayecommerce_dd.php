<?php

require_once 'vrpayecommerce/vrpayecommerce_base.php';

/**
 * vrpayecommerce_cards class to create VR pay eCommerce Direct Debit payment method in Gambio
 *
 * @package    modules.payment
 * @subpackage vrpayecommerce
 */
class vrpayecommerce_dd extends vrpayecommerce_base
{
    /**
     * This function is constructor of vrpayecommerce_dd class
     *
     * @return void
     */
    public function __construct()
    {
        $this->code = "vrpayecommerce_dd";
        $this->brand = 'DIRECTDEBIT_SEPA';
        $this->template = 'form_dd.html';
        $this->logo = "sepa.png";
        parent::__construct();
    }

    /**
     * Get VR pay eCommerce Direct Debit keys
     *
     * @return integer
     */
    public function keys()
    {
        return array(
          'MODULE_PAYMENT_'.$this->code_upper_case.'_STATUS',
          'MODULE_PAYMENT_'.$this->code_upper_case.'_SERVER_MODE',
          'MODULE_PAYMENT_'.$this->code_upper_case.'_PAYMENT_TYPE',
          'MODULE_PAYMENT_'.$this->code_upper_case.'_CHANNEL',
          'MODULE_PAYMENT_'.$this->code_upper_case.'_ZONE',
          'MODULE_PAYMENT_'.$this->code_upper_case.'_ALLOWED',
          'MODULE_PAYMENT_'.$this->code_upper_case.'_SORT_ORDER'
        );
    }

    /**
     * Install VR pay eCommerce Direct Debit payment method
     *
     * @return void
     */
    public function install()
    {
        xtc_db_query(
            "insert into " . TABLE_CONFIGURATION . " (
            configuration_key,
            configuration_value,
            configuration_group_id,
            sort_order,
            set_function,
            date_added
            ) values (
            'MODULE_PAYMENT_".$this->code_upper_case."_PAYMENT_TYPE',
            'Debit',
            '6',
            '0',
            'xtc_cfg_select_option(array(\'Debit\', \'Pre-Authorization\'), ',
            now())"
        );
        parent::install();
    }

    /**
     * Get VR pay eCommerce Direct Debit payment type
     *
     * @return string
     */
    public function getPaymentType()
    {
        return $this->getPaymentTypeSelection();
    }
}
MainFactory::load_origin_class('vrpayecommerce_dd');
