<?php

require_once 'vrpayecommerce/vrpayecommerce_base.php';

/**
 * vrpayecommerce_cards class to create VR pay eCommerce Klarna Invoice payment method in Gambio
 *
 * @package    modules.payment
 * @subpackage vrpayecommerce
 */
class vrpayecommerce_klarnainv extends vrpayecommerce_base
{
    protected $payment_type = 'PA';

     /**
     * This function is constructor of vrpayecommerce_klarnainv class
     *
     * @return void
     */
    public function __construct()
    {
        $this->code = "vrpayecommerce_klarnainv";
        $this->brand = 'KLARNA_INVOICE';
        if ($_SESSION['language_code'] == 'de') {
            $this->logo = "klarnainv_de.png";
        } else {
            $this->logo = "klarnainv_en.png";
        }
        parent::__construct();
    }
}
MainFactory::load_origin_class('vrpayecommerce_klarnainv');
