<?php

define('VRPAYECOMMERCE_BACKEND_GENERAL_VRPAY', 'VR pay eCommerce');

define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_TEXT_TITLE', 'VR pay eCommerce General Settings');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_TEXT_DESCRIPTION',
    '<a href="http://www.vrpayecommerce.de/" target="_blank" style="text-decoration: underline; font-weight: bold;">
    <img src="../ext/modules/payment/vrpayecommerce/images/logovrpay.jpg" border="0">
    </a></br><p>VR pay eCommerce v2.1.11 - General Settings</p>'
);

define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_STATUS_TITLE', 'VR pay eCommerce General Settings');
define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_STATUS_DESC', 'General settings for module VR pay eCommerce.');

define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_USER_LOGIN_TITLE', 'User-ID');
define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_USER_LOGIN_DESC', 'Insert User-ID.');

define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_USER_PWD_TITLE', 'Password');
define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_USER_PWD_DESC', 'Insert Password.');

define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_RECURRING_TITLE', 'Recurring');
define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_RECURRING_DESC', 'Enabled Recurring.');

define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_MERCHANT_EMAIL_TITLE', 'Merchant E-mail Address');
define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_MERCHANT_EMAIL_DESC', 'Insert Merchant E-mail Address.');

define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_MERCHANT_NO_TITLE', 'Merchant No. (VR Pay)');
define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_MERCHANT_NO_DESC', 'Your Customer ID from VR pay');

define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_SHOP_URL_TITLE', 'Shop URL');
define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_SHOP_URL_DESC', 'Insert Shop URL.');

define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_VERSION_TRACKER_TITLE', 'Version Tracker');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_VERSION_TRACKER_DESC',
    'When enabled, you accept to share your IP, email address, etc with Cardprocess.'
);

define(
    'VRPAYECOMMERCE_TT_VERSIONTRACKER',
    'WARNING For providing the best service to you,
    to inform you about newer versions of the plugin and also about security issues,
    VR pay is gathering some basic and technical information from the shop system (for details please see the manual).
    The information will under no circumstances be used for marketing and/or advertising purposes.
    Please be aware that deactivating the version tracker may affect the service quality
    and also important security and update information. '
);
define('VRPAYECOMMERCE_BACKEND_BT_OK', 'OK');

define('MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_ALLOWED_TITLE', 'Allowed Countries');
define(
    'MODULE_PAYMENT_VRPAYECOMMERCE_AGENERAL_ALLOWED_DESC',
    'Please enter the countries <b>individually</b>
    that should be allowed to use this module (e.g. US, UK (leave blank to allow all countries)).'
);
